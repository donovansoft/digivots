<?php 
require 'Classes/PHPExcel.php';
//crear el objeto
$objPHPExcel = new PHPExcel();
//agregar las propiedades al documento
$objPHPExcel->getProperties()
->setCreator("Departamento Administrativo")
->setLastModifiedBy("Departamento Administrativo")
->setTitle("Excel sincro")
->setSubject("Documento de sincro")
->setDescription("Documento generado con la nomina semanal")
->setKeywords("excel sincro csv")
->setCategory("sincro");
//seleccionar hoja activa
$objPHPExcel->setActiveSheetIndex(0);
//titulo de la hoja
$objPHPExcel->getActiveSheet()->setTitle('Hoja 1');

//agregar contenido a las celdas
$objPHPExcel->getActiveSheet()->setCellValue('A1', '0001');
$objPHPExcel->getActiveSheet()->setCellValue('B1', 'Carmona');
$objPHPExcel->getActiveSheet()->setCellValue('C1', 'Montana');
$objPHPExcel->getActiveSheet()->setCellValue('D1', 'David');
$objPHPExcel->getActiveSheet()->setCellValue('E1', '1');

//HEADER PARA GUARDAR EL ARCHIVO
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="miSeccion.csv"');
header('Cache-Control: max-age=0');

//se crea otro objeto
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'csv');

//indicar que lo vamos a guardar
$objWriter->save('php://output');

?>