<?php 
include '../controller/rutalinea.php';
include 'conexion.php'; 

	$exito = false;
	$ids = array();
	if(isset($_POST['ids']) && isset($_POST['mov']))
	{ 
 		if ($_POST['ids'] != '' && $_POST['mov'] !='' ){
			$ids = $_POST['ids'];
			$movilizador = $_POST['mov'];
		}
    $exito = true;
	}
	//SACA TODOS LOS MOVILIZADORES PREVIOS ANTES DE ACTUALIZAR
	$mov_ant = array();
	foreach ($ids as $id) {
		foreach ($id as $IdCaptura) {
			$sql = "SELECT AES_DECRYPT(Movilizador, '$linea') as Movilizador, AES_DECRYPT(IdCaptura, '$linea') as IdCaptura FROM personas WHERE IdCaptura = AES_ENCRYPT('$IdCaptura', '$linea')";  
			$resultado = mysqli_query($connect, $sql);
			while($row = mysqli_fetch_assoc($resultado)){
        array_push($mov_ant, $row);
      }
		}
	}
	//VARIABLES PREVIAS DE GUARDADO
	$JSON2 = array();
	date_default_timezone_set('UTC');
	date_default_timezone_set('America/Mexico_City');
	$horas = strftime("%H");
	$minutos = strftime("%M");
	$hora = $horas.":".$minutos;
	$date = date('Y-m-d');
	$date = $date." ".$hora;
	$MovilizadorAnterior = "";
	$IdCaptura = "";
	//For each para recorrer los arreglos y poder hacer las inserciones respectivas según el IdCaptura
	foreach ($mov_ant as $id => $indice) {
		foreach ($indice as $i => $val) {
			if($i === "Movilizador")
			{
				$MovilizadorAnterior = $val;
			}
			if($i === "IdCaptura")
			{
				$IdCaptura = $val;
			}
			
		}
		//ACTUALIZA EL ESTADO Y EL MOVILIZADOR
		$sqlUpdatePersona = "UPDATE personas SET Estado = AES_ENCRYPT('2', '$linea'), Movilizador = AES_ENCRYPT('$movilizador', '$linea') WHERE IdCaptura = AES_ENCRYPT('$IdCaptura', '$linea')";  
		$resultadoUpdatePersona = mysqli_query($connect, $sqlUpdatePersona);
		//Validamos que se haga la actualización del IdCaptura con el Nuevo Movilizador, sino entonces no tiene sentido actualizar ni nada
		if($resultadoUpdatePersona)
		{
			//Insertamos el cambio en el historialmov
			$sql1 = "INSERT INTO historicomov (IdHistorico, IdCaptura, ClaveMov, FHmovimiento) VALUES (NULL, AES_ENCRYPT('$IdCaptura', '$linea'), AES_ENCRYPT('$MovilizadorAnterior', '$linea'), AES_ENCRYPT('$date', '$linea'))"; 
			$resultado1 = mysqli_query($connect, $sql1);
			if($resultado1)
			{
				//echo "Insertado Historial";
			}else
			{
				echo mysqli_error($connect);
			}

			//Buscamos si existen registros con el Id en la posición actual
			$sqlVerify = "SELECT AES_DECRYPT(IdCaptura, '$linea') as IdCaptura FROM updates WHERE IdCaptura = AES_ENCRYPT('$IdCaptura', '$linea')";
			$resultadoUpdates = mysqli_query($connect, $sqlVerify);
			//Si se cumple mandará el total de registros, caso contrario mandará el posible error
			if($resultadoUpdates)
			{
				$rows = mysqli_num_rows($resultadoUpdates);
			}else
			{
				echo mysqli_error($connect);
			}
			//Si se encuentran un rows con el IdCaptura se procede a Actualizar, caso contrario Inserta
			if($rows != 0)
			{
				$sql2 = "UPDATE updates SET SincroBridge = AES_ENCRYPT('0', '$linea'), FHupdate = AES_ENCRYPT('$date', '$linea') WHERE IdCaptura = AES_ENCRYPT('$IdCaptura', '$linea')";
				$resultado2 = mysqli_query($connect, $sql2);
				//echo "Actualizar Rows: ".$rows."<br>";
			}else
			{
				$sql2 = "INSERT INTO updates (IdCaptura, SincroBridge, FHupdate) VALUES (AES_ENCRYPT('$IdCaptura', '$linea'), AES_ENCRYPT('0', '$linea'), AES_ENCRYPT('$date', '$linea'))";
				$resultado2 = mysqli_query($connect, $sql2);
				//echo "Insertar Rows: ".$rows."<br>";
			}
			//ELIMINA IDS DE ELIMINADOS POQUE PASARON A ACTUALIZADOS
			if($resultado2)
			{
				$sqlDelete = "DELETE FROM eliminados WHERE IdCaptura = AES_ENCRYPT('$IdCaptura', '$linea')";
				$resultadoDelete = mysqli_query($connect, $sqlDelete);
			}

			//Hacemos una consulta de los Id's que se modificaron y encodeamos con el Base64 
			$sql = "SELECT
				TO_BASE64(IdCaptura) AS IdCaptura,
				TO_BASE64(Movilizador) AS Movilizador,
				TO_BASE64(Estado) AS Estado,
				AES_DECRYPT(IdCaptura, '$linea') as id
				FROM  personas 
			WHERE IdCaptura = AES_ENCRYPT('$IdCaptura', '$linea')";
			$resultado = mysqli_query($connect, $sql);
			while($row = mysqli_fetch_assoc($resultado)) {
				array_push($JSON2, $row);
			}			
		}else{
			echo mysqli_error($connect); 
		}
	}
	//print_r($JSON2);
	//$JSON2 = trim($JSON2, ",");
	echo json_encode($JSON2);
?>