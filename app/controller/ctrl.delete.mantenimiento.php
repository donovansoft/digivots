<?php 
include '../controller/rutalinea.php';
include 'conexion.php'; 
$error = false;
if(isset($_POST['id']) && isset($_POST['mov']))
{
	if($_POST['id'] != "" && $_POST['mov'] != "")
	{
		$id = $_POST['id'];
		$mov = $_POST['mov'];
	}else
	{
		echo json_encode($error);
		return false;
	}
}else
{
	echo json_encode($error);
	return false;
}


$sql = "UPDATE personas SET Estado = AES_ENCRYPT('0', '$linea'), Movilizador = AES_ENCRYPT('0000', '$linea') WHERE IdCaptura = AES_ENCRYPT('$id', '$linea')"; 
$resultado = mysqli_query($connect, $sql);

	date_default_timezone_set('UTC');
	date_default_timezone_set('America/Mexico_City');
	$horas = strftime("%H");
	$minutos = strftime("%M");
	$hora = $horas.":".$minutos;
	$date = date('Y-m-d');
	$date = $date." ".$hora;

$sql5 = "INSERT INTO historicomov (IdHistorico, IdCaptura, ClaveMov, FHmovimiento) VALUES (NULL, AES_ENCRYPT('$id', '$linea'), AES_ENCRYPT('$mov', '$linea'), AES_ENCRYPT('$date', '$linea'))"; 
$resultado5 = mysqli_query($connect, $sql5);

if ($resultado) 
{
	//Una vez actualizada la persona debemos verificar que la tabla de eliminados no tenga el ID y si la tiene modicar éste
	$sqlVerify = "SELECT AES_DECRYPT(IdCaptura, '$linea') as IdCaptura FROM eliminados WHERE IdCaptura = AES_ENCRYPT('$id', '$linea')";
	$resultado3 = mysqli_query($connect, $sqlVerify);

	if($resultado3)
	{
		$datos = mysqli_fetch_assoc($resultado3);
		if($datos != null)
		{
			//Aquí metemosel update del ID
			$sql3 = "UPDATE eliminados SET SincroBridge = AES_ENCRYPT('0', '$linea'), FHeliminado = AES_ENCRYPT('$date', '$linea') WHERE IdCaptura = AES_ENCRYPT('$id', '$linea')"; 
			$resultado3 = mysqli_query($connect, $sql3);
			
			$sql3 = "SELECT IdCaptura, Estado, Movilizador FROM personas WHERE IdCaptura = AES_ENCRYPT('$id', '$linea')";
			$resultado3 = mysqli_query($connect, $sql3);
			$array = mysqli_fetch_assoc($resultado3);
			$sending = array();
			foreach ($array as $key => $value) {
				$sending[$key] = base64_encode($value);
			}
			echo json_encode($sending);
		}else
		{
			//Aquí insertamos el ID
			$sql2 = "INSERT INTO eliminados (IdCaptura, SincroBridge, FHeliminado) VALUES (AES_ENCRYPT('$id', '$linea'), AES_ENCRYPT('0', '$linea'), AES_ENCRYPT('$date', '$linea'))";
			$resultado2 = mysqli_query($connect, $sql2);
			
			$sql3 = "SELECT IdCaptura, Estado, Movilizador FROM personas WHERE IdCaptura = AES_ENCRYPT('$id', '$linea')";
			$resultado3 = mysqli_query($connect, $sql3);
			$array = mysqli_fetch_assoc($resultado3);
			$sending = array();
			foreach ($array as $key => $value) {
				$sending[$key] = base64_encode($value);
			}
			echo json_encode($sending);
		}	
	}
} else {
	echo mysqli_error($connect);
  echo json_encode($resultado);
}

?>