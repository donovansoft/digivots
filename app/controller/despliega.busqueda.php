<?php 
  $mov = $_GET['mov'];
  $seccion = $_GET['seccion'];
?>
<link rel="stylesheet" type="text/css" href="/digivots/assets/booststrap/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="/digivots/assets/icon-awesome/css/font-awesome.min.css">
<script src="/digivots/assets/jquery/jquery.js"></script>
<link rel="stylesheet" type="text/css" href="/digivots/css/main.css">
<script>
function updatemov(valor) {
  id = valor.value;
  var btn0 = "#btn0" + id;
  var mov = "<?php echo $mov ?>";
  var mover = confirm("¿DESEAS MOVER ESTA PERSONA CON ESTE MOVILIZADOR?");
  var idbase64;
  var movbase64;
  var mov_antpre = ".mov"+id;
  var mov_ant = $(mov_antpre).val();
  if (mover == false) {
    return;
  }
  $.ajax({
    url: "../controller/ctrl.update.movilizador2.php",
    type: "POST",
    dataType: "JSON",
    data: { id: id,
            movilizador: mov,
            mov_ant : mov_ant
          },
    success: function(data)
    {
      if (data) {
        
        setTimeout(function(){
          location.reload();
        }, 1000);
        alert("Registros guardados!");
      }
      else {
        alert("Ha ocurrido un error!");
        return;
      }
      idbase64 = "" + data.IdCaptura;
      movbase64 = "" + data.Movilizador;
      estbase64 = "" + data.Estado;
      $.ajax({
        url: "http://sysreg.tech/bridge/funciones/update_vots.php",
        type: "POST",
        dataType: "JSON",
        data: { 
                u : 'syscam.consulta',
                k : 'cnslt140u4sscm',
                IdCaptura : idbase64,
                Movilizador : movbase64,
                Estado : estbase64},
        success: function(data)
        {
          if(data.estatus == "INSERTADO" || data.estatus == "ACTUALIZADO")
          {
            $.ajax({
              url: "../controller/ctrl.update.bridge.movilizador2.php",
              type: "POST",
              dataType: "JSON",
              data: { 
                      IdCaptura : id},
              success: function(data)
              {
              }, 
              error: function(errno){
                  console.log(errno);
              }
            })
            .done(function() {
              console.log("success");
            })
            .fail(function() {
              console.log("error");
            })
            .always(function() {
              console.log("complete");
            });
          }
        }, 
        error: function(errno){
          console.log(errno); 
        }
      })
      .done(function() {
        console.log("success");
      })
      .fail(function(error) {
        console.log(error);
      })
      .always(function() {
        console.log("complete");
      });
    },
    error: function(error){
      console.log(error);
    }
  })
  .done(function() {
    console.log("success");
  })
  .fail(function() {
    console.log("error");
  })
  .always(function() {
    console.log("complete");
  });
}
</script>


<?php

  $nombre = $_GET['nombre'];
  $paterno = $_GET['paterno'];
  $materno = $_GET['materno'];
  $id = $_GET['id'];
  //echo $id;

  include '../controller/rutalinea.php';
  include '../controller/conexion.php';

  $sql = "SELECT
  AES_DECRYPT(IdCaptura, '$linea') as IdCaptura
, AES_DECRYPT(NombreCaptura, '$linea') as NombreCaptura
, AES_DECRYPT(PaternoCaptura, '$linea') as PaternoCaptura
, AES_DECRYPT(MaternoCaptura, '$linea') as MaternoCaptura
, AES_DECRYPT(FechaNacimiento, '$linea') as FechaNacimiento
, AES_DECRYPT(Telefono, '$linea') as Telefono
, AES_DECRYPT(Calle, '$linea') as Calle
, AES_DECRYPT(Noext, '$linea') as Noext
, AES_DECRYPT(Noint, '$linea') as Noint
, AES_DECRYPT(Colonia, '$linea') as Colonia
, AES_DECRYPT(Municipio, '$linea') as Municipio
, AES_DECRYPT(Seccion, '$linea') as Seccion
, AES_DECRYPT(Manzana, '$linea') as Manzana
, AES_DECRYPT(Casilla, '$linea') as Casilla
, AES_DECRYPT(Movilizador, '$linea') as Movilizador
, AES_DECRYPT(Folio, '$linea') as Folio
, AES_DECRYPT(Orden, '$linea') as Orden
, AES_DECRYPT(CheckLocal, '$linea') as CheckLocal
, AES_DECRYPT(SincroBridge, '$linea') as SincroBridge
, AES_DECRYPT(Estado, '$linea') as Estado
FROM personas
WHERE
      UPPER(AES_DECRYPT(NombreCaptura, '$linea')) LIKE UPPER('%$nombre%')
  AND UPPER(AES_DECRYPT(PaternoCaptura, '$linea')) LIKE UPPER('%$paterno%')
  AND UPPER(AES_DECRYPT(MaternoCaptura, '$linea')) LIKE UPPER('%$materno%')
  AND UPPER(AES_DECRYPT(Movilizador, '$linea')) != UPPER('$mov')
  AND AES_DECRYPT(IdCaptura, '$linea') LIKE '%$id%'
  AND AES_DECRYPT(Seccion, '$linea') = '$seccion'";
$resultado = mysqli_query($connect, $sql);


echo '      
    <div style="margin: 10px;">
      <table class="table table-sm table-hover">
        <thead>
          <tr>
            <th style="width: 4rem;"><small style="font-size: 14px">ID</small></th>
            <th style="width: 16rem;"><small style="font-size: 14px">Nombre</small></th>
            <th style="width: 7rem;"><small style="font-size: 14px">Fecha Nacimiento</small></th>
            <th style="width: 4rem;"><small style="font-size: 14px">Teléfono</small></th>
            <th style="width: 5rem;"><small style="font-size: 14px">Calle </small></th>
            <th style="width: 3rem;"><small style="font-size: 14px">No. Ext</small></th>
            <th style="width: 3rem;"><small style="font-size: 14px">No. Int</small></th>
            <th><small style="font-size: 14px">Movilizador</small></th>
            <th style="width: 9rem;"><small style="font-size: 14px"><center>Cargar</center></small></th>
          </tr>
        </thead>
        <tbody>
          ';
            while($row = mysqli_fetch_assoc($resultado)) {

echo '          
          <tr style = "background-color: ';
          if ($row['Estado'] == '0') {
            echo "rgba(76, 175, 80, .4);";
          } else {
            echo "rgba(201, 12, 12, .5);";
          }
echo          '" >
            <td style="width: 4rem;"><small style="font-size: 14px">'.$row['IdCaptura'].'</small></td>
            <td><small style="font-size: 14px; width: 16rem;">'.$row['PaternoCaptura'].' '.$row['MaternoCaptura'].' '.$row['NombreCaptura'].'</small></td>
            <td><small style="font-size: 14px">'.$row['FechaNacimiento'].'</small></td>
            <td><small style="font-size: 14px"><input type="text" name="telefono" style="width: 5rem;" maxlength="10" value="'.$row['Telefono'].'"  class="tel'.$row['IdCaptura'].'" disabled></small></td>
            <td><small style="font-size: 14px">
              <input type="text" name="calle" value="'.$row['Calle'].'" style="width: 5rem"  class="calle'.$row['IdCaptura'].'" disabled>
            </small></td>
            <td><small style="font-size: 14px">
              <input type="text" name="noext" value="'.$row['Noext'].'" style="width: 3rem"  class="noext'.$row['IdCaptura'].'" disabled>
            </small></td>
            <td><small style="font-size: 14px">
              <input type="text" name="noint" value="'.$row['Noint'].'" style="width: 3rem"  class="noint'.$row['IdCaptura'].'" disabled>
            </small></td>

            <td>
              <small style="font-size: 14px">
                <select name="menu" disabled class="mov'.$row['IdCaptura'].'">
                  <option selected>'.$row['Movilizador'].'</option>
                  ';
                    ini_set('max_execution_time', 300);
                    $sql2 = "SELECT AES_DECRYPT(Movilizador, '$linea') as Mov
                      FROM personas
                      GROUP BY Mov
                      ORDER BY Mov
                      ";
                    $resultado2 = mysqli_query($connect, $sql2);
                    while($row2 = mysqli_fetch_assoc($resultado2)) {
                  echo '
                    <option value="'.$row2['Mov'].'">'.$row2['Mov'].'</option>
                  ';
                    }
echo '                
                </select>
              </small>
            </td>
            <td style="width: 9rem;">
              <center>';            
                if($row['Estado'] == 0)
                {
echo '          <button class="mr-2 btn btn-outline-success px-2" title="Editar" style="padding: 0px; margin: 0px;" value="'.$row['IdCaptura'].'" onclick="updatemov(this);" id="btn0'.$row['IdCaptura'].'">
                  <i class="fa fa-upload"></i>
                </button>';                  
                }else
                {
echo '          <button class="mr-2 btn btn-outline-success px-2 g-cursor-disabled" title="Editar" style="padding: 0px; margin: 0px;" value="'.$row['IdCaptura'].'" id="btn0'.$row['IdCaptura'].'" disabled="true">
                  <i class="fa fa-upload"></i>
                </button>';     
                }
echo '        </center>
            </td>
          </tr>
          ';
            }

echo '        
        </tbody>
      </table>
    </div>
      ';
?>