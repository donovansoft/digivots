<?php 
include '../controller/rutalinea.php';
include 'conexion.php'; 

	$exito = false;
	if(isset($_POST['id']) && isset($_POST['movilizador']))
	{ 
   			if ($_POST['id'] != '' && $_POST['movilizador'] !='' ){
				$id = $_POST['id'];
				$movilizador = $_POST['movilizador'];
			}
         $exito = true;
	}

$mov_ant = $_POST['mov_ant'];

$sql = "UPDATE personas SET Movilizador = AES_ENCRYPT('$movilizador', '$linea'), Estado = AES_ENCRYPT('2', '$linea') WHERE IdCaptura = AES_ENCRYPT('$id', '$linea')";  
$resultado = mysqli_query($connect, $sql);

  date_default_timezone_set('UTC');
	date_default_timezone_set('America/Mexico_City');
	$horas = strftime("%H");
	$minutos = strftime("%M");
	$hora = $horas.":".$minutos;
	$date = date('Y-m-d');
	$date = $date." ".$hora;
$sql5 = "INSERT INTO historicomov (IdHistorico, IdCaptura, ClaveMov, FHmovimiento) VALUES (NULL, AES_ENCRYPT('$id', '$linea'), AES_ENCRYPT('$mov_ant', '$linea'), AES_ENCRYPT('$date', '$linea'))"; 
$resultado5 = mysqli_query($connect, $sql5);

if ($resultado) 
{
	//Una vez actualizada la persona debemos verificar que la tabla de updates no tenga el ID y si la tiene modicar éste
	$sqlVerify = "SELECT AES_DECRYPT(IdCaptura, '$linea') as IdCaptura FROM updates WHERE IdCaptura = AES_ENCRYPT('$id', '$linea')";
	$resultado3 = mysqli_query($connect, $sqlVerify);
	if($resultado3)
	{
		$datos = mysqli_fetch_assoc($resultado3);
		if($datos != null)
		{
			//Aquí metemosel update del ID

			$sql4 = "UPDATE updates SET SincroBridge = AES_ENCRYPT('0', '$linea'), FHupdate = AES_ENCRYPT('$date', '$linea') WHERE IdCaptura = AES_ENCRYPT('$id', '$linea')"; 
			$resultado4 = mysqli_query($connect, $sql4);
			
			$sql3 = "SELECT IdCaptura, Movilizador, Estado FROM personas WHERE IdCaptura = AES_ENCRYPT('$id', '$linea')";
			$resultado3 = mysqli_query($connect, $sql3);
			$array = mysqli_fetch_assoc($resultado3);
			$sending = array();
			foreach ($array as $key => $value) {
				$sending[$key] = base64_encode($value);
			}
			echo json_encode($sending);
		}else
		{
			//Aquí insertamos el ID
			$sql2 = "INSERT INTO updates (IdCaptura, SincroBridge, FHupdate) VALUES (AES_ENCRYPT('$id', '$linea'), AES_ENCRYPT('0', '$linea'), AES_ENCRYPT('$date', '$linea'))";	
			$resultado2 = mysqli_query($connect, $sql2);

			$sql3 = "SELECT IdCaptura, Movilizador, Estado FROM personas WHERE IdCaptura = AES_ENCRYPT('$id', '$linea')";
			$resultado3 = mysqli_query($connect, $sql3);
			$array = mysqli_fetch_assoc($resultado3);
			$sending = array();
			foreach ($array as $key => $value) {
				$sending[$key] = base64_encode($value);
			}
			echo json_encode($sending);
		}
	}
}else
{

}
?>