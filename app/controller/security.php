<?php 
	//Agregamos la consulta al modelo Usuarios
	session_start();
	if (isset($_SESSION['username']) && isset($_SESSION['pass'])) {

		$username = strtoupper($_SESSION['username']);
	    $pass = strtoupper($_SESSION['pass']);

	    //Llamada al modelo
		include $_SERVER['DOCUMENT_ROOT'].'/digivots/app/controller/rutalinea.php';
		include ($_SERVER['DOCUMENT_ROOT']."/digivots/app/models/usuario.model.php");
		$user = new usuarioSesion();
		$datos = $user->get_usuarios($username, $pass, $linea);
		
		if ($datos == null || $datos == '') {
		  	$link = 'location:../vista/login.php';
	  		header($link);
		}
	} else {
		session_destroy();
		$link = 'location:../vista/login.php';
	  	header($link);
	}

?>