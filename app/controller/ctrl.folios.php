<?php 
  include 'rutalinea.php';
  include 'conexion.php';
  ini_set('max_execution_time', 300);


  $sql = "SELECT AES_DECRYPT(IdCaptura, '$linea') as IdCaptura
    , AES_DECRYPT(NombreCaptura, '$linea') as NombreCaptura
    , AES_DECRYPT(PaternoCaptura, '$linea') as PaternoCaptura
    , AES_DECRYPT(MaternoCaptura, '$linea') as MaternoCaptura
    , AES_DECRYPT(Movilizador, '$linea') as Movilizador
    , AES_DECRYPT(Estado, '$linea') as Estado
  FROM personas
  ORDER BY Movilizador, PaternoCaptura, MaternoCaptura, NombreCaptura";  
  $resultado = mysqli_query($connect, $sql);

  $MaxFolios = 20;
  $Movilizador = '';
  $Folio = 0;
  $contadorFolios = 0;
  while($row = mysqli_fetch_assoc($resultado)) {
    if ($row['Estado'] != '0') {
      if ($row['Movilizador'] != $Movilizador) {
        $Movilizador = $row['Movilizador'];
        $Folio++;
        if ($Folio < 10) $Folio = '0'.$Folio;
        $contadorFolios = 0;
      }
      if ($contadorFolios >= $MaxFolios) {
        $Folio++;
        if ($Folio < 10) $Folio = '0'.$Folio;
        $contadorFolios = 0;
      }
      $contadorFolios++;
      //echo $Folio.'<br>';
      $IdCaptura = $row['IdCaptura'];
      $sql2 = "UPDATE personas SET Folio = AES_ENCRYPT('$Folio', '$linea') WHERE '$IdCaptura' = AES_DECRYPT(IdCaptura, '$linea')";
      $resultado2 = mysqli_query($connect, $sql2);
    } else {
      $IdCaptura = $row['IdCaptura'];
      $sql2 = "UPDATE personas SET Folio = AES_ENCRYPT('0', '$linea') WHERE '$IdCaptura' = AES_DECRYPT(IdCaptura, '$linea')";
      $resultado2 = mysqli_query($connect, $sql2);
    }
  }
?>