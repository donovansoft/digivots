<?php 
include '../controller/rutalinea.php';
include 'conexion.php'; 

$calle = $_POST['calle'];
$cruz1 = $_POST['cruz1'];
$cruz2 = $_POST['cruz2'];
$noext= $_POST['noext'];
$noint = $_POST['noint'];
$colonia= $_POST['colonia']; 
$municipio = $_POST['municipio'];
$manzana = $_POST['manzana'];
$exito = false;
if(isset($_POST['tel']) && isset($_POST['id']))
{ 
	if ($_POST['id'] != ''){
    	$tel = $_POST['tel'];
		$id = $_POST['id'];
	}
	$exito = true;
}
//Sentencia de actualizado para Personas
$sql = "UPDATE personas SET Telefono = AES_ENCRYPT('$tel', '$linea'), Calle = AES_ENCRYPT('$calle', '$linea'), Cruzamiento1  = AES_ENCRYPT('$cruz1', '$linea'), Cruzamiento2 = AES_ENCRYPT('$cruz2', '$linea'), Noext = AES_ENCRYPT('$noext', '$linea'), Noint = AES_ENCRYPT('$noint', '$linea'), Colonia = AES_ENCRYPT('$colonia', '$linea'), Municipio = AES_ENCRYPT('$municipio', '$linea'), Manzana = AES_ENCRYPT('$manzana', '$linea'), Estado = AES_ENCRYPT('2', '$linea') WHERE IdCaptura = AES_ENCRYPT('$id', '$linea')"; 
$resultado = mysqli_query($connect, $sql);


if ($resultado) 
{
	//Una vez actualizada la persona debemos verificar que la tabla de updates no tenga el ID y si la tiene modicar éste
	$sqlVerify = "SELECT AES_DECRYPT(IdCaptura, '$linea') as IdCaptura FROM updates WHERE IdCaptura = AES_ENCRYPT('$id', '$linea')";
	$resultado3 = mysqli_query($connect, $sqlVerify);
	if($resultado3)
	{
		$datos = mysqli_fetch_assoc($resultado3);
		if($datos != null)
		{
			//Aquí metemosel update del ID
			date_default_timezone_set('UTC');
			date_default_timezone_set('America/Mexico_City');
			$horas = strftime("%H");
			$minutos = strftime("%M");
			$hora = $horas.":".$minutos;
			$date = date('Y-m-d');
			$date = $date." ".$hora;
			$sql3 = "UPDATE updates SET SincroBridge = AES_ENCRYPT('0', '$linea'), FHupdate = AES_ENCRYPT('$date', '$linea') WHERE IdCaptura = AES_ENCRYPT('$id', '$linea')"; 
			$resultado3 = mysqli_query($connect, $sql3);
			/*if($resultado3)
			{
		    	echo json_encode($resultado3);
			}else {
				echo mysqli_error($connect);
			    echo json_encode($resultado3);
			}*/
			//echo utf8_decode("Actualización con éxito");
			$sql3 = "SELECT IdCaptura, Telefono, Calle, Cruzamiento1, Cruzamiento2, Noext, Noint, Colonia, Municipio, Manzana, Movilizador, Estado FROM personas WHERE IdCaptura = AES_ENCRYPT('$id', '$linea')";
			$resultado3 = mysqli_query($connect, $sql3);
			$array = mysqli_fetch_assoc($resultado3);
			$sending = array();
			foreach ($array as $key => $value) {
				$sending[$key] = base64_encode($value);
			}
			echo json_encode($sending);
		}else
		{
			//Aquí insertamos el ID
			$sql2 = "INSERT INTO updates (IdCaptura, SincroBridge, FHupdate) VALUES (AES_ENCRYPT('$id', '$linea'), AES_ENCRYPT('0', '$linea'), AES_ENCRYPT('$date', '$linea'))";
			$resultado2 = mysqli_query($connect, $sql2);
			/*if($resultado2)
			{
		    	echo json_encode($resultado2);
			}else {
				echo mysqli_error($connect);
			    echo json_encode($resultado2);
			}*/
			//echo utf8_decode("Inerción con éxito");
			$sql3 = "SELECT IdCaptura, Telefono, Calle, Cruzamiento1, Cruzamiento2, Noext, Noint, Colonia, Municipio, Manzana, Movilizador, Estado FROM personas WHERE IdCaptura = AES_ENCRYPT('$id', '$linea')";
			$resultado3 = mysqli_query($connect, $sql3);
			$array = mysqli_fetch_assoc($resultado3);
			$sending = array();
			foreach ($array as $key => $value) {
				$sending[$key] = base64_encode($value);
			}
			echo json_encode($sending);
		}	
	}
} else {
	echo mysqli_error($connect);
    echo json_encode($resultado);
}

?>