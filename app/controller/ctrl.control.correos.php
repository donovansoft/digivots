<?php 

  include '../controller/rutalinea.php';

  $perfil = $_POST['perfil']; 
  $Orden = $_POST['Orden'];
  $correoSeleccionado = $_POST['correo']; 
  $compSelect = "";
  if ($perfil == "1") {
  //PERFIL 1
    $perfiln = "LIDERAZGO";
    if ($Orden == "1") {
      $compSelect="ORDER BY Calle, Noint, Cruzamiento1, Cruzamiento2, PaternoCaptura, MaternoCaptura, NombreCaptura"; 
    } else if ($Orden == "2") {
      $compSelect="ORDER BY PaternoCaptura, MaternoCaptura, NombreCaptura"; 
    }
  } else if ($perfil == "2") {
  //PERFIL 2 PENDIENTE POR TERMINAR************PREGUNTA A JAZIR*****************
    $perfiln = "OPERADOR";
    $Clave = $_POST['Clave'];
    if ($Orden == "1") {
      $compSelect="AND Movilizador = AES_ENCRYPT('$Clave', '$linea') ORDER BY Casilla, NombreCaptura, PaternoCaptura, MaternoCaptura"; 
    } else if ($Orden == "2") {
      $compSelect="AND Movilizador = AES_ENCRYPT('$Clave', '$linea') ORDER BY Movilizador, NombreCaptura, PaternoCaptura, MaternoCaptura"; 
    }
  } else if ($perfil == "3") {
  //PERFIL 3
    $perfiln = "MOVILIZADOR";
    $Clave = $_POST['Clave'];
    $compSelect="AND Movilizador = AES_ENCRYPT('$Clave', '$linea') ";
    if ($Orden == "1") {
      $compSelect = $compSelect." ORDER BY Calle, Noint, Cruzamiento1, Cruzamiento2, PaternoCaptura, MaternoCaptura, NombreCaptura"; 
    } else if ($Orden == "2") {
      $compSelect = $compSelect." ORDER BY PaternoCaptura, MaternoCaptura, NombreCaptura"; 
    }
  } else if ($perfil == "4") {
  //PERFIL 4
    $perfiln = "RC";
    if ($Orden == "1") {
      $compSelect="ORDER BY Casilla, PaternoCaptura, MaternoCaptura, NombreCaptura"; 
    } else if ($Orden == "2") {
      $compSelect="ORDER BY Casilla, Calle, Noint, Cruzamiento1, Cruzamiento2, PaternoCaptura, MaternoCaptura, NombreCaptura"; 
    }
  } else if ($perfil == "5") {
  //PERFIL 5
    $perfiln = "VISITAS";
    if ($Orden == "1") {
      $compSelect="ORDER BY Manzana, Calle, Noint, Cruzamiento1, Cruzamiento2, PaternoCaptura, MaternoCaptura, NombreCaptura"; 
    } else if ($Orden == "2") {
      $compSelect="ORDER BY Colonia, Calle, Noint, Cruzamiento1, Cruzamiento2, PaternoCaptura, MaternoCaptura, NombreCaptura"; 
    }
  } else if ($perfil == "6") {
  //PERFIL 6
    $perfiln = "MOVILIDAD";
    if ($Orden == "1") {
      $compSelect="ORDER BY Municipio, Colonia, Calle, Noint, Cruzamiento1, Cruzamiento2, PaternoCaptura, MaternoCaptura, NombreCaptura"; 
    } else if ($Orden == "2") {
      $compSelect="ORDER BY Municipio, Seccion, Calle, Noint, Cruzamiento1, Cruzamiento2, PaternoCaptura, MaternoCaptura, NombreCaptura"; 
    }
  } else if ($perfil == "7") {
  //PERFIL 7
    $perfiln = "COMPLETO";
    if ($Orden == "1") {
      $compSelect="ORDER BY PaternoCaptura, MaternoCaptura, NombreCaptura"; 
    } else if ($Orden == "2") {
      $compSelect="ORDER BY Seccion, Casilla, PaternoCaptura, MaternoCaptura, NombreCaptura"; 
    }
  }
 
include '../controller/rutalinea.php'; 
include ("../models/conexion.php"); 
include '../controller/security.php'; 
include '../controller/ctrl.sesion.seccion.php'; 
require '../../assets/fpdf/fpdf.php';
include 'conexion.php';  
 
$sql = "SELECT  
  AES_DECRYPT(IdCaptura, '$linea') as IdCaptura 
, AES_DECRYPT(NombreCaptura, '$linea') as NombreCaptura 
, AES_DECRYPT(PaternoCaptura, '$linea') as PaternoCaptura 
, AES_DECRYPT(MaternoCaptura, '$linea') as MaternoCaptura 
, AES_DECRYPT(FechaNacimiento, '$linea') as FechaNacimiento 
, AES_DECRYPT(Telefono, '$linea') as Telefono 
, AES_DECRYPT(Calle, '$linea') as Calle 
, AES_DECRYPT(Cruzamiento1, '$linea') as Cruzamiento1 
, AES_DECRYPT(Cruzamiento2, '$linea') as Cruzamiento2 
, AES_DECRYPT(Noext, '$linea') as Noext 
, AES_DECRYPT(Noint, '$linea') as Noint 
, AES_DECRYPT(Colonia, '$linea') as Colonia 
, AES_DECRYPT(Municipio, '$linea') as Municipio 
, AES_DECRYPT(Seccion, '$linea') as Seccion 
, AES_DECRYPT(Manzana, '$linea') as Manzana 
, AES_DECRYPT(Casilla, '$linea') as Casilla 
, AES_DECRYPT(Movilizador, '$linea') as Movilizador 
, AES_DECRYPT(Folio, '$linea') as Folio 
, AES_DECRYPT(Orden, '$linea') as Orden 
, AES_DECRYPT(CheckLocal, '$linea') as CheckLocal 
, AES_DECRYPT(SincroBridge, '$linea') as SincroBridge 
, AES_DECRYPT(Estado, '$linea') as Estado 
FROM personas 
WHERE CheckLocal = AES_ENCRYPT('0', '$linea') AND Seccion = AES_ENCRYPT('$seccion', '$linea') 
$compSelect"; 

$contador = 0;
$resultado = mysqli_query($connect, $sql);
$pdf=new FPDF('L','mm','A4');
while($row = mysqli_fetch_assoc($resultado))
{
  if($contador == 20 || $contador == 0){
    $pdf->AddPage();
    //creacion de celdas
    $pdf->SetFont('Arial','B',10);
    $pdf->Cell(275, 8, 'PERFIL: '.$perfiln.', Orden: '.$Orden, 1, 1,'C');

    $pdf->SetFont('Arial','B',9);
    $pdf->Cell(20, 10, 'ID', 0, 0,'C');
    $pdf->Cell(105, 10, 'NOMBRE', 0, 0,'C');
    $pdf->Cell(25, 10, 'NACIMIENTO', 0, 0,'C');
    $pdf->Cell(115, 10, 'DIRECCION', 0, 1,'C');
    $contador = 1;
  }
  //$pdf->AddPage();
  $nombre_completo = $row['PaternoCaptura'].' '.$row['MaternoCaptura'].' '.$row['NombreCaptura'];
  //genera direccion completa
  $direccion_completa = 'C.'.$row['Calle'].' #'.$row['Noext'].' Col.'.$row['Colonia'];

  $pdf->SetFont('Arial','',11);
  $pdf->Cell(20, 8, $row['IdCaptura'], 1, 0,'C');
  $pdf->Cell(105, 8, $nombre_completo, 1, 0,'C');
  $pdf->Cell(25, 8, $row['FechaNacimiento'], 1, 0,'C');
  $pdf->Cell(115, 8, $direccion_completa, 1, 0,'C');
  $pdf->Cell(10, 8, "", 1, 1,'C');
  $contador++;
  
}

//hora de creacion
    date_default_timezone_set('UTC');
    date_default_timezone_set('America/Mexico_City');
    $horas = strftime("%I");
    $minutos = strftime("%M");
    $hora = $horas."Hrs".$minutos."Mint";
    $name_file = $perfiln."-".$hora;
    //guardar el pdf
    $pdf->Output('F', '../reportes/archivos/'.$name_file.'.pdf');



   require '../../assets/PHPMailer/src/Exception.php';
   require '../../assets/PHPMailer/src/PHPMailer.php';
   require '../../assets/PHPMailer/src/SMTP.php';

   //hasta este punto ya tenemos el documento y procedemos a preparar el envio
$mail = new PHPMailer\PHPMailer\PHPMailer();
 
$mail->SMTPDebug = 3;                               // Enable verbose debug output
 
$mail->isSMTP();                                      // Set mailer to use SMTP
$mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
$mail->SMTPAuth = true;                               // Enable SMTP authentication
$mail->Username = 'correito.fake@gmail.com';                 // SMTP username
$mail->Password = 'farito007';                           // SMTP password
$mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
$mail->Port = 465;                                    // TCP port to connect to
 
$mail->setFrom($correoSeleccionado, 'Solicitud de Lista');
$mail->addAddress($correoSeleccionado, 'Solicitud de Lista');
 
//Attachments
//$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
$mail->addAttachment('../reportes/archivos/'.$name_file.'.pdf', $name_file.'.pdf');    // Optional name     
 
$mail->Subject = 'SOLICITUD DE LISTAS';
$mail->Body    = "Lista solicitada:".$perfiln;
 
if(!$mail->send()) {
    //echo 'Error, mensaje no enviado';
    //echo 'Error del mensaje: ' . $mail->ErrorInfo;
} else {
    //echo 'El mensaje se ha enviado correctamente';
    echo "true";
 }   

//echo "true";

?>