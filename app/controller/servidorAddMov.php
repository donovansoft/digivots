<?php
include 'rutalinea.php';
include 'conexion.php';
include ("../models/conexion.php");
include 'security.php';
include 'ctrl.sesion.seccion.php';

//$seccion = 1;

$NombreMovilizador = strtoupper($_POST['nombre']);
$PaternoMovilizador = strtoupper($_POST['paterno']);
$MaternoMovilizador = strtoupper($_POST['materno']);
$TelefonoMovilizador = strtoupper($_POST['telefono']);
//echo "NOMBRE: ".$NombreMovilizador.' '.$PaternoMovilizador.' '.$MaternoMovilizador.' TEL: '.$TelefonoMovilizador.' Seccion: '.$seccion;

/********************NOMENCLATURA PARA CLAVES DE MOVILIZADORES ********************/
//SEGUN SECCION
$nomIzq = "";
if ($seccion < 10) {
	//echo "esta entre 1 y 9".'<br>';
	$nomIzq = "000".$seccion;
	//echo $nomIzq;
}
elseif ($seccion < 100) {
	//echo "esta entre 10 y 99".'<br>';
	$nomIzq = "00".$seccion;
	//echo $nomIzq;
}
elseif ($seccion < 1000) {
	//echo "esta entre 100 y 999".'<br>';
	$nomIzq = "0".$seccion;
	//echo $nomIzq;
}
elseif ($seccion < 10000) {
	//echo "esta entre 1000 y 9999".'<br>';
	$nomIzq = $seccion;
	//echo $nomIzq;
}
// ********* SEGUN ULTIMO ***********
//echo '<br>';
$nomDer = "";
$sql = "SELECT COUNT(ClaveMovilizador) AS num_movilizadores FROM movilizadores WHERE SeccionMovilizador = AES_ENCRYPT('$seccion', '$linea')";
$resultado = mysqli_query($connect, $sql);
while($row = mysqli_fetch_assoc($resultado))
{
	$num_movilizadores = $row['num_movilizadores'];
	//echo $num_movilizadores;
}
if ($num_movilizadores < 10) {
	//echo "son menos de 10".'<br>';
	$nomDer = "0".$num_movilizadores;
	//echo $nomDer;
}
elseif ($num_movilizadores >= 10)
{
	//echo "son 10 o mas".'<br>';
	$nomDer = $num_movilizadores;
	//echo $nomDer;
}
//echo '<br>';
$ClaveMovilizador = $nomIzq.$nomDer;
//echo "Clave final: ".$ClaveMovilizador;
/******************** FIN NOMENCLATURA PARA CLAVES DE MOVILIZADORES ********************/

$sql = "INSERT INTO movilizadores (ClaveMovilizador, NombreMovilizador, PaternoMovilizador, MaternoMovilizador, TelefonoMovilizador, SeccionMovilizador) VALUES (AES_ENCRYPT('$ClaveMovilizador', '$linea'), AES_ENCRYPT('$NombreMovilizador', '$linea'), AES_ENCRYPT('$PaternoMovilizador', '$linea'), AES_ENCRYPT('$MaternoMovilizador', '$linea'), AES_ENCRYPT('$TelefonoMovilizador', '$linea'), AES_ENCRYPT('$seccion', '$linea'))";
$resultado = mysqli_query($connect, $sql);

if ($resultado) {
	# code...
	echo'<script type="text/javascript">
    alert("Movilizador Agregado");
    window.location.href="../vistaMantenimiento/index.php";
    </script>';
}
else
{
	echo'<script type="text/javascript">
    alert("No se agregar");
    window.location.href="../vistaMantenimiento/index.php";
    </script>';
}



?>
