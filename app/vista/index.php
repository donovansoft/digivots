<?php 
	include ("../models/conexion.php");
	include '../controller/security.php';
	//include '../controller/security.test.php';
	include '../models/personas.model.php';
	//include '../controller/ctrl.sesion.seccion.php';
?>

<!DOCTYPE html>
<html>
<head>
	<title>Inicio</title>
	<?php include 'head.php'; ?>

</head>
<body onload="cargaFunction()">
	<div id="loader"></div>
	<main id="body-content" class="animate-bottom">
		<!-- Inicio Navbar -->
		<div>
			<?php include 'barranav.php'; ?>
		</div>
		<br>
		<!-- Fin Navbar -->
		<!-- Inicio Contenedor -->
		<div class="container">
			<center><h5><?php echo $username; ?></h5></center>
			<br><br>
			<br><br>
			<form method="POST" action="../controller/redirecciona.etapa.php">
			<div class="row">
				<div class="col-6">
					<label>Sección: </label>
					<select class="select-folio col-12 form-control" name="seccion" required>
						<option selected disabled>Seleccionar Sección</option>
						<?php 
							$val = 'Seccion';
							$personas = new personasModel();
							$resultado = $personas->get_seccion($val, $linea, $val, $val);
	  						foreach ($resultado as $result) {
						?>

							<option value="<?php echo $result['Seccion']; ?>"><?php echo $result['Seccion']; ?></option>
						<?php 
							}
						?>

					</select>
				</div>
				<div class="col-6">
					<label>Etapa: </label>
					<select class="select-folio col-12 form-control" name="etapa" required>
						<!-- <option selected disabled>Seleccionar Etapa</option> -->
						<!-- <option value="1">Mantenimiento</option> -->
						<option value="2" selected="">Día de</option>
					</select>
				</div>
			</div>	
			<br><br>
			<center><input type="submit" value="Siguiente" class="btn btn-primary"></center>
			</form>
		</div>
		<!-- Fin Contenedor -->
		</main>
</body>
</html>