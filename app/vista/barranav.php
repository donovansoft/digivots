<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <!-- <a class="navbar-brand" href="../vista/index.php">Inicio</a> -->
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
    </ul>    
  </div>
  <div class="dropdown">
    <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown"><span class="fa fa-cogs"></span> Operaciones
    <span class="caret"></span></button>
    <div class="dropdown-menu">
      <a class="dropdown-item" href="../vistaDiade/actualizarSeccion.php"><span class="fa fa-sign-out"></span>  Configuración</a>
      <a class="dropdown-item" href="../controller/salir.php"><span class="fa fa-sign-out"></span>  Salir</a>
    </div>
  </div>
</nav>