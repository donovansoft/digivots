<!DOCTYPE html>
<html lang="es">
<head>
  <title>Login</title>
  <?php include 'head.php'; ?>
<script>
	function actualiza() {
		window.open('buscaractualizaciones.php');
		alert("ESPERA A QUE SE CIERRE LA VENTANA NEGRA");
	}
</script>
</head>
<body>
	<br>
	<div class="container text-right">
		<!-- <button class="btn btn-info" onclick="actualiza()">Actualzar versión</button> -->
	</div>
	<div class="container">
	 	<div class="row d-flex justify-content-center">
	 		<div class="col-6 card card-container mt-5">
        <p id="profile-name" class="profile-name-card"></p>
        <form method="post" accept-charset="utf-8" action="../controller/ctrl.login.php" name="loginform" autocomplete="off" role="form" class="form-signin py-3">
				<?php
					// show potential errors / feedback (from login object)
					if (isset($login)) {
						if ($login->errors) {
							?>
							<div class="alert alert-danger alert-dismissible" role="alert">
							    <strong>Error!</strong> 
							
							<?php 
							foreach ($login->errors as $error) {
								echo $error;
							}
							?>
							
							</div>
							<?php
						}
						if ($login->messages) {
							?>

							<div class="alert alert-success alert-dismissible" role="alert">
							    <strong>Aviso!</strong>
							<?php
							foreach ($login->messages as $message) {
								echo $message;
							}
							?>

							</div> 
							<?php 
						}
					}
				?>
        <span id="reauth-email" class="reauth-email"></span>
        <div class="col-12 d-flex flex-column mb-2">
        	<label for="username">Usuario</label>
        	<input class="form-control"  id="username" placeholder="Usuario" name="username" type="text" value="" autofocus="" required>
        </div>
        <div class="col-12 d-flex flex-column mb-4">
        	<label for="username">Contraseña</label>
        	<input class="form-control" id="pass" placeholder="Contraseña" name="pass" type="password" value="" autocomplete="off" required>
        </div>
        <div class="col-12 text-right">
        	<center><button type="submit" class="btn btn-success" name="login" id="submit">Iniciar Sesión</button></center>
        </div>
        <br>  
   			<?php
					if(isset($_GET['error'])) {
				?>

				<div class="alert alert-danger alert-dismissible" role="alert">
				  <strong>Usuario o contraseña invalidos, por favor inténtalo de nuevo!</strong>
				</div> 
				<?php 
					}
        ?>
            
        </form>
      </div>
	 	</div>
	</div>
 </body>
</html>