<?php
//ESTE ARCHIVO CARGA EL CSV seccion ubicado en C, en la tabla personas
include '../models/conexion.php';
include '../controller/security.php';?>

<!DOCTYPE html>
<html>
<head>
	<title>Excel</title>
	<?php include 'head.php'; ?>

</head>
<body>
	<!-- Inicio Navbar -->
	<div>
		<?php include 'barranav.php'; ?>
	</div>
	<br>
	<!-- Fin Navbar -->
	<!-- Inicio Contenedor -->
	<div class="container">
	<?php
	include '../controller/rutalinea.php';
    include '../controller/conexion.php';
    if (($handle = fopen("C:\seccion.csv", "r")) !== FALSE) {
      $i = 0;
      $data2DArray = array();
      while (($lineArray = fgetcsv($handle, 0, ",")) !== FALSE) {
        for ($j = 0; $j < count($lineArray); $j++) {
          $data2DArray[$i][$j] = $lineArray[$j];
        }
        $i++;
      } fclose($handle);
    }
    $continsert = 0;
    $contupdate = 0;

    foreach($data2DArray as $a=>$b){
      
        foreach($b as $c=>$d){
          if ($c == 0)  {
            $IdCaptura = $d;
            //echo $IdCaptura."&emsp;";
          }
          if ($c == 1)  {
            $IdPersona = $d;
            //echo $IdCaptura."&emsp;";
          }
          $IdExterno = 0;

          /*if ($c == 1)  {
            $Orden = $d;
            //echo $Orden."&emsp;";
          }*/
          $Orden = 0;
          if ($c == 3)  {
            $PaternoCaptura = $d;
            //echo $PaternoCaptura."&emsp;";
          }
          if ($c == 4)  {
            $MaternoCaptura = $d;
            //echo $MaternoCaptura."&emsp;";
          }
          if ($c == 2) {
            $NombreCaptura = $d;
            //list($dia, $mes, $año) = explode("/", $d);
            //$fecha = $año.'-'.$mes.'-'.$dia;
            //$fecha = $dia.'/'.$mes.'/'.$año;
            //echo $NombreCaptura."&emsp;";
          }
          if ($c == 6) {
            $tel = $d;
            //echo $Calle."&emsp;";
          }
          /*if ($c == 5) {
            list($dia, $mes, $año) = explode("/", $d);
            $FechaNacimiento = $dia.'/'.$mes.'/'.$año;
            //echo $FechaNacimiento."&emsp;";
          }*/
          $FechaNacimiento = "";
          if ($c == 7) {
            $Calle = $d;
            //echo $Calle."&emsp;";
          }
          if ($c == 8) {
            $Cruzamiento1 = $d;
            //if($num_Int != ""){$numInt= $num_Int;}else{$numInt="";}
            //echo $Cruzamiento1."&emsp;";
          }
          if ($c == 9) {
            $Cruzamiento2 = $d;
            //if($col=="#¿NOMBRE?"){$colonia="";}else{$colonia=$col;}
            //echo $Cruzamiento2."&emsp;";
          }
          if ($c == 10){
            $Noext = $d;
            //echo $Noext."&emsp;";
          }
          if ($c == 11) {
            $Noint = $d;
            //echo $Noint."&emsp;";
          }
          if ($c == 12) {
            $Colonia = $d;
            //echo $Colonia."&emsp;";
          }
          if ($c == 13) {
            $Municipio = $d;
            //echo $Municipio."&emsp;";
          }
          if ($c == 14) {
            $Seccion = $d;
            //echo $Seccion."&emsp;";
          }
          if ($c == 15) {
            $Manzana = $d;
            //echo $Manzana."&emsp;";
          }
          if ($c == 16) {
            $Casilla = $d;
            //echo $Casilla."&emsp;";
          }
          if ($c == 17) {
            $no_mov = $d;
            if($no_mov==""){$Movilizador="0000";}elseif($no_mov==" "){$Movilizador="0000";}elseif($no_mov=="0"){$Movilizador="0000";}else{$Movilizador=$no_mov;}
            //echo $Movilizador."&emsp;";
          }
        }

        //$direccion = $calle." , ".$numExt." ".$numInt.", ".$colonia." MNZ ".$mnz;
        //$tel = '0000000000';
        //$folio = '0';
        $default = '0';

        ini_set('max_execution_time', 14000);
        
        $sql = "INSERT INTO personas 
        (
            IdCaptura
          , IdPersona
          , NombreCaptura
          , PaternoCaptura
          , MaternoCaptura
          , FechaNacimiento
          , Telefono, Calle
          , Cruzamiento1
          , Cruzamiento2
          , Noext, Noint
          , Colonia
          , Municipio
          , Seccion
          , Manzana
          , Casilla
          , Movilizador
          , Folio
          , Orden
          , CheckLocal
          , SincroBridge
          , Estado
          , IdExterno
        ) 
        VALUES (
            AES_ENCRYPT('$IdCaptura', '$linea')
          , AES_ENCRYPT('$IdPersona', '$linea')
          , AES_ENCRYPT('$NombreCaptura', '$linea')
          , AES_ENCRYPT('$PaternoCaptura', '$linea')
          , AES_ENCRYPT('$MaternoCaptura', '$linea')
          , AES_ENCRYPT('$FechaNacimiento', '$linea')
          , AES_ENCRYPT('$tel', '$linea')
          , AES_ENCRYPT('$Calle', '$linea')
          , AES_ENCRYPT('$Cruzamiento1', '$linea')
          , AES_ENCRYPT('$Cruzamiento2', '$linea')
          , AES_ENCRYPT('$Noext', '$linea')
          , AES_ENCRYPT('$Noint', '$linea')
          , AES_ENCRYPT('$Colonia', '$linea')
          , AES_ENCRYPT('$Municipio', '$linea')
          , AES_ENCRYPT('$Seccion', '$linea')
          , AES_ENCRYPT('$Manzana', '$linea')
          , AES_ENCRYPT('$Casilla', '$linea')
          , AES_ENCRYPT('$Movilizador', '$linea')
          , AES_ENCRYPT('$default', '$linea')
          , AES_ENCRYPT('$Orden', '$linea')
          , AES_ENCRYPT('$default', '$linea')
          , AES_ENCRYPT('$default', '$linea')
          , AES_ENCRYPT('1', '$linea')
          , AES_ENCRYPT('$IdExterno', '$linea')
        )";
        //$sql = "INSERT INTO personas(IdCaptura) VALUES(AES_ENCRYPT('$IdCaptura', '$linea'))";
        $Result = mysqli_query($connect, $sql);
        if($Result)
        {
          echo "nice";
        }
        else{
          echo mysqli_error($connect);
          //echo "nope";
        }

        //echo "<br>"; 
    }
    //echo "<br>";
    ?>
	</div>
	<!-- Fin Contenedor -->
</body>
</html>