<?php 
include '../controller/rutalinea.php';
include '../controller/conexion.php';
include '../controller/security.php';
?>
<!DOCTYPE html>
<html>
<head>
	<title>Mapas</title>
	<?php include 'head.php'; ?>
	<script type="text/javascript">
		function mostrarMapa(){
			var seleccion = $("#sec").val();
			//alert(seleccion);
			if (seleccion != null)
			{
				window.open("../mapas/test_"+seleccion+".pdf","_blank");
			}
			else
			{
				alert("Debe seleccionar una sección");
			}
			//alert($("#sec").val());
			//window.open("../mapas/test_"+$("#sec").val()+".pdf","_blank");
		}
	</script>
</head>
<body>
<?php include 'barranav.php'; ?>
	<div id="divPrincipal" style="margin: 3rem;" class="card">
		<div class="col-12 col-md-12 col-lg-12 d-flex flex-column align-items-center justify-content-center">
			<h4 class="w-100 text-center pb-2">Impresión de Mapas</h4>
			<select class='clave-movilizador form-control col-4' style="height: 5rem;" id="sec" required>
				<option selected disabled value="0">Seleccionar una sección:</option>
			<?php
				$sql = "SELECT AES_DECRYPT(Seccion, '$linea') as secciones FROM personas GROUP BY Seccion ORDER BY Seccion";
    			$resultado = mysqli_query($connect, $sql);
				while($row = mysqli_fetch_assoc($resultado)) {
			?>
				<option value="<?php echo $row['secciones'] ?>"><?php echo $row['secciones'] ?></option>
			<?php
				} 
			?>
			</select>
			<h1></h1><h1></h1><h1></h1>
			<input type="submit" name="abre" value="Abrir" class="btn btn-success" onclick="mostrarMapa()">
		</div>
	</div>
</body>
</html>