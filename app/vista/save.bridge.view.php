<!DOCTYPE html>
<html>
<head>
	<title>Actualización de bridge</title>
	<?php include 'head.php'; ?>
  <script src="/digivots/js/getBridge.js"></script>
</head>
<body>
<?php 
	include 'barranav.php'; 
	include ("../models/conexion.php");
	include '../controller/security.php';
	include '../controller/conexionBridge.php';
	include '../panel/controllerPanel/ctr.select.seccion.Bridge.php';
	$arreglo = array();
	while($row = mysqli_fetch_assoc($resultadosec)) {
		if($row['Seccion'] != "")
		{
			array_push($arreglo, $row);
		}
	}
?>
	<div class="container" style="margin-top: 3rem;">
		<div class="row justify-content-center align-items-center">
			<h2>Carga de datos del bridge</h2>
			<h4 style="color: red;"></h4>
		</div>
		<div  class="row flex-row justify-content-center align-items-center mt-3">
			
			<div class="col-4 d-flex flex-column justify-content-center">
				<label for="inicio" class="text-center g-font-weight-700">Sección Inicio</label>
				<select name="inicio" id="inicio" class="form-control">
					<option value="0" selected="" disabled="">Selecciona una sección de inicio</option>
			<?php 
				foreach ($arreglo as $variable) {
					foreach ($variable as $key => $value) {
			?>
						<option value="<?php echo $value ?>"><?php echo $value ?></option>
			<?php 
					}
				}
			?>			
				</select>
			</div>
			<div class="col-4 d-flex flex-column justify-content-center">
				<label for="fin" class="text-center g-font-weight-700">Sección Fin</label>
				<select name="fin" id="fin" class="form-control">
					<option value="0" selected="" disabled="">Selecciona una sección de fin</option>
			<?php 
				foreach ($arreglo as $variable) {
					foreach ($variable as $key => $value) {
			?>
						<option value="<?php echo $value ?>"><?php echo $value ?></option>
			<?php 
					}
				}
			?>	
				</select>
			</div>

			

			<div class="col-6 d-flex justify-content-center align-items-center">
				<a class="btn btn-success mt-3 " id="btn-bridge"  href="#">Actualizar</a>
			</div>

		</div>

	</div>
</body>
</html>
