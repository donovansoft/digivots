<?php
include '../controller/conexion.php';
include '../controller/rutalinea.php';
include ("../models/conexion.php");
include '../controller/security.php';
?>

<!DOCTYPE html>
<html>
<head>
<title>Sincronizacion</title>
<?php 
	include '../vistaMantenimiento/head.php'; 
/*  $ip = "sysreg.tech";
  $output = shell_exec("ping $ip");
  $conexion = "";
  if (strpos($output, "recibidos = 0")) {
      //$conexion='No Conectado';
  } else {
      $conexion = 'Conectado';
  } 
  echo $conexion;*/
?>
<script>
$( document ).ready(function() {
	$("#sinc").on('click', function(){
    $.ajax({
    	url: "http://sysreg.tech/bridge/funciones/update_vots.php",
        type: "POST",
        dataType: "JSON",
        data: {
                u : 'syscam.consulta',
                k : 'cnslt140u4sscm'
              },
        success : function () {
          sincro();
          location.reload();
        }
        ,error : function () {
          alert('No puedes sincronizar, no tienes conexion al servidor');
        }
    });
	});
});

<?php
	$JSON = "";
	$sql = "SELECT
		TO_BASE64(updates.IdCaptura) AS IdCaptura,
		TO_BASE64(personas.Telefono) as Telefono,
		TO_BASE64(personas.Calle) AS Calle,
		TO_BASE64(personas.Cruzamiento1) AS Cruzamiento1,
		TO_BASE64(personas.Cruzamiento2) AS Cruzamiento2,
		TO_BASE64(personas.Noext) AS Noext,
		TO_BASE64(personas.Noint) AS Noint,
		TO_BASE64(personas.Colonia) AS Colonia,
		TO_BASE64(personas.Municipio) AS Municipio,
		TO_BASE64(personas.Manzana) AS Manzana,
		TO_BASE64(personas.Movilizador) AS Movilizador,
		TO_BASE64(personas.Estado) AS Estado,
		AES_DECRYPT(updates.IdCaptura, '$linea') as id
	FROM updates INNER JOIN personas ON updates.IdCaptura = personas.IdCaptura
	WHERE updates.SincroBridge = AES_ENCRYPT('0', '$linea')";
	$resultado = mysqli_query($connect, $sql);
	while($row = mysqli_fetch_assoc($resultado)) {
		$JSON .= json_encode($row) . ",";
	}
	$JSON = trim($JSON, ",");

	$JSON2 = "";
	$sql = "SELECT
		TO_BASE64(eliminados.IdCaptura) AS IdCaptura,
		TO_BASE64(personas.Movilizador) AS Movilizador,
		TO_BASE64(personas.Estado) AS Estado,
		AES_DECRYPT(eliminados.IdCaptura, '$linea') as id
	FROM eliminados INNER JOIN personas ON eliminados.IdCaptura = personas.IdCaptura
	WHERE eliminados.SincroBridge = AES_ENCRYPT('0', '$linea')";
	$resultado = mysqli_query($connect, $sql);
	while($row = mysqli_fetch_assoc($resultado)) {
		$JSON2 .= json_encode($row) . ",";
	}
	$JSON2 = trim($JSON2, ",");

?>
	var $json_data = <?php echo "[".$JSON."]" ?>;
	var $json_data2 = <?php echo "[".$JSON2."]" ?>;

	function sincro() {
	$.each($json_data, function(index, value){
		$.ajax({
			url : 'http://sysreg.tech/bridge/funciones/update_vots.php',
			type : 'POST',
			dataType : 'JSON',
			data : {
				u : 'syscam.consulta',
				k : 'cnslt140u4sscm',
				IdCaptura : value.IdCaptura,
				Telefono : value.Telefono,
				Calle : value.Calle,
				Cruzamiento1 : value.Cruzamiento1,
				Cruzamiento2 : value.Cruzamiento2,
				Noext : value.Noext,
				Noint : value.Noint,
				Colonia : value.Colonia,
				Municipio : value.Municipio,
				Manzana : value.Manzana,
				Movilizador : value.Movilizador,
				Estado : value.Estado
			},
			success : function(response){
				console.log(response);
				if (response.estatus == "INSERTADO" || response.estatus == "ACTUALIZADO") {
					$.ajax({
						url : '../controller/ctrl.update.bridge.movilizador2.php',
						type : 'POST',
						dataType : 'JSON',
						data : {
							u : 'syscam.consulta',
							k : 'cnslt140u4sscm',
							IdCaptura : value.id
						},
						success : function(response){
							if (response == true) {
								//alert("SINCRONIZADO");
							}
							console.log(response);
						}
					});
				}
			}
		});	
	});


	$.each($json_data2, function(index, value){
		$.ajax({
			url : 'http://sysreg.tech/bridge/funciones/update_vots.php',
			type : 'POST',
			dataType : 'JSON',
			data : {
				u : 'syscam.consulta',
				k : 'cnslt140u4sscm',
				IdCaptura : value.IdCaptura,
				Movilizador : value.Movilizador,
				Estado : value.Estado
			},
			success : function(response){
				console.log(response);
				if (response.estatus == "INSERTADO" || response.estatus == "ACTUALIZADO") {
					$.ajax({
						url : '../controller/ctrl.update.bridge.delete.php',
						type : 'POST',
						dataType : 'JSON',
						data : {
							u : 'syscam.consulta',
							k : 'cnslt140u4sscm',
							IdCaptura : value.id
						},
						success : function(response){
							if (response == true) {
								//alert("SINCRONIZADO");
							}
							console.log(response);
						}
					});
				}
			}
		});
	});
	alert("SE HAN SINCRONIZADO LOS REGISTROS");
  }

</script>
</head>
<body onload="cargaFunction()">
	<div id="loader"></div>
	<main id="body-content" class="animate-bottom">
		<!-- Inicio Navbar -->
		<?php include '../vistaMantenimiento/barranav.php'; ?>
		<!-- Fin Navbar -->
		<!-- Inicio Contenedor -->
		<div class="container">
			<br>
			<center><h1>Sincronización</h1></center>
		</div>
		<br><br>
		 <center><button class="btn btn-primary" id="sinc">SINCRONIZAR</button></center>
		<!-- Fin Contenedor -->
	</main>
</body>
</html>
