<?php 
  include ("../models/conexion.php");
  include '../controller/security.php';
  include '../controller/ctrl.sesion.seccion.php';
  include 'head.php';
?>
<!DOCTYPE html>
<html>
<head>
  <title></title>
  <style type="text/css">
  
    #popup {
        background-color: rgba(0,0,0,0.8);
        position: fixed;
        top:0;
        left:0;
        right:0;
        bottom:0;
        margin:0;
        -webkit-animation:autopopup 2s;
        -moz-animation:autopopup 2s;
        animation:autopopup 2s;
    }
    @-webkit-keyframes autopopup {
      from {opacity: 0;margin-top:-200px;}
      to {opacity: 1;}
    }
    @-moz-keyframes autopopup {
      from {opacity: 0;margin-top:-200px;}
      to {opacity: 1;}
    }
    @keyframes autopopup {
      from {opacity: 0;margin-top:-200px;}
      to {opacity: 1;}
    }

    #popup:target {
      -webkit-transition:all 1s;
      -moz-transition:all 1s;
      transition:all 1s;
      opacity: 0;
      visibility: hidden;
    }

    .popup-contenedor {
      position: relative;
      margin:2% auto;
      padding:30px 50px;
      background-color: #fafafa;
      color:#333;
      border-radius: 3px;
      width:30%;
    }

    input[type=text] {
    width: 80%; 
    padding: 5px;
    border: 1px solid #ccc; 
    border-radius: 4px;
    box-sizing: border-box; 
    margin-top: 6px; 
    margin-bottom: 16px; 
    resize: vertical 
   }
   
   input[type=submit] {
    background-color: #4CAF50;
    color: white;
    padding: 12px 20px;
    border: none;
    border-radius: 4px;
    cursor: pointer;
    }
   input[type=submit]:hover {
    background-color: #45a049;
    }

    #popup-cerrar {
      position: absolute;
      top:3px;
      right:3px;

      padding:7px 10px;
      font-size: 20px;
      text-decoration: none;
      line-height: 1;
      color:#fff;
    }

  </style>
</head>
<body>

  <div class="modal-wrapper" id="popup">
    <div class="popup-contenedor text-center">
        <h4> Agregar Movilizador</h4>
        <h4><span class="badge badge-dark">Seccion: <?php echo $seccion; ?></span></h4>
        
      <div id="cuerpo">
           <form action="../controller/servidorAddMov.php" method="POST">
           <center><div class="form-group">
            <label>Nombre(s):</label>
            <input type="text" class="form-control" name="nombre" required="">
          </div>
          <center><div class="form-group">
            <label>Apellido Paterno:</label>
            <input type="text" class="form-control" name="paterno" required="">
          </div></center>
          <center><div class="form-group">
            <label>Apellido Materno:</label>
            <input type="text" class="form-control" name="materno" required="">
          </div></center>
          <center><div class="form-group form-check">
            <label>Teléfono:</label>
            <input type="text" class="form-control" name="telefono" maxlength="10" required="">
          </div></center>
             <center><input  type="submit" value="Guardar"></center>
           </form>
      </div>
        <button type="button" id="popup-cerrar" class="btn btn-danger">X</button>
    </div>
  </div>
  <script type="text/javascript">
            document.getElementById("popup-cerrar").onclick = function () {
            location.href ="index.php";
            };
         </script>
</body>
</html>