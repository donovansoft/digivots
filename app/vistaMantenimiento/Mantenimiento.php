<?php 
  include ("../models/conexion.php");
  include '../controller/security.php'; 
  if(!empty($_POST['clv']) && !empty($_POST['nameMov']))
  {
    $clave_mov = $_POST['clv'];
    $nameMov = $_POST['nameMov'];
  }else
  {
    header('location:../vista/index.php');
  }
?>
<?php include '../controller/ctrl.sesion.seccion.php'; ?>
<!DOCTYPE html>
<html>
<head>
	<title>Mantenimiento</title>
	<?php include 'head.php'; ?>
<script>
  function getval(valor)
{
  var opcion = confirm("¿DESEAS EDITAR ESTE REGISTRO?");
  if (opcion == false) {
    return;
  }
  var id = valor.value;
  var elemento1 = ".tel"+id;
  var elemento2 = ".calle"+id;
  var elemento3 = ".noext"+id;
  var elemento4 = ".noint"+id;
  var elemento5 = ".colonia"+id;
  var elemento6 = ".municipio"+id;
  var elemento8 = ".manzana"+id;
  var elemento10 = ".cruz1"+id;
  var elemento11 = ".cruz2"+id;
  $(elemento1).removeAttr('disabled');
  $(elemento2).removeAttr('disabled');
  $(elemento3).removeAttr('disabled');
  $(elemento4).removeAttr('disabled');
  $(elemento5).removeAttr('disabled');
  $(elemento6).removeAttr('disabled');
  $(elemento8).removeAttr('disabled');
  $(elemento10).removeAttr('disabled');
  $(elemento11).removeAttr('disabled');
  var btn1 = "#btn1" + id;
  var btn2 = "#btn2" + id;
  $(btn1).addClass('d-none');
  $(btn1).siblings(btn2).removeClass('d-none');
  $(btn1).siblings('.btn-eliminar').addClass('d-none');
  $(btn1).siblings('.btn-agregar').addClass('d-none');
}

function enviar(valor)
{
  var tel = "";
  var manzana = "";
  var id = valor.value;
  var elemento1 = ".tel"+id;
  var elemento2 = ".calle"+id;
  var elemento3 = ".noext"+id;
  var elemento4 = ".noint"+id;
  var elemento5 = ".colonia"+id;
  var elemento6 = ".municipio"+id;
  var elemento8 = ".manzana"+id;
  var elemento10 = ".cruz1"+id;
  var elemento11 = ".cruz2"+id;
  var btn1 = "#btn1" + id;
  var btn2 = "#btn2" + id;
  function validarTel(tele)
  {
    if((/^([0-9])*$/.test(tele)))
    {
      tel = tele;
      return true;
    }else
    {
      return false;
    }
  }
  function validarDigito(nume)
  {
    if((/^([0-9])*$/.test(nume)))
    {
      manzana = nume;
      return true;
    }else
    {
      return false;
    }
  }
  var telflag = validarTel($(elemento1).val());
  var calle = $(elemento2).val();
  var noext = $(elemento3).val();
  var noint = $(elemento4).val();
  var colonia = $(elemento5).val();
  var municipio = $(elemento6).val()
  var manzanaflag = validarDigito($(elemento8).val());
  var cruz1 = $(elemento10).val();
  var cruz2 = $(elemento11).val();

  if(telflag != false && manzanaflag != false)
  {
    $(elemento1).attr('disabled','disabled');
    $(elemento2).attr('disabled','disabled');
    $(elemento3).attr('disabled','disabled');
    $(elemento4).attr('disabled','disabled');
    $(elemento5).attr('disabled','disabled');
    $(elemento6).attr('disabled','disabled');
    $(elemento8).attr('disabled','disabled');
    $(elemento10).attr('disabled','disabled');
    $(elemento11).attr('disabled','disabled');
    $(btn1).removeClass('d-none');
    $(btn1).siblings(btn2).addClass('d-none');
    $(btn1).siblings('.btn-eliminar').removeClass('d-none');
    $(btn1).siblings('.btn-agregar').addClass('d-none');
    var opcion = confirm("ESTÁS SEGURO DE ACTUALIZAR ESTOS DATOS? \n \n Teléfono: " +
      tel + ", Calle: " + calle + ", X " + cruz1 + ", Y " + cruz2 +
      ", No Ext: " + noext + ", No Int: " + noint + ", Colonia: " + colonia +
      ", Municipio: " + municipio + ", Manzana: " + manzana);
    if (opcion == false) {
      return;
    } else {
      //AQUI ENVIAMOS LOS DATOS PARA ACTUALIZAR//
      $.ajax({
        url: '../controller/ctrl.update.movilizador.php',
        type: 'POST',
        dataType: 'JSON',
        data: { id: id,
                tel: tel,
                calle: calle,
                cruz1: cruz1,
                cruz2: cruz2,
                noext: noext,
                noint: noint,
                colonia: colonia,
                municipio: municipio,
                manzana: manzana},
        success: function(data)
        {
          alert("DATOS GUARDADOS CORRECTAMENTE");
          idbase64  = data.IdCaptura;
          telbase64 = data.Telefono;
          calbase64 = data.Calle;
          cr1base64 = data.Cruzamiento1;
          cr2base64 = data.Cruzamiento2;
          extbase64 = data.Noext;
          intbase64 = data.Noint;
          colbase64 = data.Colonia;
          munbase64 = data.Municipio;
          manbase64 = data.Manzana;
          $.ajax({
            url: "http://sysreg.tech/bridge/funciones/update_vots.php",
            type: "POST",
            dataType: "JSON",
            data: {
                    u : 'syscam.consulta',
                    k : 'cnslt140u4sscm',
                    IdCaptura : idbase64 ,
                    Telefono : telbase64,
                    Calle : calbase64,
                    Cruzamiento1 : cr1base64,
                    Cruzamiento2 : cr2base64,
                    Noext : extbase64,
                    Noint : intbase64,
                    Colonia : colbase64,
                    Municipio : munbase64,
                    Manzana : manbase64
                  },
            success: function(data)
            {
              if(data.estatus == "INSERTADO" || data.estatus == "ACTUALIZADO")
              {
                $.ajax({
                  url: "../controller/ctrl.update.bridge.movilizador2.php",
                  type: "POST",
                  dataType: "JSON",
                  data: {
                          IdCaptura : id},
                  success: function(data)
                  {
                  },
                  error: function(errno){
                  }
                })
                .done(function() {
                  console.log("success");
                })
                .fail(function() {
                  console.log("error");
                })
                .always(function() {
                  console.log("complete");
                });

                //alert("SINCRONIZADO");
              }
            },
            error: function(errno){
            }
          })
          .done(function() {
            console.log("success");
          })
          .fail(function() {
            console.log("error");
          })
          .always(function() {
            console.log("complete");
          });

        }
      })
      .done(function() {
        console.log("success");
      })
      .fail(function() {
        console.log("error");
      })
      .always(function() {
        console.log("complete");
      });
    }
  }else if(telflag == false){
    alert("Coloca un formato adecuado para el teléfono, gracias");
    $(elemento1).val('');
    $(elemento1).focus();
  }else if(manzanaflag == false){
    /*$(elemento8).focus();*/
  }
}

function getval2()
{
  $("#table-search").html('');
  var nombre = $("#nombre").val();
  var apellidoP = $("#apellidoP").val();
  var apellidoM = $("#apellidoM").val();
  var idpersona = $("#idpersona").val();
  var mov = "<?php echo $_POST['clv']; ?>";
  $("#table-search").load("../controller/despliega.busqueda.php?nombre="+nombre+"&paterno="+apellidoP+"&materno="+apellidoM+"&mov="+mov+"&seccion=<?php echo $seccion; ?>&id="+idpersona);
}
</script>
</head>
<body onload="cargaFunction()">
  <div id="loader"></div>
	<main id="body-content" class="animate-bottom">
    <!-- Inicio Navbar -->
    <div>
      <?php include 'barranav.php'; ?>
    </div>
    <br>
    <!-- Fin Navbar -->
    <!-- Inicio Contenedor -->
    <div class="cover-container px-2">
      <?php
        $clave_mov = $_POST['clv'];
				$nameMov = $_POST['nameMov'];
        $arrayids = array();
        $cont = 0;

        include '../controller/rutalinea.php';
        include '../controller/conexion.php';
        include '../controller/ctrl.sesion.seccion.php';

        $sqlpre = "SELECT AES_DECRYPT(IdCaptura, '$linea') as IdCaptura,
                  AES_DECRYPT(Movilizador, '$linea') as Movilizador
                FROM personas 
                WHERE AES_DECRYPT(Seccion, '$linea') = '$seccion'
                AND AES_DECRYPT(Movilizador, '$linea') = '$clave_mov'
                ";
        $resultadopre = mysqli_query($connect, $sqlpre);
        while($rowpre = mysqli_fetch_assoc($resultadopre)) {
          $arrayids[$cont] = $rowpre['IdCaptura'];
          //$arrayids[$cont][1] = "white";
          $cont++;
        }
      ?>
  	  <div class="container">
  			<div class="row">
          <div class=" col-3">
            <a href="index.php" class="btn btn-info">Regresar</a>
          </div>
          <div class=" col-7">
          </div>
          <div class=" col-2">
            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#myModal">Buscar Simpatizantes</button>
          </div>
  			</div>
        <div class="row d-flex justify-content-start w-100">
          <h2 class="col-12 col-md-12 col-lg-12 g-font-size-25 pl-10 pr-10 text-center">Listado de: <?php echo $nameMov." "."(".$clave_mov.")" ?></h2>
        </div>

        <div class="row mb-3">
          <div class="col-6"></div>
          <h2 class="col-6 col-md-6 col-lg-6 d-flex justify-content-end align-items-center g-font-size-19 pl-10 pr-10 mb-0">Total de simpatizantes:&nbsp;<span class="g-color-dark"><?php echo $resultadopre->num_rows; ?></span></h2>
        </div>

  			<!-- inicia Modal -->
  			<div class="modal fade modal-lg" id="myModal" role="dialog">
  				<div class="modal-dialog modal-lg">
  					<div class="modal-content">
  						<div class="modal-header">
  								<h4 class="modal-title">Simpatizantes: </h4>
  									<button type="button" class="close" data-dismiss="modal">&times;</button>
  							<h4 class="modal-title"></h4>
  						</div>
              <div class="form-row" style="padding: 10px;">
                <div class="form-group col-md-2">
                  <label>ID</label>
                  <input type="text" class="form-control" id="idpersona">
                </div>
                <div class="form-group col-md-3">
                  <label>Apellido Paterno</label>
                  <input type="text" class="form-control" id="apellidoP" placeholder="Apellido Paterno">
                </div>
                <div class="form-group col-md-3">
                  <label>Apellido Materno</label>
                  <input type="text" class="form-control" id="apellidoM" placeholder="Apellido Materno">
                </div>
                <div class="form-group col-md-3">
                  <label>Nombres</label>
                  <input type="text" class="form-control" id="nombre" placeholder="Nombre(s)">
                </div>
                <div class="form-group col-md-1">
                  <a class="btn btn-primary" style="margin-top: 2rem;" onclick="getval2();">Buscar</a>
                </div>
              </div>
  						<div id="table-search"></div>
  						<div class="modal-footer">
  							<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
  						</div>
  					</div>
  				</div>
  			</div>
  		</div>

      <table id="table-mantenimiento" class="table table-sm table-hover">
        <thead>
          <tr>
            <th style="width: 4rem;"><small style="font-size: 14px">ID</small></th>
            <th style="width: 16rem;"><small style="font-size: 14px">Nombre</small></th>
            <th style="width: 6rem;"><small style="font-size: 14px">Fecha Nacimiento</small></th>
            <th style="width: 4rem;"><small style="font-size: 14px">Teléfono</small></th>
            <th style="width: 3rem;"><small style="font-size: 14px">Calle </small></th>
						<th style="width: 3rem;"><small style="font-size: 14px">Cruz. 1</small></th>
						<th style="width: 3rem;"><small style="font-size: 14px">Cruz. 2</small></th>
            <th style="width: 2.5rem;"><small style="font-size: 14px">No. Ext</small></th>
            <th style="width: 2.5rem;"><small style="font-size: 14px">No. Int</small></th>
            <th style="width: 3.5rem;"><small style="font-size: 14px">Colonia</small></th>
            <th style="width: 2rem;"><small style="font-size: 14px">Municipio</small></th>
            <th style="width: 2rem;"><small style="font-size: 14px">Manzana</small></th>
            <th style="width: 9rem;"><small style="font-size: 14px">&nbsp;</small></th>
          </tr>
        </thead>
        <tbody>
          <?php

            $arrayidshist=array();
            $cont2 = 0;
            $sql2 = "SELECT AES_DECRYPT(IdCaptura, '$linea') as IdCaptura FROM historicomov WHERE AES_DECRYPT(ClaveMov, '$linea') = '$clave_mov' AND AES_DECRYPT(ClaveMov, '$linea') != '0000' GROUP BY AES_DECRYPT(IdCaptura, '$linea')";
            $resultado2 = mysqli_query($connect, $sql2);
            while($row2 = mysqli_fetch_assoc($resultado2)) {
              $arrayidshist[$cont2] = $row2['IdCaptura'];
              $cont2++;
            }

            $array0000 = array();
            $cont3 = 0;
            $sql3 = "SELECT AES_DECRYPT(IdCaptura, '$linea') as IdCaptura FROM historicomov WHERE AES_DECRYPT(ClaveMov, '$linea') = '0000' GROUP BY AES_DECRYPT(IdCaptura, '$linea')";
            $resultado3 = mysqli_query($connect, $sql3);
            while($row3 = mysqli_fetch_assoc($resultado3)) {
              $array0000[$cont3] = $row3['IdCaptura'];
              $cont3++;
            }
            $contx = 0;
            /*foreach ($array0000 as $key => $value) {
              echo $contx."-> ".$value."<br>";
              $contx++;
            }*/
            $arraycomb = array_merge($arrayids, $arrayidshist);
            $arraycomb = array_unique($arraycomb);

            $arrayfinal = array();
            $contfinal = 0;
            foreach ($arraycomb as $key => $value) {
              //SI ES EL QUE ESTA EN LA TABLA PERSONAS
              if (in_array($value, $arrayids)) {
                if (in_array($value, $array0000)) {
                  $arrayfinal[$contfinal][0] = $value;
                  $arrayfinal[$contfinal][1] = "g-bg-add";
                } else {
                  $arrayfinal[$contfinal][0] = $value;
                  $arrayfinal[$contfinal][1] = "white";
                }
              } 
              //NO SE ENCUENTRA EN LA TABLA DE PERSONAS PERO SI EN LA DE HISTORIAL
              elseif(in_array($value, $arrayidshist)) {
                $arrayfinal[$contfinal][0] = $value;
                $arrayfinal[$contfinal][1] = "red";
              }
              $contfinal++;
            }

            for ($i = 0; $i < $contfinal; $i++) {
              $id = $arrayfinal[$i][0];
              $sql4 = "
                SELECT 
                  AES_DECRYPT(IdCaptura, '$linea') as IdCaptura
                , AES_DECRYPT(NombreCaptura, '$linea') as NombreCaptura
                , AES_DECRYPT(PaternoCaptura, '$linea') as PaternoCaptura
                , AES_DECRYPT(MaternoCaptura, '$linea') as MaternoCaptura
                , AES_DECRYPT(FechaNacimiento, '$linea') as FechaNacimiento
                , AES_DECRYPT(Telefono, '$linea') as Telefono
                , AES_DECRYPT(Calle, '$linea') as Calle
                , AES_DECRYPT(Cruzamiento1, '$linea') as Cruzamiento1
                , AES_DECRYPT(Cruzamiento2, '$linea') as Cruzamiento2
                , AES_DECRYPT(Noext, '$linea') as Noext
                , AES_DECRYPT(Noint, '$linea') as Noint
                , AES_DECRYPT(Colonia, '$linea') as Colonia
                , AES_DECRYPT(Municipio, '$linea') as Municipio
                , AES_DECRYPT(Seccion, '$linea') as Seccion
                , AES_DECRYPT(Manzana, '$linea') as Manzana
                , AES_DECRYPT(Casilla, '$linea') as Casilla
                , AES_DECRYPT(Movilizador, '$linea') as Movilizador
                , AES_DECRYPT(Folio, '$linea') as Folio
                , AES_DECRYPT(Orden, '$linea') as Orden
                , AES_DECRYPT(CheckLocal, '$linea') as CheckLocal
                , AES_DECRYPT(SincroBridge, '$linea') as SincroBridge
                , AES_DECRYPT(Estado, '$linea') as Estado
                FROM personas 
                WHERE AES_DECRYPT(IdCaptura, '$linea') = '$id' ";
              $resultado4 = mysqli_query($connect, $sql4);
              while($row4 = mysqli_fetch_assoc($resultado4)) {

                if ($arrayfinal[$i][1] != "red") {
                  $color = $arrayfinal[$i][1];
          ?>

                  <tr class="<?php echo $color; ?>">
                    <td style="width: 4rem;"><small style="font-size: 14px"><?php echo $row4['IdCaptura']; ?></small></td>
                    <td><small style="font-size: 14px; width: 16rem;"><?php echo $row4['PaternoCaptura'].' '.$row4['MaternoCaptura'].' '.$row4['NombreCaptura'];?></small></td>
                    <td><small style="font-size: 14px"><?php echo $row4['FechaNacimiento']; ?></small></td>
                    <td><small style="font-size: 14px"><input type="text" name="telefono" style="width: 5rem;" maxlength="10" value="<?php echo $row4['Telefono']; ?>"  class="tel<?php echo $row4['IdCaptura']; ?>" disabled></small></td>
                    <td><small style="font-size: 14px">
                      <input type="text" name="calle" value="<?php echo $row4['Calle']; ?>" style="width: 3rem"  class="calle<?php echo $row4['IdCaptura']; ?>" disabled>
                    </small></td>
                    <td><small style="font-size: 14px">
                      <input type="text" name="cruz1" value="<?php echo $row4['Cruzamiento1']; ?>" style="width: 3.2rem"  class="cruz1<?php echo $row4['IdCaptura']; ?>" disabled>
                    </small></td>
                    <td><small style="font-size: 14px">
                      <input type="text" name="cruz2" value="<?php echo $row4['Cruzamiento2']; ?>" style="width: 3.2rem"  class="cruz2<?php echo $row4['IdCaptura']; ?>" disabled>
                    </small></td>
                    <td><small style="font-size: 14px">
                      <input type="text" name="noext" value="<?php echo $row4['Noext']; ?>" style="width: 2.5rem"  class="noext<?php echo $row4['IdCaptura']; ?>" disabled>
                    </small></td>
                    <td><small style="font-size: 14px">
                      <input type="text" name="noint" value="<?php echo $row4['Noint']; ?>" style="width: 2.5rem"  class="noint<?php echo $row4['IdCaptura']; ?>" disabled>
                    </small></td>
                    <td><small style="font-size: 14px">
                      <input type="text" name="colonia" value="<?php echo $row4['Colonia']; ?>" style="width: 4rem"  class="colonia<?php echo $row4['IdCaptura']; ?>" disabled>
                    </small></td>
                    <td><small style="font-size: 14px">
                      <input type="text" name="municipio" value="<?php echo $row4['Municipio']; ?>" style="width: 5rem"  class="municipio<?php echo $row4['IdCaptura']; ?>" disabled>
                    </small></td>
                    <td><small style="font-size: 14px">
                      <input type="number" name="manzana" value="<?php echo $row4['Manzana']; ?>" style="width: 3rem;"  class="manzana<?php echo $row4['IdCaptura']; ?>" disabled>
                    </small></td>
                    <td style="width: 9rem;">
                      <center>
                        <?php
                          if($row4['Estado'] == 1 || $row4['Estado'] == 2)
                          {
                        ?>
                            <button class="mr-2 btn btn-outline-success px-2" title="Editar" style="padding: 0px; margin: 0px;" value="<?php echo $row4['IdCaptura']; ?>" onclick="getval(this);" id="btn1<?php echo $row4['IdCaptura']; ?>">
                              <i class="fa fa-edit"></i>
                            </button>
                            <button class="mr-2 btn btn-outline-success px-2 d-none" title="Guardar" style="padding: 0px; margin: 0px;" value="<?php echo $row4['IdCaptura']; ?>" onclick="enviar(this);" id="btn2<?php echo $row4['IdCaptura']; ?>">
                              <i class="fa fa-check-circle"></i>
                            </button>
                            <button class="btn-eliminar mr-0 btn btn-outline-danger px-2" data-movilizador="<?=$row4['Movilizador']?>" title="Eliminar" style="padding: 0px; margin: 0px;" value="<?php echo $row4['IdCaptura']; ?>">
                              <i class="fa fa-times"></i>
                            </button>
                            <button class="btn-agregar mr-2 btn btn-outline-success px-2 d-none" title="Agregar" data-mov="<?=$clave_mov?>" style="padding: 0px; margin: 0px;" value="<?php echo $row4['IdCaptura']; ?>">
                              <i class="fa fa-plus-circle"></i>
                            </button>
                        <?php
                          }else if($row4['Estado'] == 0)
                          {
                        ?>
                            <button class="mr-2 btn btn-outline-success px-2 d-none" title="Editar" style="padding: 0px; margin: 0px;" value="<?php echo $row4['IdCaptura']; ?>" onclick="getval(this);" id="btn1<?php echo $row4['IdCaptura']; ?>">
                              <i class="fa fa-edit"></i>
                            </button>
                            <button hidden class="mr-2 btn btn-outline-success px-2 d-none" title="Guardar" style="padding: 0px; margin: 0px;" value="<?php echo $row4['IdCaptura']; ?>" onclick="enviar(this);" id="btn2<?php echo $row4['IdCaptura']; ?>">
                              <i class="fa fa-check-circle"></i>
                            </button>
                            <button class="btn-eliminar mr-2 btn btn-outline-danger px-2 d-none" data-movilizador="<?=$row4['Movilizador']?>" title="Eliminar" style="padding: 0px; margin: 0px;" value="<?php echo $row4['IdCaptura']; ?>">
                              <i class="fa fa-times"></i>
                            </button>
                            <button class="btn-agregar mr-2 btn btn-outline-success px-2" title="Agregar" data-mov="<?=$clave_mov?>" style="padding: 0px; margin: 0px;" value="<?php echo $row4['IdCaptura']; ?>">
                              <i class="fa fa-plus-circle"></i>
                            </button>
                        <?php
                          }
                        ?>
                      </center>
                    </td>
                  </tr>

          <?php
                } else {
          ?>
                  <tr class="g-bg-delete">
                    <td style="width: 4rem;"><small style="font-size: 14px"><?php echo $row4['IdCaptura']; ?></small></td>
                    <td><small style="font-size: 14px; width: 16rem;"><?php echo $row4['PaternoCaptura'].' '.$row4['MaternoCaptura'].' '.$row4['NombreCaptura'];?></small></td>
                    <td><small style="font-size: 14px"><?php echo $row4['FechaNacimiento']; ?></small></td>
                    <td><small style="font-size: 14px"><input type="text" name="telefono" style="width: 5rem;" maxlength="10" value="<?php echo $row4['Telefono']; ?>"  class="tel<?php echo $row4['IdCaptura']; ?>" disabled></small></td>
                    <td><small style="font-size: 14px">
                      <input type="text" name="calle" value="<?php echo $row4['Calle']; ?>" style="width: 3rem"  class="calle<?php echo $row4['IdCaptura']; ?>" disabled>
                    </small></td>
                    <td><small style="font-size: 14px">
                      <input type="text" name="cruz1" value="<?php echo $row4['Cruzamiento1']; ?>" style="width: 3.2rem"  class="cruz1<?php echo $row4['IdCaptura']; ?>" disabled>
                    </small></td>
                    <td><small style="font-size: 14px">
                      <input type="text" name="cruz2" value="<?php echo $row4['Cruzamiento2']; ?>" style="width: 3.2rem"  class="cruz2<?php echo $row4['IdCaptura']; ?>" disabled>
                    </small></td>
                    <td><small style="font-size: 14px">
                      <input type="text" name="noext" value="<?php echo $row4['Noext']; ?>" style="width: 2.5rem"  class="noext<?php echo $row4['IdCaptura']; ?>" disabled>
                    </small></td>
                    <td><small style="font-size: 14px">
                      <input type="text" name="noint" value="<?php echo $row4['Noint']; ?>" style="width: 2.5rem"  class="noint<?php echo $row4['IdCaptura']; ?>" disabled>
                    </small></td>
                    <td><small style="font-size: 14px">
                      <input type="text" name="colonia" value="<?php echo $row4['Colonia']; ?>" style="width: 4rem"  class="colonia<?php echo $row4['IdCaptura']; ?>" disabled>
                    </small></td>
                    <td><small style="font-size: 14px">
                      <input type="text" name="municipio" value="<?php echo $row4['Municipio']; ?>" style="width: 5rem"  class="municipio<?php echo $row4['IdCaptura']; ?>" disabled>
                    </small></td>
                    <td><small style="font-size: 14px">
                      <input type="number" name="manzana" value="<?php echo $row4['Manzana']; ?>" style="width: 3rem;"  class="manzana<?php echo $row4['IdCaptura']; ?>" disabled>
                    </small></td>
                    <td style="width: 9rem;">
                      <center>
                        <button class="btn-agregar mr-2 btn btn-outline-success px-2" title="Agregar" data-mov="<?=$clave_mov?>" style="padding: 0px; margin: 0px;" value="<?php echo $row4['IdCaptura']; ?>">
                          <i class="fa fa-plus-circle"></i>
                        </button>
                      </center>
                    </td>
                  </tr>
          <?php 
                }
              }
            }
          ?>
        </tbody>
      </table>
      <br><br>
    </div>
    <!-- Fin Contenedor -->
  </main>
</body>
</html>