<?php 
include '../controller/rutalinea.php';
include '../controller/conexion.php';
?>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <a class="navbar-brand" href="../vista/index.php">Inicio</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
        <li class="navbar-item active"><a class="nav-link" href="../vistaMantenimiento/index.php">Mantenimiento<span class="sr-only">(current)</span></a></li>
        <li class="navbar-item active"><a class="nav-link" href="../vistaMantenimiento/mantenimiento_seccion.php">Asignar por busqueda<span class="sr-only">(current)</span></a></li>
        <li class="navbar-item active"><a class="nav-link" href="../vistaMantenimiento/movilizador.php">Listas<span class="sr-only">(current)</span></a></li>
        <li class="navbar-item active"><a class="nav-link" href="../vistaMantenimiento/mapas.php">Mapas<span class="sr-only">(current)</span></a></li>
        <li class="navbar-item active"><a class="nav-link" href="../vistaMantenimiento/contactos.php">Contactos<span class="sr-only">(current)</span></a></li>
    </ul>    
  </div>
  
  <div class="form-inline mt-2 mt-md-0" style="margin-right: 1rem;">
  <span style="color: white;">
    <i class=" fa fa-exclamation-circle"></i> Por Sincronizar
     <span class="badge badge-light">
        <?php
        $numpor = 0;  
        $sql22 = "SELECT COUNT(IdCaptura) as numpor FROM eliminados WHERE SincroBridge =AES_ENCRYPT('0', '$linea')";
         $resultado22 = mysqli_query($connect, $sql22);
         while($row22 = mysqli_fetch_assoc($resultado22))
         {
            $numpor = $numpor + $row22['numpor'];
         }

         $sql22 = "SELECT COUNT(IdCaptura) as numpor FROM updates WHERE SincroBridge =AES_ENCRYPT('0', '$linea')";
         $resultado22 = mysqli_query($connect, $sql22);
         while($row22 = mysqli_fetch_assoc($resultado22))
         {
            $numpor = $numpor + $row22['numpor'];
            
         }
         echo $numpor;

        ?>
     </span>
  </span>
  </div>

  <div class="form-inline mt-2 mt-md-0" style="margin-right: 1rem;">
  <a class="btn btn-primary text-white" href="../vistaMantenimiento/sincronizacion.php" id="btn-checa-conexion" ><i class=" fa fa-cloud-upload"></i> Sincronizar
  </a>
  </div>
  <div class="dropdown">
    <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown"><span class="fa fa-cogs"></span>  Operaciones    
    </button>
    <div class="dropdown-menu">
      <!--<a class="dropdown-item" href="../vistaMantenimiento/actualizarSeccion.php"><span class="fa fa-pencil-square-o"></span> Configuración</a>-->
      <!-- <a class="dropdown-item" href="../vistaMantenimiento/generafolios.php"><span class="fa fa-pencil-square-o"></span> Generar folios</a> -->
      <div class="dropdown-divider"></div>
      <a class="dropdown-item" href="../controller/salir.php"><span class="fa fa-sign-out"></span>  Salir</a>
    </div>
  </div>
</nav>