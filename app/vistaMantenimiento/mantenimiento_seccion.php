<?php
include ("../models/conexion.php");
include '../controller/security.php'; 
include '../controller/ctrl.sesion.seccion.php';
include '../controller/conexion.php';

$sql = "SELECT 
  AES_DECRYPT(ClaveMovilizador, '$linea') as ClaveMovilizador
  , AES_DECRYPT(NombreMovilizador, '$linea') as NombreMovilizador
  , AES_DECRYPT(PaternoMovilizador, '$linea') as PaternoMovilizador
  , AES_DECRYPT(MaternoMovilizador, '$linea') as MaternoMovilizador
FROM movilizadores
WHERE AES_DECRYPT(SeccionMovilizador, '$linea') = '$seccion'
";
$resultado = mysqli_query($connect, $sql);
$arraymov = array();
$contmovs = 0;
while($row = mysqli_fetch_assoc($resultado))
{
  $arraymov[$contmovs][0] = $row['ClaveMovilizador'];
  $arraymov[$contmovs][1] = $row['NombreMovilizador']." ".$row['PaternoMovilizador']." ".$row['MaternoMovilizador'];
  $contmovs++;  
}

?>
<!DOCTYPE html>
<html>
<head>
  <title>Busqueda Avanzada</title>
  <?php include 'head.php'; ?>
</head>
<body>
<?php include 'barranav.php'; ?>
<br>
<center><h2>Busqueda avanzada</h2></center><br>
<div id="divFiltros" class="container">
  <form class="row d-flex flex-row justify-content-center aling-items-center mt-2" id="myForm">
    <select class="select-filtro form-control col-4">
      <option selected disabled>SELECCIONAR FILTRO</option>
      <option value="1">ID</option>
      <option value="2">DIRECCIÓN</option>
      <option value="3">MOVILIZADOR</option>
      <option value="4">FOLIO</option>
    </select>
    <div class="d-none container-id col-4">
      <input type="text" class="form-control" name="IdCaptura" id="IdCaptura" placeholder="Ingrese un Id">
    </div>
    <div class="d-none container-direccion col-4 flex-row flex-nowrap justify-content-around aling-items-center">
      <input type="text" class="form-control col-3" name="Calle" id="Calle" placeholder="Calle">
      <input type="text" class="form-control col-3" name="Colonia" id="Colonia" placeholder="Colonia">
      <input type="text" class="form-control col-3" name="Manzana" id="Manzana" placeholder="Manzana">
    </div>
    <div class="d-none container-movilizador col-4">
      <!-- <input type="text" class="form-control" name="Movilizador" id="Movilizador" placeholder="Movilizador"> -->
      <select class="form-control" name="Movilizador" id="Movilizador">
        <option disabled="" selected="">Escoge el movilizadror</option>
        <?php 
          for ($i = 0; $i < $contmovs; $i++) {
        ?>
        <option value="<?php echo $arraymov[$i][0]; ?>"><?php echo $arraymov[$i][1]; ?></option>
        <?php
          }
        ?>
      </select>
    </div>
    <div class="d-none container-folio col-4" >
      <input type="text" class="form-control" name="Folio" id="Folio" placeholder="Folio">
    </div>
    <div class="d-none container-btnFiltro">
      <button class="btn btn-success btn-filtrar" role="button" title="Filtrar">Buscar</button>
    </div>
  </form>
</div>
  <div id="divTabla" class="mt-5" style="margin: 2rem;">
     <table id="table-filtro" class="table table-striped table-bordered d-none" style="width:100%">
      <thead>
        <tr>
          <th style="width: 2rem;">Check</th>
          <th>ID</th>
          <th>Nombre</th>
          <th>Dirección</th>
          <th>Colonia</th>
<!--           <th>Movilizador</th>
          <th>Opciones</th> -->
        </tr>
      </thead>
    </table> 
    <br><br>
    <div class="selecionamov d-none">
      <div class="row">
        <div class="col-6 text-right">
          <h5>ESCOGE EL MOVILIZADOR</h5>
        </div>
        <div class="col-3 text-left">
          <select class="form-control" name="NuevoMovilizador" id="NuevoMovilizador">
            <option disabled="" selected="">Escoge el movilizadror</option>
            <?php 
              for ($i = 0; $i < $contmovs; $i++) {
            ?>
            <option value="<?php echo $arraymov[$i][0]; ?>"><?php echo $arraymov[$i][1]; ?></option>
            <?php
              }
            ?>
          </select>
        </div>
      </div>
      <br>
      <div class="col-12">
        <center><button class="btn btn-primary" id="Ids-Movilizador">Mover a Movilizador</button></center>
      </div>
    </div>
    <br><br>
  </div>
</body>
</html>