<?php
include '../controller/rutalinea.php';
//include '../controller/conexion.php';
include ("../models/conexion.php");
include '../controller/security.php';
include '../controller/ctrl.sesion.seccion.php';
?>
<!DOCTYPE html>
<html>
<head>
	<title>Mantenimiento</title>
	<?php include 'head.php'; ?>
</head>
<body>
<?php include 'barranav.php'; ?>
	<div style="padding: 30px;" id="principal">
		<?php
		//PARA OBTENER EL NUMERO DE Movilizadores EN LA SECCION RESIVIDA
		$sql = "SELECT COUNT(ClaveMovilizador) AS num_movilizadores FROM movilizadores WHERE SeccionMovilizador = AES_ENCRYPT('$seccion', '$linea')";
		$resultado = mysqli_query($connect, $sql);
		while($row = mysqli_fetch_assoc($resultado))
		{
			$num_movilizadores = $row['num_movilizadores'];
		}
		//PARA OBTENER EL NUMERO DE PROMOVIDOS EN LA SECCION
		$sql2 = "SELECT COUNT(IdCaptura) AS num_promovidos FROM personas WHERE Seccion= AES_ENCRYPT('$seccion', '$linea') AND Movilizador <> AES_ENCRYPT('0000', '$linea')";
		     $resultado2 = mysqli_query($connect, $sql2);
		while($row2 = mysqli_fetch_assoc($resultado2))
		{
			$num_promovidos = $row2['num_promovidos'];
		}
		//PARA OBTENER EL NUMERO DE VOTANTES DISPONIBLES
		$sql2 = "SELECT COUNT(IdCaptura) AS disponibles FROM personas WHERE Seccion= AES_ENCRYPT('$seccion', '$linea') AND Movilizador = AES_ENCRYPT('0000', '$linea')";
		     $resultado2 = mysqli_query($connect, $sql2);
		while($row2 = mysqli_fetch_assoc($resultado2))
		{
			$disponibles = $row2['disponibles'];
		}
		//PARA OBTENER EL NUMERO DE VOTANTES EN LA SECCION
		$sql2 = "SELECT COUNT(IdCaptura) AS tot_vot FROM personas WHERE Seccion= AES_ENCRYPT('$seccion', '$linea')";
		$resultado2 = mysqli_query($connect, $sql2);
		while($row2 = mysqli_fetch_assoc($resultado2))
		{
			$tot_vot = $row2['tot_vot'];
		}
		//PARA OBTENER EL NOMBRE DEL COORDINADOR DE LA SECCION
		$sql2 = "SELECT AES_DECRYPT(nombreCoor, '$linea') as nombreCoor, AES_DECRYPT(paternoCoor, '$linea') as paternoCoor, AES_DECRYPT(maternoCoor, '$linea') as maternoCoor FROM coordinadores WHERE seccionCoor= AES_ENCRYPT('$seccion', '$linea')";
		$resultado2 = mysqli_query($connect, $sql2);
		while($row2 = mysqli_fetch_assoc($resultado2))
		{
			$nombreCoor = $row2['nombreCoor'];
			$paternoCoor = $row2['paternoCoor'];
			$maternoCoor = $row2['maternoCoor'];
			$nombre_coordinador = $nombreCoor." ".$paternoCoor." ".$maternoCoor;
			//echo $nombre_coordinador;
		}
		?>
		<div id="etiquetas">
			<div>
				<h5><b>Sección:</b> <?php echo $seccion; ?></h5>
				<h5><b>Coordinador:</b> <?php echo $nombre_coordinador; ?> </h5>
				<h5><b>Total Movilizadores:</b> <?php echo $num_movilizadores; ?></h5>
				<h5><b>Total de personas:</b> <?php echo $tot_vot; ?></h5>
				<h5><b> Total Promovidos:</b> <?php echo $num_promovidos; ?> </h5>
				<h5><b> Disponibles:</b> <?php echo $disponibles; ?></h5>
			</div>
		</div>
		<div style="text-align: right; padding-right: 9rem;">
			<a href="addMov.php" class="btn btn-danger">Agregar Movilizador</a> 
			<a href="mantenimiento_seccion.php" class="btn btn-danger">Busqueda Avanzada</a>
		</div>
		<br>
		<center>
		<h3>Movilizadores de la sección: <?php echo $seccion; ?></h3>
		<br>
		<div id="contieneTabla" style="width: 75rem; border: 2px black solid; border-radius: 5px; height: 23rem; overflow: scroll;">


				<table class="table table-hover">
				    <thead>
				      <tr>
				        <th>Clave</th>
				        <th>Nombre</th>
				        <th>Promovidos</th>
				        <th>Opción</th>
				      </tr>
				    </thead>
				    <tbody>

				      <?php
						$sql3 = "SELECT AES_DECRYPT(ClaveMovilizador, '$linea') as ClaveMovilizador, AES_DECRYPT(NombreMovilizador, '$linea') as NombreMovilizador, AES_DECRYPT(PaternoMovilizador, '$linea') as PaternoMovilizador, AES_DECRYPT(MaternoMovilizador, '$linea') as MaternoMovilizador FROM movilizadores WHERE SeccionMovilizador= AES_ENCRYPT('$seccion', '$linea') OR SeccionMovilizador= AES_ENCRYPT('0', '$linea') ORDER BY ClaveMovilizador DESC";
						$resultado3 = mysqli_query($connect, $sql3);
						while($row3 = mysqli_fetch_assoc($resultado3))
						{
							$ClaveMovilizador = $row3['ClaveMovilizador'];

							$nombre_operador_t = utf8_decode($row3['NombreMovilizador']).' '.utf8_decode($row3['PaternoMovilizador']).' '.$row3['MaternoMovilizador'];
							//PROMOVIDOS POR CADA UNO
							$sql4 = "SELECT COUNT(IdCaptura) as promovidosx FROM personas WHERE Movilizador=AES_ENCRYPT('$ClaveMovilizador', '$linea')";
							$resultado4 = mysqli_query($connect, $sql4);
							while($row4 = mysqli_fetch_assoc($resultado4))
							{
								$promovidosx = $row4['promovidosx'];
				      ?>
				      <tr>
					  <form action="Mantenimiento.php" method="POST">
				        <td><?php echo $ClaveMovilizador; ?></td>
				        <td><?php echo $nombre_operador_t; ?></td>
				        <td><?php echo $promovidosx; ?></td>
				        <td>
				        	<input type="hidden" name="clv"  value="<?php echo $ClaveMovilizador; ?>">
									<input type="hidden" name="nameMov" value="<?php echo $nombre_operador_t;?>">

				        	<input type="submit"  class="btn btn-success" value="Ver Simpatizantes">
				     	</td>
					  </form>
				      </tr>
					<?php
							}
						}
					?>
				    </tbody>
				  </table>
		</div>
		</center>
	</div>
</body>
</html>
