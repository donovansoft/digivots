<?php 
	include ("../models/conexion.php");
	include '../controller/security.php';
?>
<?php include '../controller/ctrl.sesion.seccion.php'; ?>

<!DOCTYPE html>
<html>
<head>
	<title>Generar Folio</title>
	<?php include 'head.php'; ?>

</head>
<body onload="cargaFunction()">
	<div id="loader"></div>
	<main id="body-content" class="animate-bottom">
		<!-- Inicio Navbar -->
		<div>
			<?php include 'barranav.php'; ?>
		</div>
		<br>
		<!-- Fin Navbar -->
		<!-- Inicio Contenedor -->
		<div class="container">
			<br><br>
			<?php include '../controller/ctrl.folios.php';?>
			<h1>FOLIOS GENERADOS!!!</h1>
			<?php
				echo "<meta HTTP-EQUIV=Refresh content='5; URL=index.php'>";
			?>
			<br><br>
		</div>
		<!-- Fin Contenedor -->
	</main>
</body>
</html>