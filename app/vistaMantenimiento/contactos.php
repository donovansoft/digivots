<?php
include '../controller/rutalinea.php';
//include '../controller/conexion.php';
include ("../models/conexion.php");
include '../controller/security.php';
include '../controller/ctrl.sesion.seccion.php';
?>
<!DOCTYPE html>
<html>
<head>
	<title>Mantenimiento</title>
	<?php include 'head.php'; ?>
</head>
<body>
<?php include 'barranav.php'; ?>
	
	<div class="container"><br>
		<center><h1 class="m-0">Contactos</h1></center>
		<br>
		
		<div class="row">
			<div class="col-12 text-right">
				<button class="btn btn-success" data-toggle="modal" data-target="#personasModal"><i class="fa fa-plus-circle"></i> Agregar contacto</button>
			</div>
		</div>
	</div>
	<br>
	<!-- DIV DE PERFILES Y ORDEN -->
	<div id="divElementos" class="form-inline d-flex justify-content-center mt-4">
		<select class="form-control mr-2 select-perfil-contacto" id="select-perfil">
			<option disabled="" selected="">Escoge el perfil</option>
			<option value="1">Liderazgo</option>
			<option value="2">Operador</option>
			<option value="3">Movilizador</option>
			<option value="4">RC</option>
			<option value="5">Visitas</option>
			<option value="6">Movilidad</option>
			<option value="7">Completo</option>
		</select>

		<input type="text" name="" placeholder="Clave Movilizador .. " class="form-control mr-2 d-none" id="txt_clv_mov">
		<input type="text" name="" placeholder="Clave Operador .. " class="form-control d-none" id="txt_clv_op">

		<select class="form-control mr-2 select-orden-lid-op-mov d-none" id="select-orden-lid-op-mov">
			<option disabled="" selected="">Seleccionar orden</option>
			<option value="1" title="clave, calle, numero, cruzamiento, nombre">Ordenamiento 1</option>
			<option value="2" title="clave, nombre, GENERAL">Ordenamiento 2</option>
		</select>

		<select class="form-control mr-2 select-orden-rc d-none" id="select-orden-rc">
			<option disabled="" selected="">Seleccionar orden</option>
			<option value="1" title="casilla, nombre">Ordenamiento 1</option>
			<option value="2" title="casilla, calle, numero, cruzamiento, nombre, RC">Ordenamiento 2</option>
		</select>

		<select class="form-control mr-2 select-orden-visitas d-none" id="select-orden-visitas">
			<option disabled="" selected="">Seleccionar orden</option>
			<option value="1" title="manzana, calle, numero, cruzamiento, nombre">Ordenamiento 1</option>
			<option value="2" title="colonia, calle, numero, cruzamiento, nombre, VISITAS">Ordenamiento 2</option>
		</select>

		<select class="form-control mr-2 select-orden-movilidad d-none" id="select-orden-movilidad">
			<option disabled="" selected="">Seleccionar orden</option>
			<option value="1" title="municipio, colonia, calle, numero, cruzamiento, nombre">Ordenamiento 1</option>
			<option value="2" title="municipio, seccion, calle, numero, cruzamiento, nombre, MOVILIDAD">Ordenamiento 2</option>
		</select>

		<select class="form-control mr-2 select-orden-completo d-none" id="select-orden-completo">
			<option disabled="" selected="">Seleccionar orden</option>
			<option value="1" title="nombre">Ordenamiento 1</option>
			<option value="2" title="seccion, casilla, nombre, COMPLETO">Ordenamiento 2</option>
		</select>
		<button class="btn btn-danger" id="x">Verificar</button>
	</div>
	
	<div class="imprimir-btn-contactos text-center mt-4"></div>
	<!-- TABLA DE DESPLIEGUE DE DATOS -->
	<div id="divTabla" class="mt-5" style="margin: 2rem;">
    <table id="table-listado" class="table table-striped table-bordered d-none" style="width:100%">
      <thead>
        <tr>
          <th>ID</th>
          <th>Nombre</th>
          <th>Dirección</th>
          <th>Colonia</th>
        </tr>
      </thead>
    </table> 
  </div>


	<!-- Modal AGREGAR PERSONAS-->
	<div class="modal fade" id="personasModal" tabindex="-1" role="dialog" aria-labelledby="personasModalLabel" aria-hidden="true">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="personasModalLabel">Agregar contacto</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
	      	<form method="POST" action="../controller/ctrl.Add.contactos.php">
					  <div class="form-group">
					    <label>Nombre(s)</label>
					    <input type="text" class="form-control" name="nombreContacto" placeholder="Nombre(s)" required="">
					  </div>
					  <div class="form-group">
					    <label>Apellido Paterno</label>
					    <input type="text" class="form-control" name="paternoContacto" placeholder="Apellido Paterno" required="">
					  </div>
					  <div class="form-group">
					    <label>Apellido Materno</label>
					    <input type="text" class="form-control" name="maternoContacto" placeholder="Apellido Materno" required="">
					  </div>
					  <div class="form-group">
					    <label>Posición</label>
					    <select class="form-control" name="posicion" required="">
					    	<option disabled="" selected="">Selecciona la posición</option>
					    	<option>Liderazgo</option>
					    	<option>Operador</option>
					    	<option>Movilizador</option>
					    	<option>RC</option>
					    	<option>Visitas</option>
					    	<option>Movilidad</option>
					    	<option>Completo</option>
					    </select>
					  </div>
					  <div class="form-group">
					    <label>Sección</label>
					    <input type="number" min="1" class="form-control" name="value" placeholder="Sección" required="">
					  </div>
					  <div class="form-group">
					    <label>Correo</label>
					    <input type="email" class="form-control" name="correoContacto" placeholder="correo@ejemplo.com" aria-describedby="emailHelp" required="">
					  </div>
					  <div class="form-group">
					    <label>Telefono</label>
					    <input type="tel" class="form-control" name="telefonoContacto" placeholder="9999001122" required="">
					  </div>
					  <center><button type="Submit" class="btn btn-primary">Guardar Contacto</button></center>
				  </form>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
	      </div>
	    </div>
	  </div>
	</div>





	<!-- Modal 2 -->
	<div class="modal fade" id="ModalContactos" tabindex="-1" role="dialog" aria-labelledby="ModalContactos" aria-hidden="true">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="MOdalContactosLabel">Envio de listas</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
					  <div class="form-group">
					    <label>Contacto</label>
					    <select class="form-control" id="select-correo" required="">
					    	<option disabled="" selected="">Selecciona correo</option>
					    	<?php 
					    		$sql = "SELECT AES_DECRYPT(idContacto, '$linea') as idContacto, AES_DECRYPT(nombreContacto, '$linea') as nombreContacto, AES_DECRYPT(paternoContacto, '$linea') as paternoContacto, AES_DECRYPT(maternoContacto, '$linea') as maternoContacto, AES_DECRYPT(correoContacto, '$linea') as correoContacto FROM contactos WHERE value=AES_ENCRYPT('$seccion','$linea')";
									$resultado = mysqli_query($connect, $sql);
									while($row = mysqli_fetch_assoc($resultado))
									{
					    	?>
					    	<option value="<?php echo $row['correoContacto']; ?>"><?php echo $row['nombreContacto']." ".$row['paternoContacto']." ".$row['maternoContacto']." - ".$row['correoContacto']; ?></option>
					    	 <?php
									}
        					 ?> 
					    </select>
					  </div>
					  <center><button class="btn btn-primary" id="btn-enviando-correos">Mandar</button></center>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
	      </div>
	    </div>
	  </div>
	</div>






</body>
</html>
