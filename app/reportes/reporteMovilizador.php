<?php
include '../controller/rutalinea.php';
include '../controller/conexion.php';//Quitar apenas se agrege el Modelo
include ("../models/conexion.php");
include '../controller/security.php';
require '../../assets/fpdf/fpdf.php';

$clave_movilizador = $_GET['clave_mov'];

//PARA SACAR EL NOMBRE DEL MOVILIZADOR
$sql7 = "SELECT AES_DECRYPT(NombreMovilizador, '$linea') as NombreMovilizador, AES_DECRYPT(PaternoMovilizador, '$linea') as PaternoMovilizador, AES_DECRYPT(MaternoMovilizador, '$linea') as MaternoMovilizador, AES_DECRYPT(TelefonoMovilizador, '$linea') as TelefonoMovilizador, AES_DECRYPT(SeccionMovilizador, '$linea') as SeccionMovilizador  FROM movilizadores WHERE ClaveMovilizador= AES_ENCRYPT('$clave_movilizador', '$linea')";
$resultado7 = mysqli_query($connect, $sql7);
while($row7 = mysqli_fetch_assoc($resultado7))
{
  $NombreMovilizador = $row7['NombreMovilizador'];
  $PaternoMovilizador = $row7['PaternoMovilizador'];
  $MaternoMovilizador = $row7['MaternoMovilizador'];
  $TelefonoMovilizador = $row7['TelefonoMovilizador'];
  $SeccionMovilizador = $row7['SeccionMovilizador'];
}
$nobre_completo_movilizador = $NombreMovilizador.' '.$PaternoMovilizador.' '.$MaternoMovilizador;
//echo $nobre_completo_movilizador.'<br>';
//echo $TelefonoMovilizador;

//crear constructor
//$pdf = new FPDF();
$pdf=new FPDF('L','mm','A4');

//agregar una nueva pagina
$pdf->AddPage();

//creacion de celdas
$pdf->SetFont('Arial','B',10);
$pdf->Cell(275, 8, 'CLAVE: '.$clave_movilizador.'            '.' NOMBRE: '.$nobre_completo_movilizador.'          Telefono: '.$TelefonoMovilizador.'            SECCION: '.$SeccionMovilizador, 1, 1,'C');

$pdf->SetFont('Arial','B',9);
$pdf->Cell(20, 10, 'ID', 0, 0,'C');
$pdf->Cell(105, 10, 'NOMBRE', 0, 0,'C');
$pdf->Cell(25, 10, 'NACIMIENTO', 0, 0,'C');
$pdf->Cell(115, 10, 'DIRECCION', 0, 1,'C');

$sql2 = "SELECT AES_DECRYPT(IdCaptura, '$linea') as IdCaptura, AES_DECRYPT(NombreCaptura, '$linea') as NombreCaptura, AES_DECRYPT(PaternoCaptura, '$linea') as PaternoCaptura, AES_DECRYPT(MaternoCaptura, '$linea') as MaternoCaptura, AES_DECRYPT(FechaNacimiento, '$linea') as FechaNacimiento,  AES_DECRYPT(Calle, '$linea') as Calle, AES_DECRYPT(Cruzamiento1, '$linea') as Cruzamiento1, AES_DECRYPT(Cruzamiento2, '$linea') as Cruzamiento2, AES_DECRYPT(Noext, '$linea') as Noext, AES_DECRYPT(Noint, '$linea') as Noint, AES_DECRYPT(Colonia, '$linea') as Colonia, AES_DECRYPT(Municipio, '$linea') as Municipio, AES_DECRYPT(Seccion, '$linea') as Seccion FROM personas WHERE Movilizador=AES_ENCRYPT('$clave_movilizador', '$linea') ORDER BY AES_DECRYPT(PaternoCaptura, '$linea'),AES_DECRYPT(MaternoCaptura, '$linea'),AES_DECRYPT(NombreCaptura, '$linea')";
//$sql2 = "SELECT AES_DECRYPT(IdCaptura, '$linea') as IdCaptura FROM personas WHERE Movilizador=AES_ENCRYPT('$', '$linea') Estado <> '0'";
$contador = 0;
$resultado2 = mysqli_query($connect, $sql2);
while($row2 = mysqli_fetch_assoc($resultado2))
  {
    
    if($contador==20)
    {
      $pdf->AddPage();
      //creacion de celdas
      $pdf->SetFont('Arial','B',10);
      $pdf->Cell(275, 8, 'CLAVE: '.$clave_movilizador.'            '.' NOMBRE: '.$nobre_completo_movilizador.'          Telefono: '.$TelefonoMovilizador.'            SECCION: '.$SeccionMovilizador, 1, 1,'C');

      $pdf->SetFont('Arial','B',9);
      $pdf->Cell(20, 10, 'ID', 0, 0,'C');
      $pdf->Cell(105, 10, 'NOMBRE', 0, 0,'C');
      $pdf->Cell(25, 10, 'NACIMIENTO', 0, 0,'C');
      $pdf->Cell(115, 10, 'DIRECCION', 0, 1,'C');
      $contador = 1;
    }

    //genera nombre completo
    $nombre_completo = $row2['PaternoCaptura'].' '.$row2['MaternoCaptura'].' '.$row2['NombreCaptura'];
    //genera direccion completa
    $direccion_completa = 'C.'.$row2['Calle'].' #'.$row2['Noext'].' Col.'.$row2['Colonia'];

    $pdf->SetFont('Arial','',11);
    $pdf->Cell(20, 8, $row2['IdCaptura'], 1, 0,'C');
    $pdf->Cell(105, 8, $nombre_completo, 1, 0,'C');
    $pdf->Cell(25, 8, $row2['FechaNacimiento'], 1, 0,'C');
    $pdf->Cell(115, 8, $direccion_completa, 1, 0,'C');
    $pdf->Cell(10, 8, "", 1, 1,'C');
    $contador++;
  }

//visualizar el pdf
$pdf->Output();

?>
