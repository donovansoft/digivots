<?php 
include ("../models/conexion.php");
include '../controller/security.php';
include '../controller/ctrl.sesion.seccion.php';
?>
<?php 
	include '../controller/rutalinea.php';
	include '../controller/conexion.php';
  strtoupper($sql = "SELECT 
  AES_DECRYPT(ClaveMovilizador, 'a57%gs_?+@\s') as Mov,
  AES_DECRYPT(NombreMovilizador, 'a57%gs_?+@\s') as nom,
  AES_DECRYPT(PaternoMovilizador, 'a57%gs_?+@\s') as pat,
  AES_DECRYPT(MaternoMovilizador, 'a57%gs_?+@\s') as mat
  FROM movilizadores WHERE AES_DECRYPT(SeccionMovilizador, '$linea')='$seccion'
  GROUP BY Mov ORDER BY Mov
");
    $resultado = mysqli_query($connect, $sql);
?>
<!DOCTYPE html>
<html>
<head>
	<title>Movilizador </title>
	<?php include 'head.php'; ?>
</head>
<body onload="cargaFunction()">
	<div id="loader"></div>
	<main id="body-content" class="animate-bottom">
		<!-- Inicio Navbar -->
	    <div>
			<?php include 'barranav.php'; ?>
		</div>
		<!-- Fin Navbar -->
		<!-- Inicio Contenedor -->
		<div class="container">
			<div  class="row pt-3">
				<!-- Inicio Contenedor Tabla -->
				<div class="container container-faltantes mb-2">
				</div>
				<!-- Fin Contenedor Tabla -->
				<div class="col-12 col-md-12 col-lg-12 d-flex flex-column align-items-center justify-content-center">
					<h4 class="w-100 text-center pb-2">Selecciona un movilizador</h4>
					<select class='clave-movilizador form-control col-4' style="height: 8vh;" name='menu'>
						<option selected disabled>Selecciona un movilizador:</option>
					<?php
						while($row = mysqli_fetch_assoc($resultado)) {
							//Armar Nombre Movilizador
							$NombreMovilizadorCompleto = utf8_decode($row['nom']).' '.utf8_decode($row['pat']).' '.utf8_decode($row['mat']);
					?>
						<option value="<?php echo $row['Mov'] ?>"><?php echo $row['Mov'].' - '.$NombreMovilizadorCompleto; ?></option>
						<br>
					<?php
						} 
					?>
					</select>
					
				</div>
				<br><br><br>
			</div>
			<br>

			<table id="table-faltantes" class="table table-striped table-bordered d-none" style="width:100%">
				<thead>
			        <tr>
			          <th>ID</th>
			          <th>Nombre</th>
			          <th>Fecha Nacimiento</th>
			          <th>Dirección</th>
			          
			          <!-- <th>Movilizador</th> -->
			          <!-- <th>Orden</th> -->
			        </tr>
				</thead>
			</table>
		</div>
		<!-- Fin Contenedor -->
	</main>
</body>
</html>