<?php 
include ("../models/conexion.php");
include '../controller/security.php';
?>
<?php //include '../controller/ctrl.sesion.seccion.php'; ?>
<?php include '../controller/conexion.php'; ?>
<?php include '../controller/rutalinea.php'; ?>
<!DOCTYPE html>
<html>
<head>
<title>Actualización de sección</title>
<?php include 'head.php'; ?>
	<script type="text/javascript">
		function validar(url)
		  {
		    var eliminar=confirm("¿Estás seguro que deseas actualizar tu sección?");
		    if (eliminar==true)
		    {
		      window.location=url;
		    }
		    else
		    {
		      return false;
		    }
		  }

	</script>
</head>
<body onload="cargaFunction()">
	<div id="loader"></div>
	<main id="body-content" class="animate-bottom">
		<?php include '../vistaDiaDe/barranav.php'; ?>
		<div style="margin-top: 3rem;">
			<center>
				<h2>Exportar sincronización local</h2>
				<br>
<?php 
$sql7 = "SELECT count(IdCaptura) as IdCaptura FROM personas WHERE SincroBridge = AES_ENCRYPT('0', '$linea') AND CheckLocal = AES_ENCRYPT('1', '$linea')";
$resultado7 = mysqli_query($connect, $sql7);
while($row7 = mysqli_fetch_assoc($resultado7))
{
	$ids1=$row7['IdCaptura'];
}
if($ids1>0)
{
	echo '<a href="sincroLoc.php" id="btnExp" class="btn btn-success">Exportar</a>';
}
?>

<?php 
$sql7 = "SELECT count(IdCaptura) as IdCaptura FROM personas WHERE SincroBridge = AES_ENCRYPT('2', '$linea')";
$resultado7 = mysqli_query($connect, $sql7);
while($row7 = mysqli_fetch_assoc($resultado7))
{
	$ids2=$row7['IdCaptura'];
}
if($ids2>0)
{
	echo '<a href="sincroLoc_confirmada.php" id="btnSinc" class="btn btn-success">Sincronizar</a>';
}
?>

				
				
				<hr>
				<h2>Carga de secciones</h2>
				<h4 style="color: red;">Asegúrate de tener el archivo proporcionado por soporte en el escritorio del equipo</h4>
				<!--<a href="ejecutarBat.php" class="btn btn-success">Actualizar</a>-->
				<a class="btn btn-success" Onclick="validar('ejecutarBat.php')" href="#">Actualizar</a>

			</center>
		</div>
	</main>
</body>
</html>