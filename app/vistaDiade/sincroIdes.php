<?php
include '../controller/conexion.php';
include '../controller/rutalinea.php';
include ("../models/conexion.php");
include '../controller/security.php';
?>

<!DOCTYPE html>
<html>
<head>
	<title>Sincronizacion</title>
	<?php include '../vistaMantenimiento/head.php'; ?>
<script>
$( document ).ready(function() {
	$("#sinc").on('click', function(){
		sincro();
	});
});


function sincro() {
	$.ajax({
		url : 'http://sysreg.tech/bridge/funciones/consulta_ides.php',
		type : 'POST',
		dataType : 'JSON',
		data : {
			u : 'syscam.consulta',
			k : 'cnslt140u4sscm',
			sistem: '2',
			action: '0'
		},
		success : function(response){
			console.log(response);
			$.each(response.ides, function(index, value){
				alert("ID: " + value.id);
				$.ajax({
	        url: '../controller/ctrl.update.asistencia.si.php',
	        type: 'POST',
	        dataType: 'JSON',
	        data: {id: value.id},
	        success: function(data)
	        {
	          if(data)
	          {
	            //ENVIO DE DATOS AL BRIDGE
	            var idbase64 = data.IdCaptura;
	            var chkbase64 = data.CheckLocal;
	            $.ajax({
	              url: "http://sysreg.tech/bridge/funciones/update_vots.php",
	              type: "POST",
	              dataType: "JSON",
	              data: {
	                      u : 'syscam.consulta',
	                      k : 'cnslt140u4sscm',
	                      IdCaptura : idbase64,
	                      CheckLocal : chkbase64
	                    },
	              success: function(data)
	              {
	                if(data.estatus == "INSERTADO" || data.estatus == "ACTUALIZADO")
	                {
	                  $.ajax({
	                    url: "../controller/ctrl.update.bridge.sincrobridge.php",
	                    type: "POST",
	                    dataType: "JSON",
	                    data: {
	                            IdCaptura : value.id},
	                    success: function(data)
	                    {
	                    },
	                    error: function(errno){
	                    }
	                  })
	                  .done(function() {
	                    console.log("success");
	                  })
	                  .fail(function() {
	                    console.log("error");
	                  })
	                  .always(function() {
	                    console.log("complete");
	                  });

	                  //alert("SINCRONIZADO");
	                }
	              },
	              error: function(errno){
	              }
	            })
	            .done(function() {
	              console.log("success");
	            })
	            .fail(function() {
	              console.log("error");
	            })
	            .always(function() {
	              console.log("complete");
	            });
	          }else{
	            alert("Algo sucedió en el cambio de CheckLocal");
	          }
	        }
	      })
	      .done(function() {
	        console.log("success");
	      })
	      .fail(function() {
	        console.log("error");
	      })
	      .always(function() {
	        console.log("complete");
	      });
			});
		}
	});	
}

</script>
</head>
<body onload="cargaFunction()">
	<div id="loader"></div>
	<main id="body-content" class="animate-bottom">
		<!-- Inicio Navbar -->
		<?php include '../vistaDiaDe/barranav.php'; ?>
		<!-- Fin Navbar -->
		<!-- Inicio Contenedor -->
		<div class="container">
			<br>
			<center><h1>Sincronización</h1></center>
		</div>
		<br><br>
		 <center><button class="btn btn-primary" id="sinc">SINCRONIZAR IDES</button></center>
		<!-- Fin Contenedor -->
	</main>
</body>
</html>
