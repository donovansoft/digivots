<?php
  include ("../models/conexion.php");
  include '../controller/security.php';
  include '../controller/ctrl.sesion.seccion.php';
  include '../controller/rutalinea.php';
  include '../controller/conexion.php';

  $sql = "  SELECT 
    AES_DECRYPT(personas.Movilizador, '$linea') as Mov
  , AES_DECRYPT(movilizadores.NombreMovilizador, '$linea') as nom
  , AES_DECRYPT(movilizadores.PaternoMovilizador, '$linea') as pat
  , AES_DECRYPT(movilizadores.MaternoMovilizador, '$linea') as matm
  , COUNT(movilizadores.ClaveMovilizador) as faltantes
  FROM personas INNER JOIN movilizadores ON personas.Movilizador = movilizadores.ClaveMovilizador
  WHERE AES_DECRYPT(personas.Seccion, '$linea') = '$seccion'AND AES_DECRYPT(personas.CheckLocal, '$linea') = '0' AND AES_DECRYPT(personas.Movilizador, '$linea') <> '0000'
  GROUP BY Mov, nom, pat, matm
  ORDER BY Mov
  ";
  $resultado = mysqli_query($connect, $sql);
?>
<!DOCTYPE html>
<html>
<head>
  <title>Listado</title>
  <?php include 'head.php'; ?>
</head>
<body onload="cargaFunction()">
  <div id="loader"></div>
  <main id="body-content" class="animate-bottom">
    <!-- Inicio Navbar -->
    <div>
      <?php include 'barranav.php'; ?>
    </div>
    <br>
    <!-- Fin Navbar -->
    <!-- Inicio Contenedor -->
    <div class="cover-container px-2">
      <div class="row d-flex justify-content-start w-100 mx-0 alert-danger">
        <h2 class="col-12 col-md-12 col-lg-12 g-font-size-25 pl-10 pr-10 text-center">Listado de simpatizantes</h2>
        <h2 class="col-12 g-font-size-25 pl-10 pr-10 text-left">Sección: <?=$seccion?></h2>
        <div class="col-12 container-print">
         <!-- <a href="../vista/index.php" class="btn btn-info" title="Regresar">Regresar</a> -->
        </div>
        <div class="row d-flex align-items-center justify-content-center my-2 w-100">
          <!-- <h4 class="w-100 text-center pb-2">Selecciona un movilizador</h4> -->
          <h2 class="col-12 col-md-12 col-lg-12 g-font-size-18 text-center "><label class="" for="menu"><span class="badge badge-danger">Selecciona un Movilizador</span></label></h2>
          <select class='clave-movilizador-main form-control col-3' id="menu" name='menu'>
            <option selected disabled>Selecciona un movilizador:</option>
          <?php
            while($row = mysqli_fetch_assoc($resultado)) {
              //Armar Nombre Movilizador
              $NombreMovilizadorCompleto = utf8_decode($row['nom']).' '.utf8_decode($row['pat']).' '.utf8_decode($row['mat']);
          ?>
            <option value="<?php echo $row['Mov'] ?>"><?php echo strtoupper($row['Mov'].' - '.$NombreMovilizadorCompleto.' - Faltantes: '.$row['faltantes']); ?></option>
            <br>
          <?php
            }
          ?>
          </select>
        </div>
      </div>
      <table id="table-votantes" class="table table-sm table-hover table-bordered d-none">
        <thead>
          <tr>
            <th>ID</th>
            <th>Nombre</th>
            <th>Fecha Nacimiento</th>
            <th>Casilla</th>
            <th>Asistencia</th>
          </tr>
        </thead>
      </table>
      <br><br>
    </div>
    <!-- Fin Contenedor -->
    <!-- Modal Comentario -->
    <div id="monster-modal" class="modales g-transition-4">
      <div class="container g-max-width-80vh g-bg-scare g-pa-20 g-rounded-6">
        <div class="row g-ma-0 g-px-15 d-flex justify-content-center align-items-center">
          <div class="g-color-white g-font-size-100 mt-3">
            <span><i class="fa fa-exclamation-triangle"></i></span>
          </div>
          <div class="mt-5 pt-5 px-5 d-flex justify-content-center align-items-center">
            <h1 class="g-font-size-35 g-color-white text-center">Haz excedido el número de intentos para poder asignar asistencia!!!!</h1>
          </div>
        </div>
      </div>
    </div>
    <!-- End Modal Comentario -->
  </main>
    <!-- MODAL PARA CORREO -->
    <div class="modal fade bd-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
      <style type="text/css">
        .modal-content{
          width: 40rem;
        }
      </style>
      <div class="modal-dialog modal-sm">
        <div class="modal-content">
          <form>
            <center>
            <h4>Envio de listas</h4>
            <h6>Mandar a:</h6>
            <hr>
            <select id="select-correo">
                <option disabled selected>Seleccionar contacto</option>
            <?php 
$sql = "SELECT AES_DECRYPT(claveCoor, '$linea') as claveCoor, AES_DECRYPT(nombreCoor, '$linea') as nombreCoor, AES_DECRYPT(paternoCoor, '$linea') as paternoCoor, AES_DECRYPT(maternoCoor, '$linea') as maternoCoor, AES_DECRYPT(correoCoor, '$linea') as correoCoor FROM coordinadores WHERE seccionCoor=AES_ENCRYPT('$seccion','$linea')";
$resultado = mysqli_query($connect, $sql);
while($row = mysqli_fetch_assoc($resultado))
{
            ?>
                <option value="<?php echo $row['correoCoor']; ?>"><?php echo $row['nombreCoor']." ".$row['paternoCoor']." ".$row['maternoCoor']." - ".$row['correoCoor']; ?></option> 
            <?php
}
            ?>
            </select>
            <hr>
            <a href="#" class="btn btn-info btn-correo">Enviar</a>
            <hr>
            </center>
          </form>
        </div>
      </div>
    </div>
    <!-- FIN MODAL-->
</body>
</html>
