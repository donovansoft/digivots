<?php
include '../controller/conexion.php';
include '../controller/rutalinea.php';
include ("../models/conexion.php");
include '../controller/security.php';
?>

<!DOCTYPE html>
<html>
<head>
	<title>Sincronizacion</title>
	<?php include '../vistaDiaDe/head.php'; ?>
<script>
$( document ).ready(function() {
	$("#sinc").on('click', function(){
    $.ajax({
    	url: "http://sysreg.tech/bridge/funciones/update_vots.php",
        type: "POST",
        dataType: "JSON",
        data: {
                u : 'syscam.consulta',
                k : 'cnslt140u4sscm'
              },
        success : function () {
          sincro();
          location.reload();
        }
        ,error : function () {
          alert('No puedes sincronizar, no tienes conexion al servidor');
        }
    });
	});
});
<?php
	$JSON = "";
	$sql = "SELECT 
  TO_BASE64(IdCaptura) as IdCaptura,
  TO_BASE64(CheckLocal) as CheckLocal,
  AES_DECRYPT(IdCaptura, '$linea') as id
	FROM personas 
	WHERE 
		AES_DECRYPT(CheckLocal, '$linea') = '1' AND
    AES_DECRYPT(SincroBridge, '$linea') = '0'";
	$resultado = mysqli_query($connect, $sql);
	while($row = mysqli_fetch_assoc($resultado)) {
		$JSON .= json_encode($row) . ",";
	}
	$JSON = trim($JSON, ",");
?>
var $json_data = <?php echo "[".$JSON."]" ?>;

function sincro() {
	$.each($json_data, function(index, value){
		$.ajax({
			url : 'http://sysreg.tech/bridge/funciones/update_vots.php',
			type : 'POST',
			dataType : 'JSON',
			data : {
				u : 'syscam.consulta',
				k : 'cnslt140u4sscm',
				IdCaptura : value.IdCaptura,
				SincroBridge : value.CheckLocal
			},
			success : function(response){
				console.log(response);
				if (response.estatus == "INSERTADO" || response.estatus == "ACTUALIZADO") {
					$.ajax({
						url : '../controller/ctrl.update.bridge.sincrobridge.php',
						type : 'POST',
						dataType : 'JSON',
						data : {
							u : 'syscam.consulta',
							k : 'cnslt140u4sscm',
							IdCaptura : value.id
						},
						success : function(response){
							if (response == true) {
								//alert("SINCRONIZADO");
							}
							console.log(response);
						}
					});
				}
			}
		});	
	});
	alert("SE HAN SINCRONIZADO LOS REGISTROS");
}

</script>
</head>
<body onload="cargaFunction()">
	<div id="loader"></div>
	<main id="body-content" class="animate-bottom">
		<!-- Inicio Navbar -->
		<?php include '../vistaDiaDe/barranav.php'; ?>
		<!-- Fin Navbar -->
		<!-- Inicio Contenedor -->
		<div class="container">
			<br>
			<center><h1>Sincronización</h1></center>
		</div>
		<br><br>
		 <center><button class="btn btn-primary" id="sinc">SINCRONIZAR</button></center>
		<!-- Fin Contenedor -->
	</main>
</body>
</html>
