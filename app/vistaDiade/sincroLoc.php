<?php 
include '../controller/rutalinea.php';
include '../controller/conexion.php';
require '../../assets/sincroCSV/Classes/PHPExcel.php';

//crear el objeto
$objPHPExcel = new PHPExcel();
//agregar las propiedades al documento
$objPHPExcel->getProperties()
->setCreator("Departamento Administrativo")
->setLastModifiedBy("Departamento Administrativo")
->setTitle("Excel sincro")
->setSubject("Documento de sincro")
->setDescription("Documento generado con la nomina semanal")
->setKeywords("excel sincro csv")
->setCategory("sincro");
//seleccionar hoja activa
$objPHPExcel->setActiveSheetIndex(0);
//titulo de la hoja
$objPHPExcel->getActiveSheet()->setTitle('Hoja 1');

//agregar contenido a las celdas
$contador = 1;
//$sql = "SELECT IdCaptura, NombreCaptura, PaternoCaptura, MaternoCaptura FROM personas WHERE SincroBridge= AES_ENCRYPT('0', '$linea') AND CheckLocal=AES_ENCRYPT('1', '$linea')";

$sql = "SELECT IdCaptura, NombreCaptura, PaternoCaptura, MaternoCaptura, FechaNacimiento, Telefono, Calle, Cruzamiento1, Cruzamiento2, Noext, Noint, Colonia, Municipio, Seccion, Manzana, Casilla, Movilizador, Folio, Orden, CheckLocal, SincroBridge, Estado FROM personas WHERE SincroBridge= AES_ENCRYPT('0', '$linea') AND CheckLocal= AES_ENCRYPT('1', '$linea')";
$resultado = mysqli_query($connect, $sql);
while($row = mysqli_fetch_assoc($resultado))
{
	if($row != null || $row != "")
	{
		$objPHPExcel->getActiveSheet()->setCellValue('A'.$contador, base64_encode($row['IdCaptura']));
		$objPHPExcel->getActiveSheet()->setCellValue('B'.$contador, base64_encode($row['NombreCaptura']));
		$objPHPExcel->getActiveSheet()->setCellValue('C'.$contador, base64_encode($row['PaternoCaptura']));
		$objPHPExcel->getActiveSheet()->setCellValue('D'.$contador, base64_encode($row['MaternoCaptura']));
		$objPHPExcel->getActiveSheet()->setCellValue('E'.$contador, base64_encode($row['FechaNacimiento']));
		$objPHPExcel->getActiveSheet()->setCellValue('F'.$contador, base64_encode($row['Telefono']));
		$objPHPExcel->getActiveSheet()->setCellValue('G'.$contador, base64_encode($row['Calle']));
		$objPHPExcel->getActiveSheet()->setCellValue('H'.$contador, base64_encode($row['Cruzamiento1']));
		$objPHPExcel->getActiveSheet()->setCellValue('I'.$contador, base64_encode($row['Cruzamiento2']));
		$objPHPExcel->getActiveSheet()->setCellValue('J'.$contador, base64_encode($row['Noext']));
		$objPHPExcel->getActiveSheet()->setCellValue('K'.$contador, base64_encode($row['Noint']));
		$objPHPExcel->getActiveSheet()->setCellValue('L'.$contador, base64_encode($row['Colonia']));
		$objPHPExcel->getActiveSheet()->setCellValue('M'.$contador, base64_encode($row['Municipio']));
		$objPHPExcel->getActiveSheet()->setCellValue('N'.$contador, base64_encode($row['Seccion']));
		$objPHPExcel->getActiveSheet()->setCellValue('O'.$contador, base64_encode($row['Manzana']));
		$objPHPExcel->getActiveSheet()->setCellValue('P'.$contador, base64_encode($row['Casilla']));
		$objPHPExcel->getActiveSheet()->setCellValue('Q'.$contador, base64_encode($row['Movilizador']));
		$objPHPExcel->getActiveSheet()->setCellValue('R'.$contador, base64_encode($row['Folio']));
		$objPHPExcel->getActiveSheet()->setCellValue('S'.$contador, base64_encode($row['Orden']));
		$objPHPExcel->getActiveSheet()->setCellValue('T'.$contador, base64_encode($row['CheckLocal']));
		$objPHPExcel->getActiveSheet()->setCellValue('U'.$contador, base64_encode($row['SincroBridge']));
		$objPHPExcel->getActiveSheet()->setCellValue('V'.$contador, base64_encode($row['Estado']));
		$contador = $contador + 1;
	}
}


//PONERLOS LOS EN ESTADO PENDIENTE
$sql2 = "UPDATE personas SET SincroBridge=AES_ENCRYPT('2', '$linea') WHERE SincroBridge= AES_ENCRYPT('0', '$linea') AND CheckLocal= AES_ENCRYPT('1', '$linea')";
$resultado = mysqli_query($connect, $sql2);

//HEADER PARA GUARDAR EL ARCHIVO
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="miSeccion.csv"');
header('Cache-Control: max-age=0');

//se crea otro objeto
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'csv');

//indicar que lo vamos a guardar
$objWriter->save('php://output');
//SELECT IdCaptura, NombreCaptura, PaternoCaptura, MaternoCaptura FROM personas WHERE SincroBridge= 0 AND CheckLocal=1
// a57%gs_?+@\s
//SELECT AES_DECRYPT(IdCaptura, 'a57%gs_?+@\s') as IdCaptura FROM `sincro_local`
//SELECT AES_DECRYPT(PaternoCaptura, 'a57%gs_?+@\s') as PaternoCaptura FROM `personas` WHERE IdCaptura = AES_ENCRYPT('1188795', 'a57%gs_?+@\s') 
?>

