<?php
include '../controller/rutalinea.php';
//include '../controller/conexion.php';
include ("../models/conexion.php");
include '../controller/security.php';
include '../controller/ctrl.sesion.seccion.php';
?>
<!DOCTYPE html>
<html>
<head>
	<title>Mantenimiento</title>
	<?php include 'head.php'; ?>
</head>
<body>
<?php include 'barranav.php'; ?>
	<div class="container"><br>
		<?php 
			$sql = "SELECT idvida, estado FROM vidas WHERE estado = 0 ORDER BY idvida ASC LIMIT 1";
			$resultado = mysqli_query($connect, $sql);
			while ($row = mysqli_fetch_assoc($resultado)) {
				$intento = $row['idvida'];		    
		  }
		  if (isset($intento)) {
		?>
		<center><h1>Reestablecer Datos (Intento No. <?php echo $intento; ?>)</h1></center>
		<br><br>
		<div class="row">
			<div class="col-4 text-right">
				<p>Contraseña:</p>
			</div>
			<div class="col-3 text-right">
				<input type="text" id="input-pass" class="form-control">
			</div>
			<div class="col-5">
				<button class="btn btn-primary" id="btn-reestablecer">Ingresar</button>
			</div>
		</div>
		<?php 
			} else {
		?>

		<br>
		<center><h1 style="color: red">HAS EXCEDIDO EL NUMERO DE REESTABLECIMIENTOS</h1></center>
		<?php
			}
		?>
	</div>
</body>
</html>