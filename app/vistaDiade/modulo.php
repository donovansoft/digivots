<?php 
include '../controller/rutalinea.php';
//include '../controller/conexion.php';
include ("../models/conexion.php");
include '../controller/security.php';
include '../controller/ctrl.sesion.seccion.php';
?>
<!DOCTYPE html>
<html>
<head>
<title>Modulo</title>
<?php include 'head.php'; ?>
<script type="text/javascript">
  //PARA MÓDULO CONTACTOS 
  $(document).ready(function(){
	//Evento para seleccionar perfil de modulo de contacto
	$(".select-perfil-contacto").on('change', function(event) {
	  var perfils = $(this).val();
	  switch(perfils) {
	    case "1":
	      $("#txt_clv_mov").addClass('d-none');
	      $("#txt_clv_op").addClass('d-none');
	      $("#select-orden-lid-op-mov").removeClass('d-none');
	      $("#select-orden-rc").addClass('d-none');
	      $("#select-orden-visitas").addClass('d-none');
	      $("#select-orden-movilidad").addClass('d-none');
	      $("#select-orden-completo").addClass('d-none');
	    break;
	    case "2":
	      $("#txt_clv_op").removeClass('d-none');
	      $("#txt_clv_mov").addClass('d-none');
	      $("#select-orden-lid-op-mov").removeClass('d-none');
	      $("#select-orden-rc").addClass('d-none');
	      $("#select-orden-visitas").addClass('d-none');
	      $("#select-orden-movilidad").addClass('d-none');
	      $("#select-orden-completo").addClass('d-none');
	    break;
	    case "3":
	      $("#txt_clv_mov").removeClass('d-none');
	      $("#txt_clv_op").addClass('d-none');
	      $("#select-orden-lid-op-mov").removeClass('d-none');
	      $("#select-orden-rc").addClass('d-none');
	      $("#select-orden-visitas").addClass('d-none');
	      $("#select-orden-movilidad").addClass('d-none');
	      $("#select-orden-completo").addClass('d-none');
	    break;
	    case "4":
	      $("#txt_clv_mov").addClass('d-none');
	      $("#txt_clv_op").addClass('d-none');
	      $("#select-orden-rc").removeClass('d-none');
	      $("#select-orden-lid-op-mov").addClass('d-none');
	      $("#select-orden-visitas").addClass('d-none');
	      $("#select-orden-movilidad").addClass('d-none');
	      $("#select-orden-completo").addClass('d-none');
	    break;
	    case "5":
	      $("#txt_clv_mov").addClass('d-none');
	      $("#txt_clv_op").addClass('d-none');
	      $("#select-orden-visitas").removeClass('d-none');
	      $("#select-orden-lid-op-mov").addClass('d-none');
	      $("#select-orden-rc").addClass('d-none');
	      $("#select-orden-movilidad").addClass('d-none');
	      $("#select-orden-completo").addClass('d-none');
	    break;
	    case "6":
	      $("#txt_clv_mov").addClass('d-none');
	      $("#txt_clv_op").addClass('d-none');
	      $("#select-orden-movilidad").removeClass('d-none');
	      $("#select-orden-lid-op-mov").addClass('d-none');
	      $("#select-orden-rc").addClass('d-none');
	      $("#select-orden-visitas").addClass('d-none');
	      $("#select-orden-completo").addClass('d-none');
	    break;
	    case "7":
	      $("#txt_clv_mov").addClass('d-none');
	      $("#txt_clv_op").addClass('d-none');
	      $("#select-orden-completo").removeClass('d-none');
	      $("#select-orden-lid-op-mov").addClass('d-none');
	      $("#select-orden-rc").addClass('d-none');
	      $("#select-orden-visitas").addClass('d-none');
	      $("#select-orden-movilidad").addClass('d-none');
	    break;
	  }
	});
	$("#x").on('click', function(event) {
		var data = "";
	    var perfil = $(".select-perfil-contacto").val();
	    if (perfil == "1") {
	    	var selectorden = $("#select-orden-lid-op-mov").val();
	    	data =  { 
	    				perfil : perfil,
	    				Orden : selectorden
	    			};
	    } else if (perfil == "2") {
			var selectorden = $("#select-orden-lid-op-mov").val();
			var selectorvalue = $("#txt_clv_op").val();
			data =  { 
	    				perfil : perfil,
	    				Orden : selectorden,
	    				Clave : selectorvalue
	    			};	        	
	    } else if (perfil == "3") {
	    	var selectorden = $("#select-orden-lid-op-mov").val();
	    	var selectorvalue = $("#txt_clv_mov").val();
			data =  { 
	    				perfil : perfil,
	    				Orden : selectorden,
	    				Clave : selectorvalue
	    			};
	    } else if (perfil == "4") {
	    	var selectorden = $("#select-orden-rc").val();
			data =  { 
	    				perfil : perfil,
	    				Orden : selectorden
	    			};
	    } else if (perfil == "5") {
	    	var selectorden = $("#select-orden-visitas").val();
			data =  { 
	    				perfil : perfil,
	    				Orden : selectorden
	    			};
	    } else if (perfil == "6") {
	    	var selectorden = $("#select-orden-movilidad").val();
			data =  { 
	    				perfil : perfil,
	    				Orden : selectorden
	    			};
	    } else if (perfil == "7") {
	    	var selectorden = $("#select-orden-completo").val();
	    	data =  { 
	    				perfil : perfil,
	    				Orden : selectorden
	    			};
	    }

	  $.ajax({
	    url: "../controller/ctrl.impresiones.orderby.php",
	    type: "POST",
	    dataType: "JSON",
	    data: data,
	    success: function(data)
	    {
	    	console.log(data);
	    },
	    error: function(errno){
	    }
	  })
	  .done(function() {
	    console.log("success");
	  })
	  .fail(function() {
	    console.log("error");
	  })
	  .always(function() {
	    console.log("complete");
	  });    
	});
  });
</script>
</head>
<body>
<?php include 'barranav.php'; ?>
	<div id="divGenerarl">
		<div id="divElementos" class="form-inline d-flex justify-content-center mt-4">
				
				<select class="form-control mr-2 select-perfil-contacto" id="select-perfil">
					<option disabled="" selected="">Escoge el perfil</option>
					<option value="1">Liderazgo</option>
					<option value="2">Operador</option>
					<option value="3">Movilizador</option>
					<option value="4">RC</option>
					<option value="5">Visitas</option>
					<option value="6">Movilidad</option>
					<option value="7">Completo</option>
				</select>

				<input type="text" name="" placeholder="Clave Movilizador .. " class="form-control mr-2 d-none" id="txt_clv_mov">
				<input type="text" name="" placeholder="Clave Operador .. " class="form-control d-none" id="txt_clv_op">

				<select class="form-control mr-2 select-orden-lid-op-mov d-none" id="select-orden-lid-op-mov">
					<option disabled="" selected="">Seleccionar orden</option>
					<option value="1" title="clave, calle, numero, cruzamiento, nombre">Ordenamiento 1</option>
					<option value="2" title="clave, nombre, GENERAL">Ordenamiento 2</option>
				</select>

				<select class="form-control mr-2 select-orden-rc d-none" id="select-orden-rc">
					<option disabled="" selected="">Seleccionar orden</option>
					<option value="1" title="casilla, nombre">Ordenamiento 1</option>
					<option value="2" title="casilla, calle, numero, cruzamiento, nombre, RC">Ordenamiento 2</option>
				</select>

				<select class="form-control mr-2 select-orden-visitas d-none" id="select-orden-visitas">
					<option disabled="" selected="">Seleccionar orden</option>
					<option value="1" title="manzana, calle, numero, cruzamiento, nombre">Ordenamiento 1</option>
					<option value="2" title="colonia, calle, numero, cruzamiento, nombre, VISITAS">Ordenamiento 2</option>
				</select>

				<select class="form-control mr-2 select-orden-movilidad d-none" id="select-orden-movilidad">
					<option disabled="" selected="">Seleccionar orden</option>
					<option value="1" title="municipio, colonia, calle, numero, cruzamiento, nombre">Ordenamiento 1</option>
					<option value="2" title="municipio, seccion, calle, numero, cruzamiento, nombre, MOVILIDAD">Ordenamiento 2</option>
				</select>

				<select class="form-control mr-2 select-orden-completo d-none" id="select-orden-completo">
					<option disabled="" selected="">Seleccionar orden</option>
					<option value="1" title="nombre">Ordenamiento 1</option>
					<option value="2" title="seccion, casilla, nombre, COMPLETO">Ordenamiento 2</option>
				</select>

				<button class="btn btn-danger" id="x">Verificar</button>

		</div>
	</div>
</body>
</html>