
<?php
  include ("../models/conexion.php");
  include '../controller/security.php';
  include '../controller/ctrl.sesion.seccion.php';
  include '../controller/rutalinea.php';
  include '../controller/conexion.php';

  strtoupper($sql = "SELECT
  AES_DECRYPT(Casilla, '$linea') as cas
  FROM personas
  WHERE AES_DECRYPT(Seccion, '$linea') = '$seccion'
  GROUP BY cas
  ORDER BY cas
  ");
  $resultado = mysqli_query($connect, $sql);
?>
<!DOCTYPE html>
<html>
<head>
  <title>Listado</title>
  <?php include 'head.php'; ?>
</head>
<body onload="cargaFunction()">
  <div id="loader"></div>
  <main id="body-content" class="animate-bottom">
    <!-- Inicio Navbar -->
    <div>
      <?php include 'barranav.php'; ?>
    </div>
    <br>
    <!-- Fin Navbar -->
    <!-- Inicio Contenedor -->
    <div class="cover-container px-2">
      <div class="row d-flex justify-content-start w-100 mx-0 alert-success">
        <h2 class="col-12 col-md-12 col-lg-12 g-font-size-25 pl-10 pr-10 text-center">Listado de simpatizantes </h2>
        <h2 class="col-12 g-font-size-25 pl-10 pr-10 text-left">Sección: <?=$seccion?></h2>
        <div class="col-12 container-print">
         <!-- <a href="../vista/index.php" class="btn btn-info" title="Regresar">Regresar</a> -->
        </div>
        <div class="row d-flex align-items-center justify-content-center my-2 w-100">
          <!-- <h4 class="w-100 text-center pb-2">Selecciona un movilizador</h4> -->
          <h2 class="col-12 col-md-12 col-lg-12 g-font-size-18 text-center "><label class="" for="menu"><span class="badge badge-success">Selecciona una Casilla</span></label></h2>
          <select class='clave-casilla-main form-control col-3' id="menu" name='menu'>
            <option selected disabled>Selecciona una Casilla:</option>
          <?php
            while($row = mysqli_fetch_assoc($resultado)) {
              //Armar Nombre Movilizador
             strtoupper( $Casilla = utf8_decode($row['cas']));
          ?>
            <option value="<?php echo strtoupper($row['cas']) ?>"><?php echo 'Casilla: '.strtoupper($Casilla);?></option>
            <br>
          <?php
            }
          ?>
          </select>
        </div>
      </div>
      <table id="table-casilla" class="table table-sm table-hover table-bordered d-none">
        <thead>
          <tr>
            <th>ID</th>
            <th>Apellido Paterno</th>
            <th>Apellido Materno</th>
            <th>Nombre</th>
            <!-- <th>Fecha Nacimiento</th> -->
            <!-- <th>Movilizador</th> -->
            <th>Asistencia</th>
          </tr>
        </thead>
      </table>
      <br><br>
    </div>
    <!-- Fin Contenedor -->
    <!-- Modal Comentario -->
    <div id="monster-modal" class="modales g-transition-4">
      <div class="container g-max-width-80vh g-bg-scare g-pa-20 g-rounded-6">
        <div class="row g-ma-0 g-px-15 d-flex justify-content-center align-items-center">
          <div class="g-color-white g-font-size-100 mt-3">
            <span><i class="fa fa-exclamation-triangle"></i></span>
          </div>
          <div class="mt-5 pt-5 px-5 d-flex justify-content-center align-items-center">
            <h1 class="g-font-size-35 g-color-white text-center">Haz excedido el número de intentos para poder asignar asistencia!!!!</h1>
          </div>
        </div>
      </div>
    </div>
    <!-- End Modal Comentario -->
  </main>
</body>
</html>
