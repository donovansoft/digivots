<?php
include '../controller/rutalinea.php';
//include '../controller/conexion.php';
include ("../models/conexion.php");
include '../controller/security.php';
include '../controller/ctrl.sesion.seccion.php';
?>
<!DOCTYPE html>
<html>
<head>
	<title>Dia De</title>
	<?php include 'head.php'; ?>
</head>
<body>
<?php include 'barranav.php'; ?>
<?php
	$sql = "SELECT COUNT(IdCaptura) AS conteoVots 
					FROM personas 
					WHERE Seccion = AES_ENCRYPT('$seccion', '$linea')
					/*AND Movilizador != AES_ENCRYPT('0000', '$linea')*/
	";
	$resultado = mysqli_query($connect, $sql);
	while($row = mysqli_fetch_assoc($resultado)) {
		$num_simpat = $row['conteoVots'];
	} 
	/*$sql = "SELECT COUNT(ClaveMovilizador) AS num_movilizadores 
					FROM movilizadores 
					WHERE SeccionMovilizador = AES_ENCRYPT('$seccion', '$linea')
	";
	$resultado = mysqli_query($connect, $sql);
	while($row = mysqli_fetch_assoc($resultado)) {
		$num_mov = $row['num_movilizadores'];
	}*/
	//PARA OBTENER EL NOMBRE DEL COORDINADOR DE LA SECCION
	/*$sql2 = "SELECT AES_DECRYPT(nombreCoor, '$linea') as nombreCoor, AES_DECRYPT(paternoCoor, '$linea') as paternoCoor, AES_DECRYPT(maternoCoor, '$linea') as maternoCoor FROM coordinadores WHERE seccionCoor= AES_ENCRYPT('$seccion', '$linea')";
	$resultado2 = mysqli_query($connect, $sql2);
	while($row2 = mysqli_fetch_assoc($resultado2))
	{
		$nombreCoor = $row2['nombreCoor'];
		$paternoCoor = $row2['paternoCoor'];
		$maternoCoor = $row2['maternoCoor'];
		$nombre_coordinador = $nombreCoor." ".$paternoCoor." ".$maternoCoor;
		//echo $nombre_coordinador;
	}*/
?>
	<div style="padding: 30px;" id="principal">
		<center>
		<h3>Vista de Detalles de Etapa 2</h3>
		</center>
		<br>
		<h5><b>Sección: </b><?php echo $seccion ?></h5>
		<!-- <h5><b>Coordinador: </b> <?php echo $nombre_coordinador; ?> </h5> -->
		<h5><b>Total de Simpatizantes: </b><?php echo $num_simpat ?></h5>
		<!-- <h5><b>Total de Movilizador: </b><?php echo $num_mov ?></h5> -->
		<br>
			<!-- <div style="text-align: right;">
				<a href="imprimirListas.php" target="-" class="btn btn-warning" style="color: white; margin-right: 6rem;">Imprimir listas</a>
			</div> -->
		<br>
		<div class="container">
			<table class="table table-hover">
				<thead>
					<tr>
						<!-- <th>Clave</th> -->
						<th>Casilla</th>
						<th>Total</th>
						<th>Checkeados</th>
						<th>No checkeados</th>
						<th>Porcentaje de checkeo</th>
					</tr>
				</thead>
				<tbody>
					<?php 
						$sql = "SELECT
									AES_DECRYPT(Casilla, '$linea') as Casilla
								FROM personas WHERE Seccion = AES_ENCRYPT('$seccion', '$linea')
								GROUP BY Casilla ORDER BY Casilla DESC
						";
						$resultado = mysqli_query($connect, $sql);
						/*if($resultado)
						{
							echo "Yei";
						}else
						{
							echo mysqli_error($connect);
						}*/
						$totalfin = 0;
						$totalsifin = 0;
						$totalnofin = 0;

						while($row = mysqli_fetch_assoc($resultado)) {
							/*$mov = $row['MpmbreMovi']." ".$row['PMovi']." ".$row['MMovi'];*/
							$casilla = $row['Casilla'];
							/*$clvmov = $row['ClaveMovi'];*/
							$sql2 = "SELECT COUNT(IdCaptura) AS Votsi,
												SUM(AES_DECRYPT(CheckLocal, '$linea')) as totsi
								FROM personas 
								WHERE Seccion = AES_ENCRYPT('$seccion', '$linea')
									AND Casilla = AES_ENCRYPT('$casilla', '$linea')
							";
							$resultado2 = mysqli_query($connect, $sql2);
							while($row2 = mysqli_fetch_assoc($resultado2)) {
								$total = $row2['Votsi'];
								$totalsi = $row2['totsi'];
								if ($totalsi == "") $totalsi = 0;
								$totalno = $total - $totalsi;
								if ($total == 0 && $totalsi == 0) {
									$xcent = 0;
								} else {
									$xcent = $totalsi * 100 / $total;
								}
								$xcent = number_format($xcent, 2, '.', '');

								$totalfin = $totalfin + $total;
								$totalsifin = $totalsifin + $totalsi;
								$totalnofin = $totalnofin + $totalno;
							}
							$xcentfin = $totalsifin * 100 / $totalfin;
							$xcentfin = number_format($xcentfin, 2, '.', '');

					?>
					<tr>
						<!-- <td><?php echo $clvmov ?></td> -->
						<td><?php echo $casilla ?></td>
						<td><?php echo $total ?></td>
						<td><?php echo $totalsi ?></td>
						<td><?php echo $totalno ?></td>
						<td><?php echo $xcent ?> %</td>
					</tr>
					<?php 
						}
					?>
					<tr style="background-color: skyblue">
						<!-- <td></td> -->
						<td class="text-center">TOTALES: </td>
						<td><?php echo $totalfin ?></td>
						<td><?php echo $totalsifin ?></td>
						<td><?php echo $totalnofin ?></td>
						<td><?php echo $xcentfin ?> %</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</body>
</html>
