// PARA mantenimiento_seccion.php
$(document).ready(function(){
  //Evento para actualizar el movilizador
  $(".select-filtro").on('change', function(event) {
    //Limpia los campos
    $("#IdCaptura").val("");
    $("#Calle").val("");
    $("#Colonia").val("");
    $("#Manzana").val("");
    $("#Movilizador").val("");
    $("#Folio").val("");

    event.preventDefault();
    /* Act on the event */
    var select = $(this).val();
    //si es busqueda por id
    switch(select)
    {
      case "1":
          //console.log("1\n");
          $(".container-id").removeClass('d-none');
          $(".container-direccion").addClass('d-none');
          $(".container-direccion").removeClass('d-flex');
          $(".container-movilizador").addClass('d-none');
          $(".container-folio").addClass('d-none');
          $(".container-btnFiltro").removeClass('d-none');
      break;
      case "2":
          //console.log("2\n");
          $(".container-id").addClass('d-none');
          $(".container-direccion").removeClass('d-none');
          $(".container-direccion").addClass('d-flex');
          $(".container-movilizador").addClass('d-none');
          $(".container-folio").addClass('d-none');
          $(".container-btnFiltro").removeClass('d-none');
      break;
      case "3":
          //console.log("3\n");
          $(".container-id").addClass('d-none');
          $(".container-direccion").addClass('d-none');
          $(".container-direccion").removeClass('d-flex');
          $(".container-movilizador").removeClass('d-none');
          $(".container-folio").addClass('d-none');
          $(".container-btnFiltro").removeClass('d-none');
      break;
      case "4":
          //console.log("4\n");
          $(".container-id").addClass('d-none');
          $(".container-direccion").addClass('d-none');
          $(".container-direccion").removeClass('d-flex');
          $(".container-movilizador").addClass('d-none');
          $(".container-folio").removeClass('d-none');
          $(".container-btnFiltro").removeClass('d-none');
      break;
    }
  });
   var d = 0;
   $(".btn-filtrar-bridge").on("click", function(event){
    event.preventDefault();
    var IdCaptura = $("#IdCaptura").val(),
        seccion = $("#seccion").val(),
        Calle = $("#Calle").val(),
        Colonia = $("#Colonia").val(),
        Manzana = $("#Manzana").val(),
        Movilizador = $("#Movilizador").val(),
        Folio = $("#Folio").val();
    if(d > 0)
    {
      //alert("Tengo más de 0 ");
      $('#table-filtro').DataTable().destroy();
    }else{
      //alert("Aun no he subido a más de 0");
      d++;
    }
    $('#table-filtro').removeClass('d-none');
    var parameters = {
                        IdCaptura: IdCaptura,
                        Calle: Calle,
                        Colonia: Colonia,
                        Manzana: Manzana,
                        Movilizador: Movilizador,
                        Folio: Folio,
                        Seccion: seccion
                      };
    //console.log($(this).val());
    $('.selecionamov').removeClass('d-none');
    var tablafiltro = $('#table-filtro').DataTable(
      {
        "pageLength": 20,
        "language": {
               "lengthMenu": "Mostrar _MENU_ simpatizantes por página",
               "zeroRecords": "No se ha encontrado ningun elemento - Intente con otra palabra",
               "info": "Página _PAGE_ de _PAGES_",
               "infoFiltered": "(filtrados de _MAX_ simpatizantes)",
               "infoEmpty": "Ningun elemento similar",
               "search": "Buscar:",
               "paginate": {
                "previous": "Atras",
                "next": "Siguiente",
               }
            },
        "ordering": true,
        "ajax": {
                "url": "/digivots/app/panel/controllerPanel/ctr.panel.filtro.Bridge.php",
                "type": "POST",
                "dataType": "JSON",
                "cache": false,
                "data": parameters

                  },
        "columns":[
          {"data":"IdCaptura"},
          {"render":
              function ( data, type, row ) {
                  return (row['PaternoCaptura'] + ' ' + row['MaternoCaptura'] + ' ' + row['NombreCaptura']);
              }
          },
          /*{"render":
              function ( data, type, row ) {
                  return (row['Calle'] + ' ' + row['Noext'] + ' ' + row['Colonia']);
              }
          },*/
          /*{"data": "Colonia"},*/
          {"data": "Seccion"},
          /*{"data": "Movilizador"},
          {"data": "Folio"},
          {"data": "Orden"},*/
          {"data": "CheckLocal"},
          {"data": "SincroBridge"},
          /*{"data": "Estado"},*/
        ],
        "createdRow":
          function( row, data, dataIndex ) {
            $('td', row).eq(4).addClass('col-movilizador');
          },
      });
   });
});
