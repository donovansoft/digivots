// PARA mantenimiento_seccion.php
$(document).ready(function(){
  //Evento para actualizar el movilizador
  $(".select-filtro").on('change', function(event) {
    //Limpia los campos
    $("#IdCaptura").val("");
    $("#Calle").val("");
    $("#Colonia").val("");
    $("#Manzana").val("");
    $("#Movilizador").val("");
    $("#Folio").val("");

    event.preventDefault();
    /* Act on the event */
    var select = $(this).val();
    //si es busqueda por id
    switch(select)
    {
      case "1":
          //console.log("1\n");
          $(".container-id").removeClass('d-none');
          $(".container-direccion").addClass('d-none');
          $(".container-direccion").removeClass('d-flex');
          $(".container-movilizador").addClass('d-none');
          $(".container-folio").addClass('d-none');
          $(".container-btnFiltro").removeClass('d-none');
      break;
      case "2":
          //console.log("2\n");
          $(".container-id").addClass('d-none');
          $(".container-direccion").removeClass('d-none');
          $(".container-direccion").addClass('d-flex');
          $(".container-movilizador").addClass('d-none');
          $(".container-folio").addClass('d-none');
          $(".container-btnFiltro").removeClass('d-none');
      break;
      case "3":
          //console.log("3\n");
          $(".container-id").addClass('d-none');
          $(".container-direccion").addClass('d-none');
          $(".container-direccion").removeClass('d-flex');
          $(".container-movilizador").removeClass('d-none');
          $(".container-folio").addClass('d-none');
          $(".container-btnFiltro").removeClass('d-none');
      break;
      case "4":
          //console.log("4\n");
          $(".container-id").addClass('d-none');
          $(".container-direccion").addClass('d-none');
          $(".container-direccion").removeClass('d-flex');
          $(".container-movilizador").addClass('d-none');
          $(".container-folio").removeClass('d-none');
          $(".container-btnFiltro").removeClass('d-none');
      break;
    }
  });
   var d = 0;
   $(".btn-filtrar").on("click", function(event){
    event.preventDefault();
    var IdCaptura = $("#IdCaptura").val(),
        seccion = $("#seccion").val(),
        Calle = $("#Calle").val(),
        Colonia = $("#Colonia").val(),
        Manzana = $("#Manzana").val(),
        Movilizador = $("#Movilizador").val(),
        Folio = $("#Folio").val();
    if(d > 0)
    {
      //alert("Tengo más de 0 ");
      $('#table-filtro').DataTable().destroy();
    }else{
      //alert("Aun no he subido a más de 0");
      d++;
    }
    $('#table-filtro').removeClass('d-none');
    var parameters = {
                        IdCaptura: IdCaptura,
                        Calle: Calle,
                        Colonia: Colonia,
                        Manzana: Manzana,
                        Movilizador: Movilizador,
                        Folio: Folio,
                        Seccion: seccion
                      };
    //console.log($(this).val());
    $('.selecionamov').removeClass('d-none');
    var tablafiltro = $('#table-filtro').DataTable(
      {
        "pageLength": 20,
        "language": {
               "lengthMenu": "Mostrar _MENU_ simpatizantes por página",
               "zeroRecords": "No se ha encontrado ningun elemento - Intente con otra palabra",
               "info": "Página _PAGE_ de _PAGES_",
               "infoFiltered": "(filtrados de _MAX_ simpatizantes)",
               "infoEmpty": "Ningun elemento similar",
               "search": "Buscar:",
               "paginate": {
                "previous": "Atras",
                "next": "Siguiente",
               }
            },
        "ordering": true,
        "ajax": {
                "url": "/digivots/app/panel/controllerPanel/ctr.panel.filtro.php",
                "type": "POST",
                "dataType": "JSON",
                "cache": false,
                "data": parameters

                  },
        "columns":[
          {"data":"IdCaptura"},
          {"render":
              function ( data, type, row ) {
                  return (row['PaternoCaptura'] + ' ' + row['MaternoCaptura'] + ' ' + row['NombreCaptura']);
              }
          },
          {"render":
              function ( data, type, row ) {
                  return (row['Calle'] + ' ' + row['Noext'] + ' ' + row['Colonia']);
              }
          },
          {"data": "Colonia"},
          {"data": "Seccion"},
          {"data": "Movilizador"},
          {"data": "Folio"},
          {"data": "Orden"},
          {"data": "CheckLocal"},
          {"data": "SincroBridge"},
          {"data": "Estado"},
/*          {"render":
              function ( data, type, row ) {
                  //console.log("datos", type);
                  //return("<input type='tel' class='input-movilizador form-control' placeholder='Nuevo Movilizador'>");
                  return("<span class='d-none'>1</span><select name='js-movilizador' id='js-movilizador' class='js-movilizador custom-select'></select>");
              }
          },
          {"render":
              function ( data, type, row ) {
                  return ("<button class='btn-update-movilizador mr-2 btn btn-outline-info px-4' data-id='" + row['IdCaptura']  + "' data-movilizador='" + row['Movilizador'] + "'' title='Actualizar Movilizador' style='padding: 0px; margin: 0px; cursor: pointer;'><i class='fa fa-upload'></i></button>");
              }
          },*/
        ],
        "createdRow":
          function( row, data, dataIndex ) {
            /*if ( data[1] == "1188602" ) {
              $(row).addClass( 'important' );
            }else{
              $(row).addClass( 'no-important' );
            }*/
            $('td', row).eq(4).addClass('col-movilizador');
          },
      });

    $("#Ids-Movilizador").on("click", function(event){
      event.preventDefault();
      var selected = [];
      $('#table-filtro input:checked').each(function() {
          selected.push({id: $(this).attr('value')});
      });
      var mov = $('#NuevoMovilizador').val();
      if (mov == null) {
        alert("No has seleccionado al movilizador");
        return;
      }
      //AQUI VA EL CONTROLADOR PARA ENVIAR LOS ID'S DE ESTE GRUPO DE CHEQUEOS
      console.log(selected);
      //return false;
      $.ajax({
        url: "../controller/ctrl.mov.movilizador.php",
        type: "POST",
        dataType: "JSON",
        data: {
                mov: mov,
                ids : selected
              },
        success: function(data)
        {
          if (data) {
            
            /*setTimeout(function(){
              location.reload();
            }, 1000);*/
            alert("Registros guardados!");
          }
          else {
            alert("Ha ocurrido un error!");
            return;
          }
          idbase64 = "" + data.IdCaptura;
          movbase64 = "" + data.Movilizador;
          estbase64 = "" + data.Estado;
          $.ajax({
            url: "http://sysreg.tech/bridge/funciones/update_vots.php",
            type: "POST",
            dataType: "JSON",
            data: { 
                    u : 'syscam.consulta',
                    k : 'cnslt140u4sscm',
                    IdCaptura : idbase64,
                    Movilizador : movbase64,
                    Estado : estbase64},
            success: function(data)
            {
              if(data.estatus == "INSERTADO" || data.estatus == "ACTUALIZADO")
              {
                $.ajax({
                  url: "../controller/ctrl.update.bridge.movilizador2.php",
                  type: "POST",
                  dataType: "JSON",
                  data: { 
                          IdCaptura : id},
                  success: function(data)
                  {
                  }, 
                  error: function(errno){
                      console.log(errno);
                  }
                })
                .done(function() {
                  console.log("success");
                })
                .fail(function() {
                  console.log("error");
                })
                .always(function() {
                  console.log("complete");
                });
              }
            }, 
            error: function(errno){
              console.log(errno); 
            }
          })
          .done(function() {
            console.log("success");
          })
          .fail(function(error) {
            console.log(error);
          })
          .always(function() {
            console.log("complete");
          });
        },
      })
      .done(function() {
        console.log("success");
      })
      .fail(function() {
        console.log("error");
      })
      .always(function() {
        console.log("complete");
      });
    });

     function agregar()
     {
        $.ajax({
           url: '../controller/ctrl.movilizadores.php',
           type: 'POST',
           dataType: 'JSON',
           success: function(data)
           {
              $('.js-movilizador').html('');
              //$('.js-movilizador').append("<input type='tel' class='input-movilizador form-control' placeholder='Nuevo Movilizador'>");
              var inputSelect = "";
              for(var x in data)
              {
                for(var y in data[x])
                {
                  $('.js-movilizador').append("<option value='" + data[x][y].ClaveMovilizador + "' >" + data[x][y].PaternoMovilizador + " " + data[x][y].MaternoMovilizador + " " + data[x][y].NombreMovilizador);
                  //console.log('Prueba: ' + data[x][y].ClaveMovilizador);
                }
              }
           }
        })
        .done(function() {
           console.log("success");
        })
        .fail(function() {
           console.log("error");
        })
        .always(function() {
           console.log("complete");
        });
     }
     setTimeout(function(){
        agregar();
     }, 100);
   });
   $('body').on('click', '.btn-update-movilizador', function(event) {
       event.preventDefault();
       var movAnterior = $(this).attr('data-movilizador'),
           idCaptura = $(this).attr('data-id')
           movNuevo = $(this).parent('td').siblings('td.col-movilizador').children('.js-movilizador').val();
       //console.log('Id: ' + idCaptura + '\nMovilizador Anterior: ' + movAnterior + '\nMovilizador Nuevo: ' + movNuevo);
       $.ajax({
         url: '../controller/ctrl.update.movilizador2.php',
         type: 'POST',
         dataType: 'JSON',
         data: {
                 id: idCaptura,
                 mov_ant: movAnterior,
                 movilizador: movNuevo
               },
         success: function(data)
         {
            if (data) {
        
              setTimeout(function(){
                location.reload();
              }, 1000);
              alert("Registros guardados!");
            }
            else {
              alert("Ha ocurrido un error!");
              return;
            }
            idbase64 = "" + data.IdCaptura;
            movbase64 = "" + data.Movilizador;
            estbase64 = "" + data.Estado;
            $.ajax({
              url: "http://sysreg.tech/bridge/funciones/update_vots.php",
              type: "POST",
              dataType: "JSON",
              data: { 
                      u : 'syscam.consulta',
                      k : 'cnslt140u4sscm',
                      IdCaptura : idbase64,
                      Movilizador : movbase64,
                      Estado : estbase64},
              success: function(data)
              {
                if(data.estatus == "INSERTADO" || data.estatus == "ACTUALIZADO")
                {
                  //alert("SINCRONIZADO");
                  $.ajax({
                    url: "../controller/ctrl.update.bridge.movilizador2.php",
                    type: "POST",
                    dataType: "JSON",
                    data: { 
                            IdCaptura : id},
                    success: function(data)
                    {
                    }, 
                    error: function(errno){
                        console.log(errno);
                    }
                  })
                  .done(function() {
                    console.log("success");
                  })
                  .fail(function() {
                    console.log("error");
                  })
                  .always(function() {
                    console.log("complete");
                  });
                }
              }, 
              error: function(errno){
                console.log(errno); 
              }
            })
            .done(function() {
              console.log("success");
            })
            .fail(function(error) {
              console.log(error);
            })
            .always(function() {
              console.log("complete");
            });
         }
       })
       .done(function() {
         console.log("success");
       })
       .fail(function() {
         console.log("error");
       })
       .always(function() {
         console.log("complete");
       });
       
    });
});
