<!-- Inicio CSS -->
	<!-- LIBRERIA BOOTSTRAP 4.0 -->
	<link rel="stylesheet" type="text/css" href="/digivots/assets/booststrap/css/bootstrap.min.css">

	<!-- DataTable 1.10 -->
	<link rel="stylesheet" type="text/css" href="/digivots/assets/DataTables/datatables.min.css">

	<!-- Fontawesome 4.7 -->
	<link rel="stylesheet" type="text/css" href="/digivots/assets/icon-awesome/css/font-awesome.min.css">

	<!-- Css Custom -->
	<link rel="stylesheet" type="text/css" href="/digivots/css/main.css">
<!-- Fin CSS -->

<!-- Inicio JS Script -->
	<!-- JQuery 3.2.1 -->
	<script src="/digivots/assets/jquery/jquery.js"></script>

	<!-- DataTable 1.10 -->
	<script src="/digivots/assets/DataTables/datatables.min.js"></script>

	<!-- Bootstrap 4.0  -->
	<script src="/digivots/assets/booststrap/js/bootstrap.min.js"></script>

	<!-- Js Custom -->
	<script src="../panel/js/panel.js"></script>
	<!-- Js Custom -->
	<script src="../panel/js/panelServer.js"></script>
	<!-- Js Custom -->
	<script src="../panel/js/panelBridge.js"></script>
<!-- Fin JS Script -->
<!--modal-->
