<?php 
include '../controller/rutalinea.php';
include '../controller/conexion.php';
?>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <a class="navbar-brand" href="#">Inicio</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
        <li class="navbar-item active"><a class="nav-link" href="../panel/panelLocal.php">Panel Local<span class="sr-only">(current)</span></a></li>
        <li class="navbar-item active"><a class="nav-link" href="../panel/panelServer.php">Panel Server<span class="sr-only">(current)</span></a></li>
        <li class="navbar-item active"><a class="nav-link" href="../panel/panelBridge.php">Panel Bridge<span class="sr-only">(current)</span></a></li>       
    </ul>    
  </div>
</nav>

