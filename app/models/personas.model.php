<?php
    //Objeto Usuario Sesión
    class personasModel{
        private $db;
        private $personas;

        //Generamos un constructor para la conexion a la BD
        public function __construct(){
            $this->db=conectar::conexion();
            $this->personas=array();
        }
        //Funcion para obtener personas
        public function get_personas($rows = '*', $where = null, $order = null, $limit = null)
        {
          array_walk($rows, function (&$value, $key) {
             $value="AES_DECRYPT($value, $linea))";
          });
          if($rows != '*'){
            $rows = implode(",",$rows);
          }
          $sql = 'SELECT '.$rows.' FROM personas';
          if($where != null){
            $k=0;
            foreach($where as $key=>$val){
              if($k==0){
                $sql .= " where $key='".htmlentities($val, ENT_QUOTES)."'";
              }else{
                $sql .= " AND $key='".htmlentities($val, ENT_QUOTES)."'";
              }
              $k++;
            }
          }
          if($order != null){
            foreach($order as $key=>$val){
              $sql .= " ORDER BY $key ".htmlentities($val, ENT_QUOTES)."";
            }
          }
          if($limit != null){
            $limit = implode(",",$limit);
            $sql .= " LIMIT $limit";
          }
          $consulta = $this->db->query($sql);
          while($row = $consulta->fetch_assoc()){
              $this->personas[] = $row;
          }
          return $this->personas;
        }
        // insert personas
        public function insert_personas($values, $linea)
        {
          $sql = "INSERT INTO personas SET ";
          $c=0;
          if(!empty($values)){
            foreach($values as $key=>$val){
              if($c==0){
                  $sql .= "$key=AES_ENCRYPT('".htmlentities($val, ENT_QUOTES)."', '$linea')";
              }else{
                  $sql .= ", $key=AES_ENCRYPT('".htmlentities($val, ENT_QUOTES)."', '$linea')";
              }
              $c++;
            }
          }else{
            $this->error = "Valores vacios (410)";
            return false;
          }
          $result = $this->db->query($sql);
          if ($result) {
            return mysqli_insert_id($this->db);
          }else {
            return $result;
          }

        }
        // Actualizar datos en personas
        public function update_personas($values,$condition, $linea)
        {
          $sql="UPDATE personas SET ";
          $c=0;
          if(!empty($values)){
            foreach($values as $key=>$val){
                if($c==0){
                    $sql .= "$key=AES_ENCRYPT('".htmlentities($val, ENT_QUOTES)."', '$linea')";
                }else{
                    $sql .= ", $key=AES_ENCRYPT('".htmlentities($val, ENT_QUOTES)."', '$linea')";
                }
                $c++;
            }
          }
          $k=0;
          if(!empty($condition)){
              foreach($condition as $key=>$val){
                  if($k==0){
                      $sql .= " WHERE $key=AES_ENCRYPT('".htmlentities($val, ENT_QUOTES)."', '$linea')";
                  }else{
                      $sql .= " AND $key=AES_ENCRYPT('".htmlentities($val, ENT_QUOTES)."', '$linea')";
                  }
                  $k++;
              }
          }else{
            return false;
          }
          $result = $this->db->query($sql);
          if (!$result) {
            $this->error = $this->db->error;
            return false;
          }
          return $result;
        }

        public function delete_personas($where, $linea)
        {
          $sql = "DELETE FROM eliminados ";
          $k=0;
          if(!empty($where)){
              foreach($where as $key=>$val){
                  if($k==0){
                      $sql .= " where $key=AES_ENCRYPT('".htmlentities($val, ENT_QUOTES)."', '$linea')";
                  }else{
                      $sql .= " AND $key=AES_ENCRYPT('".htmlentities($val, ENT_QUOTES)."', '$linea')";
                  }
                  $k++;
              }
          }else{
            $this->error = "Parametros de consulta erronea";
            return false;
          }
          $del = $this->db->query($sql);
          if($del){
            return true;
          }else{
            $this->error = $this->db->error;
            return false;
           }
        }
        //Funcion para obtener Secciones
        public function get_seccion($rows, $linea, $order, $group)
        {
          $sql = "SELECT AES_DECRYPT($rows, '$linea') as Seccion FROM personas GROUP BY Seccion ORDER BY Seccion";
          
          $consulta = $this->db->query($sql);
          while($row = $consulta->fetch_assoc()){
              $this->seccion[] = $row;
          }
          return $this->seccion;
        }
    }
?>
