<?php
    //Objeto Usuario Sesión
    class usuarioSesion{
        private $db;
        private $usuarios;
        
        //Generamos un constructor para la conexion a la BD
        public function __construct(){
            $this->db = conectar::conexion();
            $this->usuarios = array();
        }
        //Funcion para obtener la consulta deseada del Usuario
        public function get_usuarios($username, $pass, $linea){
            $consulta = $this->db->query("SELECT *, AES_DECRYPT(pass_key, '$linea') as pass_key_d FROM usuarios WHERE UPPER(user) = '$username' AND '$pass' = UPPER(AES_DECRYPT(pass_key,'$linea'));");
            while($row = $consulta->fetch_assoc()){
                $this->usuarios[] = $row;
            }
            return $this->usuarios;
        }
    }
?>