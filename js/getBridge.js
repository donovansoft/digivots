$(document).ready(function() {
  "use strict";
  var $flag = false;
  $("#btn-bridge").on("click", function(){
    var seccioninicio = $('#inicio').val(), seccionfin = $('#fin').val();
    alert(seccioninicio + "\n" + seccionfin);
    if(seccionfin >= seccioninicio)
    {
      getDataBridge(seccioninicio, seccionfin);
      //alert("El final es mayor o igual al inicio");
    }else{
      alert("Favor de seleccionar un valor mayor o igual a la sección seleccionada como inicio, gracias!");
      $('#fin').focus();
    }
    
  });
  $(document).keydown(function(e) {
    if (e.keyCode === 116)
    {
      if($flag)
      {
        console.log("Procede a refrescar");
      }else{
        e.preventDefault();
        console.log("Hasta que termine de actualizar podras refrescar el sitio, se paciente, Gracias!");
      }
    }
  });
  function getDataBridge(seccioninicio, seccionfin) {
    $flag = false;
    $.ajax({
      url: "/digivots/app/controller/save.bridge.con.php",
      "type": "POST",
      dataType: "json",
      data: {SeccionInicio: seccioninicio, SeccionFin: seccionfin},
      success: function(response){
        console.log(response);
        /*if(response)
        {
          $flag = true;
          setTimeout(function(){
            location.reload();
          }, 1000);
        }*/
      },
      error: function(error){
        console.log(error.statusText)
      },
    });
  }
})
