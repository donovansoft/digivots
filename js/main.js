//Ready para main.php
$(document).ready(function() {
	var d = 0;
  $('.clave-movilizador-main').on('change', function(event) {
    event.preventDefault();
    /* Act on the event */
    if(d > 0)
    {
      //alert("Tengo más de 0 ");
      $('#table-votantes').DataTable().destroy();
    }else{
      //alert("Aun no he subido a más de 0");
      d++;
    }
    $('#table-votantes').removeClass('d-none');
    var clave = $(this).val();
    var parameters = {clave:$(this).val()};
    //console.log($(this).val());
    var tablavotantes = $('#table-votantes').DataTable(
      {
        "pageLength": 20,
        "language": {
               "lengthMenu": "Mostrar _MENU_ simpatizantes por página",
               "zeroRecords": "No se ha encontrado ningun elemento - Intente con otra palabra",
               "info": "Página _PAGE_ de _PAGES_",
               "infoFiltered": "(filtrados de _MAX_ simpatizantes)",
               "infoEmpty": "Ningun elemento similar",
               "search": "Buscar:",
               "paginate": {
                "previous": "Atras",
                "next": "Siguiente",
               }
            },
        'ordering': true,
        "ajax": {
                "url": "../controller/ctrl.movilizador.php",
                "type": "POST",
                "dataType": "JSON",
                "cache": false,
                "data": parameters

                  },
        "columns":[
          {"data":"IdCaptura"},
          {"render":
              function ( data, type, row ) {
                  return (row['PaternoCaptura'] + ' ' + row['MaternoCaptura'] + ' ' + row['NombreCaptura']);
              }
          },
          {"data":"FechaNacimiento"},
          {"data":"Casilla"},
          {"render":
              function ( data, type, row ) {
                  return (row['CheckLocal'] === "0" ? "<span class='d-none'>0</span><button class='btn-votar mr-2 btn btn-outline-success px-4' data-id='" + row['IdCaptura']  + "' data-nombre='" + row['PaternoCaptura'] + " " + row['MaternoCaptura'] + " " + row['NombreCaptura'] + "' title='Asistió' style='padding: 0px; margin: 0px; cursor: pointer;'><i class='fa fa-check-circle'></i></button>"+"<button class='btn-quitar-asistencia mr-2 btn btn-outline-danger px-4' data-id='" + row['IdCaptura']  + "' data-nombre='" + row['PaternoCaptura'] + " " + row['MaternoCaptura'] + " " + row['NombreCaptura'] + "' title='No asistió' disabled='true' style='padding: 0px; margin: 0px; cursor: pointer;'><i class='fa fa-times-circle'></i></button>" : "<span class='d-none'>1</span><button class='btn-votar mr-2 btn btn-outline-success px-4' data-id='" + row['IdCaptura']  + "' data-nombre='" + row['PaternoCaptura'] + " " + row['MaternoCaptura'] + " " + row['NombreCaptura'] + "' title='Asistió' disabled='true' style='padding: 0px; margin: 0px; cursor: pointer;'><i class='fa fa-check-circle'></i></button>"+"<button class='btn-quitar-asistencia mr-2 btn btn-outline-danger px-4' data-id='" + row['IdCaptura']  + "' data-nombre='" + row['PaternoCaptura'] + " " + row['MaternoCaptura'] + " " + row['NombreCaptura'] + "' title='No asistió' style='padding: 0px; margin: 0px; cursor: pointer;'><i class='fa fa-times-circle'></i></button>");
              }
          },
        ],
        "createdRow":
          function( row, data, dataIndex ) {
            /*if ( data[1] == "1188602" ) {
              $(row).addClass( 'important' );
            }else{
              $(row).addClass( 'no-important' );
            }*/
            if($('td', row).eq(4).text() === '1')
            {
              //console.log('Estoy en', row);
              $(row).addClass( 'row-simpatizante g-bg-add' );
            }else{
              //console.log('Estoy en', row);
              $(row).addClass( 'row-simpatizante' );
            }
          },
      });
    $('.container-print').html("");
    $('.container-print').append('<a href="../reportes/reporteMovilizador2.php?clave_mov=' + clave + '" target="-" class="btn btn-success" title="Imprimir">Imprimir</a>');
    //$('.container-print').append('<button type="button" class="btn btn-primary" title="Enviar" data-toggle="modal" data-target=".bd-example-modal-sm">Enviar por correo</button>');
  });

  function sincronizardides() {
    $.ajax({
      url : 'http://sysreg.tech/bridge/funciones/consulta_ides.php',
      type : 'POST',
      dataType : 'JSON',
      data : {
        u : 'syscam.consulta',
        k : 'cnslt140u4sscm',
        sistem: '2',
        action: '0'
      },
      success : function(response){
        console.log(response);
        $.each(response.ides, function(index, value){
          //alert("ID: " + value.id);
          $.ajax({
            url: '../controller/ctrl.update.asistencia.si.php',
            type: 'POST',
            dataType: 'JSON',
            data: {id: value.id},
            success: function(data)
            {
              if(data)
              {
                //ENVIO DE DATOS AL BRIDGE
                var idbase64 = data.IdCaptura;
                var chkbase64 = data.CheckLocal;
                $.ajax({
                  url: "http://sysreg.tech/bridge/funciones/update_vots.php",
                  type: "POST",
                  dataType: "JSON",
                  data: {
                          u : 'syscam.consulta',
                          k : 'cnslt140u4sscm',
                          IdCaptura : idbase64,
                          CheckLocal : chkbase64
                        },
                  success: function(data)
                  {
                    if(data.estatus == "INSERTADO" || data.estatus == "ACTUALIZADO")
                    {
                      $.ajax({
                        url: "../controller/ctrl.update.bridge.sincrobridge.php",
                        type: "POST",
                        dataType: "JSON",
                        data: {
                                IdCaptura : value.id},
                        success: function(data)
                        {
                        },
                        error: function(errno){
                        }
                      })
                      .done(function() {
                        console.log("success");
                      })
                      .fail(function() {
                        console.log("error");
                      })
                      .always(function() {
                        console.log("complete");
                      });

                      //alert("SINCRONIZADO");
                    }
                  },
                  error: function(errno){
                  }
                })
                .done(function() {
                  console.log("success");
                })
                .fail(function() {
                  console.log("error");
                })
                .always(function() {
                  console.log("complete");
                });
              }else{
                alert("Algo sucedió en el cambio de CheckLocal");
              }
            }
          })
          .done(function() {
            console.log("success");
          })
          .fail(function() {
            console.log("error");
          })
          .always(function() {
            console.log("complete");
          });
        });
      }
    });

  }

	//Botón votar
	$('body').on('click', '.btn-votar', function(event) {
		event.preventDefault();
		//var confirm = confirm("¿Estás seguro(a) de que es la persona correcta?");
    if(confirm("¿Estás seguro(a) de que es la persona correcta?"))
    {
      var btn = $(this);
      var idVotar = $(this).attr('data-id');
      var nombreVotar = $(this).attr('data-nombre');
      $.ajax({
        url: '../vistaDiaDe/ctrol.sincro.ides.php',
        type: 'POST',
        dataType: 'JSON',
        data: {id: idVotar},
        success: function(mensaje)
        {
          if (mensaje == true) {
            //alert("SI");
            sincronizardides();
          }
        }
      });
      $.ajax({
        url: '../controller/ctrl.update.asistencia.si.php',
        type: 'POST',
        dataType: 'JSON',
        data: {id: idVotar},
        success: function(data)
        {
          if(data)
          {
            //alert("Asisencia agregada al simpatizante " + nombreVotar);
            btn.siblings('.btn-quitar-asistencia').removeAttr('disabled');
            btn.attr('disabled', 'true');
            btn.parent('td').parent('tr.row-simpatizante').addClass('g-bg-add');
            //ENVIO DE DATOS AL BRIDGE
            var idbase64 = data.IdCaptura;
            var chkbase64 = data.CheckLocal;
            var idextbase64 = data.IdExterno; //Adición hecha el 29/06/18 a las 12:09 am
            $.ajax({
              url: "http://sysreg.tech/bridge/funciones/update_vots.php",
              type: "POST",
              dataType: "JSON",
              data: {
                      u : 'syscam.consulta',
                      k : 'cnslt140u4sscm',
                      IdCaptura : idbase64,
                      SincroBridge : chkbase64,
                      ides : idextbase64, //Adición hecha el 29/06/18 a las 12:09 am
                      sistem : '1',
                      action : '1'
                    },
              success: function(data)
              {
                if(data.estatus == "INSERTADO" || data.estatus == "ACTUALIZADO")
                {
                  $.ajax({
                    url: "../controller/ctrl.update.bridge.sincrobridge.php",
                    type: "POST",
                    dataType: "JSON",
                    data: {
                            IdCaptura : idVotar},
                    success: function(data)
                    {
                    },
                    error: function(errno){
                    }
                  })
                  .done(function() {
                    console.log("success");
                  })
                  .fail(function() {
                    console.log("error");
                  })
                  .always(function() {
                    console.log("complete");
                  });

                  //alert("SINCRONIZADO");
                }
              },
              error: function(errno){
              }
            })
            .done(function() {
              console.log("success");
            })
            .fail(function() {
              console.log("error");
            })
            .always(function() {
              console.log("complete");
            });
          }else{
            alert("Algo sucedió en el cambio de CheckLocal");
          }
        }
      })
      .done(function() {
        console.log("success");
      })
      .fail(function() {
        console.log("error");
      })
      .always(function() {
        console.log("complete");
      });

    }else
    {
      console.log("No");
    }
	});

  //CONTROLADOR LIMITADOR DE CHEQUEOS
  var contvotos = 0;
  var idanterior = "";
  //Botón quitar asistencia
  
  $('body').on('click', '.btn-quitar-asistencia', function(event) {
    event.preventDefault();
    //var confirm = confirm("¿Estás seguro(a) de que es la persona correcta?");
    if(confirm("¿Estás seguro(a) de que deseas quitar la asistencia de ésta persona?"))
    {
      var btn = $(this);
      var idVotar = $(this).attr('data-id');
      var nombreVotar = $(this).attr('data-nombre');
      if (idanterior == idVotar) {
        contvotos++;
      } else {
        idanterior = idVotar;
        contvotos = 0;
      }
      $.ajax({
        url: '../controller/ctrl.update.asistencia.no.php',
        type: 'POST',
        dataType: 'JSON',
        data: {id: idVotar},
        success: function(data)
        {
          if(data)
          {
            if (contvotos > 1) {
              ModalMonsterInc();
            }
            //alert("Asisencia eliminada al simpatizante " + nombreVotar);
            btn.siblings('.btn-votar').removeAttr('disabled');
            btn.attr('disabled', 'true');
            btn.parent('td').parent('tr.row-simpatizante').removeClass('g-bg-add');
            //ENVIO DE DATOS AL BRIDGE
            var idbase64 = data.IdCaptura;
            var chkbase64 = data.CheckLocal;
            var idextbase64 = data.IdExterno; //Adición hecha el 29/06/18 a las 12:09 am
            $.ajax({
              url: "http://sysreg.tech/bridge/funciones/update_vots.php",
              type: "POST",
              dataType: "JSON",
              data: {
                      u : 'syscam.consulta',
                      k : 'cnslt140u4sscm',
                      IdCaptura : idbase64,
                      SincroBridge : chkbase64,
                      ides : idextbase64, //Adición hecha el 29/06/18 a las 12:09 am
                      sistem : '1',
                      action : '0'
                    },
              success: function(data)
              {
                if(data.estatus == "INSERTADO" || data.estatus == "ACTUALIZADO")
                {
                  $.ajax({
                    url: "../controller/ctrl.update.bridge.sincrobridge.php",
                    type: "POST",
                    dataType: "JSON",
                    data: {
                            IdCaptura : idVotar},
                    success: function(data)
                    {
                    },
                    error: function(errno){
                    }
                  })
                  .done(function() {
                    console.log("success");
                  })
                  .fail(function() {
                    console.log("error");
                  })
                  .always(function() {
                    console.log("complete");
                  });

                  //alert("SINCRONIZADO");
                }
              },
              error: function(errno){
              }
            })
            .done(function() {
              console.log("success");
            })
            .fail(function() {
              console.log("error");
            })
            .always(function() {
              console.log("complete");
            });
          }else{
            alert("Algo sucedió en el cambio de CheckLocal");
          }
        }
      })
      .done(function() {
        console.log("success");
      })
      .fail(function() {
        console.log("error");
      })
      .always(function() {
        console.log("complete");
      });
    }else
    {
      console.log("No");
    }
  });

  //Función Validar que contenga solo caracteres
  function ValidarUsername(username)
  {
    if ((/^([A-Z\s\a-z])*(\.)*([A-Z\s\a-z])*$/.test(username)) && (username.length != '' || username.length != null))
    {
      return (true)
    }
    else
    {
      alert('¡Por favor ingresa un Username con el siguiente formato User.Name');
      return (false)
    }
  }

  $('#username').on('change', function(event) {
    ValidarUsername($(this).val());
  });
});

//Ready para main.casilla.php
$(document).ready(function() {
  var e = 0;
  $('.clave-casilla-main').on('change', function(event) {
    event.preventDefault();
    /* Act on the event */
    if(e > 0)
    {
      //alert("Tengo más de 0 ");
      $('#table-casilla').DataTable().destroy();
    }else{
      //alert("Aun no he subido a más de 0");
      e++;
    }
    $('#table-casilla').removeClass('d-none');
    var casilla = $(this).val();
    var parameters = {casilla:$(this).val()};
    //console.log($(this).val());
    var tablacasilla = $('#table-casilla').DataTable(
      {
        "pageLength": 20,
        "language": {
               "lengthMenu": "Mostrar _MENU_ simpatizantes por página",
               "zeroRecords": "No se ha encontrado ningun elemento - Intente con otra palabra",
               "info": "Página _PAGE_ de _PAGES_",
               "infoFiltered": "(filtrados de _MAX_ simpatizantes)",
               "infoEmpty": "Ningun elemento similar",
               "search": "Buscar:",
               "paginate": {
                "previous": "Atras",
                "next": "Siguiente",
               }
            },
        "ordering": false,
        "ajax": {
                "url": "../controller/ctrl.casillas.php",
                "type": "POST",
                "dataType": "JSON",
                "cache": false,
                "data": parameters

                  },
        "columns":[
          {"data":"IdCaptura"},
          {"render":
              function ( data, type, row ) {
                  return (row['PaternoCaptura']);
              }
          },
          {"render":
              function ( data, type, row ) {
                  return (row['MaternoCaptura']);
              }
          },
          {"render":
              function ( data, type, row ) {
                  return (row['NombreCaptura']);
              }
          },
          /*{"data":"FechaNacimiento"},
          {"render":
              function ( data, type, row ) {
                  return (row['pat'] + ' ' + row['matm'] + ' ' + row['nom']);
              }
          },*/
          {"render":
              function ( data, type, row ) {
                  return (row['CheckLocal'] === "0" ? "<span class='d-none'>0</span><button class='btn-casilla-votar mr-2 btn btn-outline-success px-4' data-id='" + row['IdCaptura']  + "' data-nombre='" + row['PaternoCaptura'] + " " + row['MaternoCaptura'] + " " + row['NombreCaptura'] + "' title='Asistió' style='padding: 0px; margin: 0px; cursor: pointer;'><i class='fa fa-check-circle'></i></button>"+"<button class='btn-casilla-quitar-asistencia mr-2 btn btn-outline-danger px-4' data-id='" + row['IdCaptura']  + "' data-nombre='" + row['PaternoCaptura'] + " " + row['MaternoCaptura'] + " " + row['NombreCaptura'] + "' title='No asistió' disabled='true' style='padding: 0px; margin: 0px; cursor: pointer;'><i class='fa fa-times-circle'></i></button>" : "<span class='d-none'>1</span><button class='btn-casilla-votar mr-2 btn btn-outline-success px-4' data-id='" + row['IdCaptura']  + "' data-nombre='" + row['PaternoCaptura'] + " " + row['MaternoCaptura'] + " " + row['NombreCaptura'] + "' title='Asistió' disabled='true' style='padding: 0px; margin: 0px; cursor: pointer;'><i class='fa fa-check-circle'></i></button>"+"<button class='btn-casilla-quitar-asistencia mr-2 btn btn-outline-danger px-4' data-id='" + row['IdCaptura']  + "' data-nombre='" + row['PaternoCaptura'] + " " + row['MaternoCaptura'] + " " + row['NombreCaptura'] + "' title='No asistió' style='padding: 0px; margin: 0px; cursor: pointer;'><i class='fa fa-times-circle'></i></button>");
              }
          },
        ],
        "createdRow":
          function( row, data, dataIndex ) {
            /*if ( data[1] == "1188602" ) {
              $(row).addClass( 'important' );
            }else{
              $(row).addClass( 'no-important' );
            }*/
            if($('td', row).eq(4).text() === '1')
            {
              //console.log('Estoy en', row);
              $(row).addClass( 'row-simpatizante g-bg-add' );
            }else{
              //console.log('Estoy en', row);
              $(row).addClass( 'row-simpatizante' );
            }
          },
      });
  });
  //Botón Casilla votar
  $('body').on('click', '.btn-casilla-votar', function(event) {
    event.preventDefault();
    //var confirm = confirm("¿Estás seguro(a) de que es la persona correcta?");
    if(confirm("¿Estás seguro(a) de que es la persona correcta?"))
    {
      var btn = $(this);
      var idVotar = $(this).attr('data-id');
      var nombreVotar = $(this).attr('data-nombre');
      $.ajax({
        url: '../vistaDiaDe/ctrol.sincro.ides.php',
        type: 'POST',
        dataType: 'JSON',
        data: {id: idVotar},
        success: function(mensaje)
        {
          if (mensaje == true) {
            //alert("SI");
            sincronizardides();
          }
        }
      });
      $.ajax({
        url: '../controller/ctrl.update.asistencia.si.php',
        type: 'POST',
        dataType: 'JSON',
        data: {id: idVotar},
        success: function(data)
        {
          if(data)
          {
            //alert("Asisencia agregada al simpatizante " + nombreVotar);
            btn.siblings('.btn-casilla-quitar-asistencia').removeAttr('disabled');
            btn.attr('disabled', 'true');
            btn.parent('td').parent('tr.row-simpatizante').addClass('g-bg-add');
            //ENVIO DE DATOS AL BRIDGE
            var idbase64 = data.IdCaptura;
            var chkbase64 = data.CheckLocal;
            var idextbase64 = data.IdExterno; //Adición hecha el 29/06/18 a las 12:09 am
            $.ajax({
              url: "http://sysreg.tech/bridge/funciones/update_vots.php",
              type: "POST",
              dataType: "JSON",
              data: {
                      u : 'syscam.consulta',
                      k : 'cnslt140u4sscm',
                      IdCaptura : idbase64,
                      SincroBridge : chkbase64,
                      ides : idextbase64, //Adición hecha el 29/06/18 a las 12:09 am
                      sistem : '1',
                      action : '1'
                    },
              success: function(data)
              {
                if(data.estatus == "INSERTADO" || data.estatus == "ACTUALIZADO")
                {
                  $.ajax({
                    url: "../controller/ctrl.update.bridge.sincrobridge.php",
                    type: "POST",
                    dataType: "JSON",
                    data: {
                            IdCaptura : idVotar},
                    success: function(data)
                    {
                    },
                    error: function(errno){
                    }
                  })
                  .done(function() {
                    console.log("success");
                  })
                  .fail(function() {
                    console.log("error");
                  })
                  .always(function() {
                    console.log("complete");
                  });

                  //alert("SINCRONIZADO");
                }
              },
              error: function(errno){
              }
            })
            .done(function() {
              console.log("success");
            })
            .fail(function() {
              console.log("error");
            })
            .always(function() {
              console.log("complete");
            });
          }else{
            alert("Algo sucedió en el cambio de CheckLocal");
          }
        }
      })
      .done(function() {
        console.log("success");
      })
      .fail(function() {
        console.log("error");
      })
      .always(function() {
        console.log("complete");
      });

    }else
    {
      console.log("No");
    }
  });
  //Botón Casilla quitar asistencia
  $('body').on('click', '.btn-casilla-quitar-asistencia', function(event) {
    var idanterior = "";
    event.preventDefault();
    //var confirm = confirm("¿Estás seguro(a) de que es la persona correcta?");
    if(confirm("¿Estás seguro(a) de que deseas quitar la asistencia de ésta persona?"))
    {
      if (idanterior == idVotar) {
        contvotos++;
      } else {
        idanterior = idVotar;
        contvotos = 0;
      }
      var btn = $(this);
      var idVotar = $(this).attr('data-id');
      var nombreVotar = $(this).attr('data-nombre');
      $.ajax({
        url: '../controller/ctrl.update.asistencia.no.php',
        type: 'POST',
        dataType: 'JSON',
        data: {id: idVotar},
        success: function(data)
        {
          if(data)
          {
            if (contvotos > 1) {
              ModalMonsterInc();
            }
            //alert("Asisencia eliminada al simpatizante " + nombreVotar);
            btn.siblings('.btn-casilla-votar').removeAttr('disabled');
            btn.attr('disabled', 'true');
            btn.parent('td').parent('tr.row-simpatizante').removeClass('g-bg-add');
            //ENVIO DE DATOS AL BRIDGE
            var idbase64 = data.IdCaptura;
            var chkbase64 = data.CheckLocal;
            var idextbase64 = data.IdExterno; //Adición hecha el 29/06/18 a las 12:09 am
            $.ajax({
              url: "http://sysreg.tech/bridge/funciones/update_vots.php",
              type: "POST",
              dataType: "JSON",
              data: {
                      u : 'syscam.consulta',
                      k : 'cnslt140u4sscm',
                      IdCaptura : idbase64,
                      SincroBridge : chkbase64,
                      ides : idextbase64, //Adición hecha el 29/06/18 a las 12:09 am
                      sistem : '1',
                      action : '0'
                    },
              success: function(data)
              {
                if(data.estatus == "INSERTADO" || data.estatus == "ACTUALIZADO")
                {
                  $.ajax({
                    url: "../controller/ctrl.update.bridge.sincrobridge.php",
                    type: "POST",
                    dataType: "JSON",
                    data: {
                            IdCaptura : idVotar},
                    success: function(data)
                    {
                    },
                    error: function(errno){
                    }
                  })
                  .done(function() {
                    console.log("success");
                  })
                  .fail(function() {
                    console.log("error");
                  })
                  .always(function() {
                    console.log("complete");
                  });

                  //alert("SINCRONIZADO");
                }
              },
              error: function(errno){
              }
            })
            .done(function() {
              console.log("success");
            })
            .fail(function() {
              console.log("error");
            })
            .always(function() {
              console.log("complete");
            });
          }else{
            alert("Algo sucedió en el cambio de CheckLocal");
          }
        }
      })
      .done(function() {
        console.log("success");
      })
      .fail(function() {
        console.log("error");
      })
      .always(function() {
        console.log("complete");
      });
    }else
    {
      console.log("No");
    }
  });
});

//Ready para movilizador.php
$(document).ready(function() {
  var c = 0;
  $('.clave-movilizador').on('change', function(event) {
    event.preventDefault();
    /* Act on the event */
    if(c > 0)
    {
      //alert("Tengo más de 0 ");
      $('#table-faltantes').DataTable().destroy();
    }else{
      //alert("Aun no he subido a más de 0");
      c++;
    }
    $('#table-faltantes').removeClass('d-none');
    var parameters = {clave:$(this).val()};
    //console.log($(this).val());
    var tabla = $('#table-faltantes').DataTable(
      {
        "pageLength": 20,
         "language": {
               "lengthMenu": "Mostrar _MENU_ elementos por página",
               "zeroRecords": "No se ha encontrado ningun elemento - Intente con otra palabra",
               "info": "Página _PAGE_ de _PAGES_",
               "infoFiltered": "(filtrados de _MAX_ simpatizantes)",
               "infoEmpty": "Ningun elemento similar",
               "search": "Buscar:",
               "paginate": {
                "previous": "Atras",
                "next": "Siguiente",
               }
            },
            "ordering": true,
         "ajax": {
                "url": "../controller/ctrl.movilizador.php",
                "type": "POST",
                "dataType": "JSON",
                "cache": false,
                "data": parameters

                  },
        "columns":[
          {"data":"IdCaptura"},
          //{"data":"PaternoCaptura" + "MaternoCaptura" + "NombreCaptura"},
          {"render":
              function ( data, type, row ) {
                  return (row['PaternoCaptura'] + ' ' + row['MaternoCaptura'] + ' ' + row['NombreCaptura']);
              }
          },
          {"data":"FechaNacimiento"},
          {"render":
              function ( data, type, row ) {
                  return (row['Calle'] + ' ' + row['Noext'] + ' ' + row['Colonia'] + ' ' + row['Municipio'] + ' ' + row['Manzana']);
              }
          },
          /*{"data":"Movilizador"},*/
          /* {"data":"Casilla"}*/
         /*{"data":"Orden"}*/
        ]
      });
    $('.container-faltantes').html('');
    $('.container-faltantes').append('<button class="imprimir-formato btn btn-success" data-id="' + $(this).val() + '" title="Imprimir Formato">Imprimir</button>');
    //Agregamos función para imprimir formato
    $('.imprimir-formato').on('click', function(event) {
      var l="../reportes/reporteMovilizador.php?clave_mov="+$(this).attr('data-id');
      window.open(l, "Diseño Web", "width=950, height=950");
      //alert($(this).attr('data-id'));
     /* $.ajax({
        url: '../reportes/reporteMovilizador.php',
        type: 'GET',
        dataType: 'JSON',
        data: {Movilizador: $(this).attr('data-id')},

      })
      .done(function() {
        console.log("success");
      })
      .fail(function() {
        console.log("error");
      })
      .always(function() {
        console.log("complete");
      });*/
    });
  });
});

//Ready para mantenimiento.php
$(document).ready(function() {
  //DataTable Mantenimiento
  $('#table-mantenimiento').DataTable({
    "language": {
        "lengthMenu": "Mostrando _MENU_ simpatizantes por página",
        "zeroRecords": "Ningun elemento encontrado - Intenta nuevamente",
        "info": "Mostrando página _PAGE_ de _PAGES_",
        "infoEmpty": "No hay registros disponibles",
        "search": "Buscar:",
       "paginate": {
          "previous": "Atras",
          "next": "Siguiente"
       }
    },
    "orderable": false
  });
  //Click Eliminar
  $('body').on('click', '.btn-eliminar', function(event) {
    event.preventDefault();
    /* Act on the event */
    let btn = $(this);
    var id = $(this).val();
    var mov = $(this).attr('data-movilizador');
    var mover = confirm("¿DESEAS QUITAR A ÉSTA PERSONA DE ÉSTE MOVILIZADOR?");
    if (mover == false) return;
    $.ajax({
      url: '../controller/ctrl.delete.mantenimiento.php',
      type: 'POST',
      dataType: 'JSON',
      data: {
              id: id,
              mov: mov
      },
      success: function(data)
      {
        if(data)
        {
          //Ocultamos el botón de eliminar y todos los demas
          $(btn).addClass('d-none');
          $(btn).siblings('#btn1'+id).addClass('d-none');
          $(btn).siblings('#btn2'+id).addClass('d-none');
          //Mostramos los botones de agregar
          $(btn).siblings('.btn-agregar').removeClass('d-none');

          setTimeout(function(){
            location.reload();
          }, 1000);
          //alert("Éxito eliminando a la persona con el ID: " + id);
        }else{
          alert("Ocurrió algo mal, reintentar nuevamente gracias");
          return;
        }
        idbase64 = data.IdCaptura;
        estadobase64 = "" + data.Estado;
        movbase64 = "" + data.Movilizador;
        $.ajax({
          url: "http://sysreg.tech/bridge/funciones/update_vots.php",
          type: "POST",
          dataType: "JSON",
          data: {
                  u : 'syscam.consulta',
                  k : 'cnslt140u4sscm',
                  IdCaptura : idbase64,
                  Estado : estadobase64,
                  Movilizador : movbase64
                },
          success: function(data)
          {
            if(data.estatus == "INSERTADO" || data.estatus == "ACTUALIZADO")
            {
              //alert("Éxito eliminando a la persona con el ID: " + id);
              $.ajax({
                url: "../controller/ctrl.update.bridge.delete.php",
                type: "POST",
                dataType: "JSON",
                data: {
                        IdCaptura : id},
                success: function(data)
                {

                },
                error: function(errno){
                }
              })
              .done(function() {
                console.log("success");
              })
              .fail(function() {
                console.log("error");
              })
              .always(function() {
                console.log("complete");
              });

            }
          },
          error: function(errno){
          }
        })
      }
    })
    .done(function() {
      console.log("success");
    })
    .fail(function() {
      console.log("error");
    })
    .always(function() {
      console.log("complete");
    });
  });

  //Click Agregar
  $('.btn-agregar').on('click', function(event) {
    event.preventDefault();
    /* Act on the event */
    let btn = $(this);
    var id = $(this).val();
    var ids = [];
    ids.push({ids: id});
    var clave_mov = $(this).attr('data-mov');
    $.ajax({
      url: '../controller/ctrl.add.mantenimiento.php',
      type: 'POST',
      dataType: 'JSON',
      data: {
              ids: ids, 
              mov: clave_mov
            },
      success: function(data)
      {
        alert("Se ha regresado esta persona a este Movilizador");
        location.reload();
        if(data)
        {
          //Ocultamos el botón de agregar
          $(btn).addClass('d-none');
          $(btn).siblings('#btn2'+id).addClass('d-none');
          //Mostramos los demas botones
          $(btn).siblings('.btn-eliminar').removeClass('d-none');
          $(btn).siblings('#btn1'+id).removeClass('d-none');
          //alert("Éxito agregando nuevamente a la persona con el ID: " + id);
          console.log("ARREGLO", data);
          var $json_data2 = data;
          $.each($json_data2, function(index, value){
            $.ajax({
              url : 'http://sysreg.tech/bridge/funciones/update_vots.php',
              type : 'POST',
              dataType : 'JSON',
              data : {
                u : 'syscam.consulta',
                k : 'cnslt140u4sscm',
                IdCaptura : value.IdCaptura,
                Movilizador : value.Movilizador,
                Estado : value.Estado
              },
              success : function(response){
                console.log(response);
                mov = response.Movilizador;
                if (response.estatus == "INSERTADO" || response.estatus == "ACTUALIZADO") {
                  $.ajax({
                    url : '../controller/ctrl.update.bridge.movilizador2.php',
                    type : 'POST',
                    dataType : 'JSON',
                    data : {
                      u : 'syscam.consulta',
                      k : 'cnslt140u4sscm',
                      IdCaptura : value.id
                    },
                    success : function(response){
                      if (response == true) {
                        //alert("SINCRONIZADO");
                      }
                      console.log(response);
                    }
                  });
                }
              }
            });
          });
        } else{
          alert("Ocurrió algo mal, reintentar nuevamente gracias");
        }
      }
    })
    .done(function() {
      console.log("success");
    })
    .fail(function() {
      console.log("error");
    })
    .always(function() {
      console.log("complete");
    });

  });
  $('.g-cursor-disabled').on('click', function(event){
    alert("No puedes asignar esta persona a este movilizador porque ya tiene a otro movilizador asignado");
  });
});
//Funciones para pantalla carga
var varCarga;
function cargaFunction() {
    varCarga = setTimeout(showPage, 1);
}

function showPage() {
  document.getElementById("loader").style.display = "none";
  document.getElementById("body-content").style.display = "block";
}

//Agregamos evento click para mostrar pantalla de carga cada vez que generemos folio ó abramos la vista mantenimiento
$(document).ready(function() {
  $('.btn-mantenimiento, .btn-generar-folio').on('click', function(event) {
    $('#loader').css('display', 'block');
    $('#body-content').css('display', 'none');
  });
});
//Ready para Panel.php
$(document).ready(function() {
  var c = 0;
  $('.clave-movilizador').on('change', function(event) {
    event.preventDefault();
    /* Act on the event */
    if(c > 0)
    {
      //alert("Tengo más de 0 ");
      $('#table-panel').DataTable().destroy();
    }else{
      //alert("Aun no he subido a más de 0");
      c++;
    }
    $('#table-panel').removeClass('d-none');
    var parameters = {clave:$(this).val()};
    //console.log($(this).val());
    var tabla = $('#table-panel').DataTable(
      {
        "pageLength": 20,
         "language": {
               "lengthMenu": "Mostrar _MENU_ elementos por página",
               "zeroRecords": "No se ha encontrado ningun elemento - Intente con otra palabra",
               "info": "Página _PAGE_ de _PAGES_",
               "infoFiltered": "(filtrados de _MAX_ simpatizantes)",
               "infoEmpty": "Ningun elemento similar",
               "search": "Buscar:",
               "paginate": {
                "previous": "Atras",
                "next": "Siguiente",
               }
            },
            "ordering": true,
         "ajax": {
                "url": "../controller/ctrl.panel.php",
                "type": "POST",
                "dataType": "JSON",
                "cache": false,
                "data": parameters

                  },
        "columns":[
          {"data":"IdCaptura"},
          //{"data":"PaternoCaptura" + "MaternoCaptura" + "NombreCaptura"},
          {"render":
              function ( data, type, row ) {
                  return (row['PaternoCaptura'] + ' ' + row['MaternoCaptura'] + ' ' + row['NombreCaptura']);
              }
          },
          {"data":"FechaNacimiento"},
          {"render":
              function ( data, type, row ) {
                  return (row['Calle'] + ' ' + row['Noext'] + ' ' + row['Colonia'] + ' ' + row['Municipio'] + ' ' + row['Manzana']);
              }
          },
          {"data":"Seccion"},
          {"data":"Casilla"},
          {"data":"Movilizador"},
          {"data":"Folio"},
          {"data":"Orden"},
          {"data":"CheckLocal"},
          {"data":"SincroBridge"},
          {"data":"Estado"}
        ]
      });

  });
});

function getval(valor)
{
  var opcion = confirm("¿DESEAS EDITAR ESTE REGISTRO?");
  if (opcion == false) {
    return;
  }
  var id = valor.value;
  var elemento1 = ".tel"+id;
  var elemento2 = ".calle"+id;
  var elemento3 = ".noext"+id;
  var elemento4 = ".noint"+id;
  var elemento5 = ".colonia"+id;
  var elemento6 = ".municipio"+id;
  var elemento8 = ".manzana"+id;
  var elemento9 = ".mov"+id;
  var elemento10 = ".cruz1"+id;
  var elemento11 = ".cruz2"+id;
  $(elemento1).removeAttr('disabled');
  $(elemento2).removeAttr('disabled');
  $(elemento3).removeAttr('disabled');
  $(elemento4).removeAttr('disabled');
  $(elemento5).removeAttr('disabled');
  $(elemento6).removeAttr('disabled');
  $(elemento8).removeAttr('disabled');
  $(elemento9).removeAttr('disabled');
  $(elemento10).removeAttr('disabled');
  $(elemento11).removeAttr('disabled');
  var btn1 = "#btn1" + id;
  var btn2 = "#btn2" + id;
  $(btn1).addClass('d-none');
  $(btn1).siblings(btn2).removeClass('d-none');
  $(btn1).siblings('.btn-eliminar').addClass('d-none');
  $(btn1).siblings('.btn-agregar').addClass('d-none');
}

function enviar(valor)
{
  var tel = "";
  var manzana = "";
  var id = valor.value;
  var elemento1 = ".tel"+id;
  var elemento2 = ".calle"+id;
  var elemento3 = ".noext"+id;
  var elemento4 = ".noint"+id;
  var elemento5 = ".colonia"+id;
  var elemento6 = ".municipio"+id;
  var elemento8 = ".manzana"+id;
  var elemento9 = ".mov"+id;
  var elemento10 = ".cruz1"+id;
  var elemento11 = ".cruz2"+id;
  var btn1 = "#btn1" + id;
  var btn2 = "#btn2" + id;
  function validarTel(tele)
  {
    if((/^([0-9])*$/.test(tele)))
    {
      tel = tele;
      return true;
    }else
    {
      return false;
    }
  }
  function validarDigito(nume)
  {
    if((/^([0-9])*$/.test(nume)))
    {
      manzana = nume;
      return true;
    }else
    {
      return false;
    }
  }
  var telflag = validarTel($(elemento1).val());
  var calle = $(elemento2).val();
  var noext = $(elemento3).val();
  var noint = $(elemento4).val();
  var colonia = $(elemento5).val();
  var municipio = $(elemento6).val()
  var manzanaflag = validarDigito($(elemento8).val());
  var mov = $(elemento9).val();
  var cruz1 = $(elemento10).val();
  var cruz2 = $(elemento11).val();

  if(telflag != false && manzanaflag != false)
  {
    $(elemento1).attr('disabled','disabled');
    $(elemento2).attr('disabled','disabled');
    $(elemento3).attr('disabled','disabled');
    $(elemento4).attr('disabled','disabled');
    $(elemento5).attr('disabled','disabled');
    $(elemento6).attr('disabled','disabled');
    $(elemento8).attr('disabled','disabled');
    $(elemento9).attr('disabled','disabled');
    $(elemento10).attr('disabled','disabled');
    $(elemento11).attr('disabled','disabled');
    $(btn1).removeClass('d-none');
    $(btn1).siblings(btn2).addClass('d-none');
    $(btn1).siblings('.btn-eliminar').removeClass('d-none');
    $(btn1).siblings('.btn-agregar').addClass('d-none');
    var opcion = confirm("ESTÁS SEGURO DE ACTUALIZAR ESTOS DATOS? \n \n Teléfono: " +
      tel + ", Calle: " + calle + ", X " + cruz1 + ", Y " + cruz2 +
      ", No Ext: " + noext + ", No Int: " + noint + ", Colonia: " + colonia +
      ", Municipio: " + municipio + ", Manzana: " + manzana + ", Movilizador" + mov);
    if (opcion == false) {
      return;
    } else {
      //AQUI ENVIAMOS LOS DATOS PARA ACTUALIZAR//
      $.ajax({
        url: '../controller/ctrl.update.movilizador.php',
        type: 'POST',
        dataType: 'JSON',
        data: { id: id,
                tel: tel,
                calle: calle,
                cruz1: cruz1,
                cruz2: cruz2,
                noext: noext,
                noint: noint,
                colonia: colonia,
                municipio: municipio,
                manzana: manzana,
                movilizador: mov},
        success: function(data)
        {
          //alert("DATOS GUARDADOS CORRECTAMENTE");
          idbase64  = data.IdCaptura;
          telbase64 = data.Telefono;
          calbase64 = data.Calle;
          cr1base64 = data.Cruzamiento1;
          cr2base64 = data.Cruzamiento2;
          extbase64 = data.Noext;
          intbase64 = data.Noint;
          colbase64 = data.Colonia;
          munbase64 = data.Municipio;
          manbase64 = data.Manzana;
          movbase64 = data.Movilizador;
          $.ajax({
            url: "http://sysreg.tech/bridge/funciones/update_vots.php",
            type: "POST",
            dataType: "JSON",
            data: {
                    u : 'syscam.consulta',
                    k : 'cnslt140u4sscm',
                    IdCaptura : idbase64,
                    Telefono : telbase64,
                    Calle : calbase64,
                    Cruzamiento1 : cr1base64,
                    Cruzamiento2 : cr2base64,
                    Noext : extbase64,
                    Noint : intbase64,
                    Colonia : colbase64,
                    Municipio : munbase64,
                    Manzana : manbase64,
                    Movilizador : movbase64
                  },
            success: function(data)
            {
              if(data.estatus == "INSERTADO" || data.estatus == "ACTUALIZADO")
              {
                $.ajax({
                  url: "../controller/ctrl.update.bridge.movilizador2.php",
                  type: "POST",
                  dataType: "JSON",
                  data: {
                          IdCaptura : id},
                  success: function(data)
                  {
                  },
                  error: function(errno){
                  }
                })
                .done(function() {
                  console.log("success");
                })
                .fail(function() {
                  console.log("error");
                })
                .always(function() {
                  console.log("complete");
                });

                //alert("SINCRONIZADO");
              }
            },
            error: function(errno){
            }
          })
          .done(function() {
            console.log("success");
          })
          .fail(function() {
            console.log("error");
          })
          .always(function() {
            console.log("complete");
          });

        }
      })
      .done(function() {
        console.log("success");
      })
      .fail(function() {
        console.log("error");
      })
      .always(function() {
        console.log("complete");
      });
    }
  }else if(telflag == false){
    alert("Coloca un formato adecuado para el teléfono, gracias");
    $(elemento1).val('');
    $(elemento1).focus();
  }else if(manzanaflag == false){
    /*$(elemento8).focus();*/
  }
}

function getval2()
{
  $("#table-search").html('');
  var nombre = $("#nombre").val();
  var apellidoP = $("#apellidoP").val();
  var apellidoM = $("#apellidoM").val();
  var mov = "<?php echo $_POST['clv']; ?>";
  $("#table-search").load("../controller/despliega.busqueda.php?nombre="+nombre+"&paterno="+apellidoP+"&materno="+apellidoM+"&mov="+mov+"&seccion=<?php echo $seccion; ?>");
}

// PARA mantenimiento_seccion.php
$(document).ready(function(){
  //Evento para actualizar el movilizador
  $(".select-filtro").on('change', function(event) {
    //Limpia los campos
    $("#IdCaptura").val("");
    $("#Calle").val("");
    $("#Colonia").val("");
    $("#Manzana").val("");
    $("#Movilizador").val("");
    $("#Folio").val("");

    event.preventDefault();
    /* Act on the event */
    var select = $(this).val();
    //si es busqueda por id
    switch(select)
    {
      case "1":
          //console.log("1\n");
          $(".container-id").removeClass('d-none');
          $(".container-direccion").addClass('d-none');
          $(".container-direccion").removeClass('d-flex');
          $(".container-movilizador").addClass('d-none');
          $(".container-folio").addClass('d-none');
          $(".container-btnFiltro").removeClass('d-none');
      break;
      case "2":
          //console.log("2\n");
          $(".container-id").addClass('d-none');
          $(".container-direccion").removeClass('d-none');
          $(".container-direccion").addClass('d-flex');
          $(".container-movilizador").addClass('d-none');
          $(".container-folio").addClass('d-none');
          $(".container-btnFiltro").removeClass('d-none');
      break;
      case "3":
          //console.log("3\n");
          $(".container-id").addClass('d-none');
          $(".container-direccion").addClass('d-none');
          $(".container-direccion").removeClass('d-flex');
          $(".container-movilizador").removeClass('d-none');
          $(".container-folio").addClass('d-none');
          $(".container-btnFiltro").removeClass('d-none');
      break;
      case "4":
          //console.log("4\n");
          $(".container-id").addClass('d-none');
          $(".container-direccion").addClass('d-none');
          $(".container-direccion").removeClass('d-flex');
          $(".container-movilizador").addClass('d-none');
          $(".container-folio").removeClass('d-none');
          $(".container-btnFiltro").removeClass('d-none');
      break;
    }
  });
  var d = 0;
  $(".btn-filtrar").on("click", function(event){
    event.preventDefault();
    var IdCaptura = $("#IdCaptura").val(),
        Calle = $("#Calle").val(),
        Colonia = $("#Colonia").val(),
        Manzana = $("#Manzana").val(),
        Movilizador = $("#Movilizador").val(),
        Folio = $("#Folio").val();
    if(d > 0)
    {
      //alert("Tengo más de 0 ");
      $('#table-filtro').DataTable().destroy();
    }else{
      //alert("Aun no he subido a más de 0");
      d++;
    }
    $('#table-filtro').removeClass('d-none');
    var parameters = {
                        IdCaptura: IdCaptura,
                        Calle: Calle,
                        Colonia: Colonia,
                        Manzana: Manzana,
                        Movilizador: Movilizador,
                        Folio: Folio
                      };
    //console.log($(this).val());
    $('.selecionamov').removeClass('d-none');
    var tablafiltro = $('#table-filtro').DataTable(
      {
        "pageLength": 20,
        "language": {
               "lengthMenu": "Mostrar _MENU_ simpatizantes por página",
               "zeroRecords": "No se ha encontrado ningun elemento - Intente con otra palabra",
               "info": "Página _PAGE_ de _PAGES_",
               "infoFiltered": "(filtrados de _MAX_ simpatizantes)",
               "infoEmpty": "Ningun elemento similar",
               "search": "Buscar:",
               "paginate": {
                "previous": "Atras",
                "next": "Siguiente",
               }
            },
        "ordering": true,
        "ajax": {
                "url": "../controller/ctrl.filtro.php",
                "type": "POST",
                "dataType": "JSON",
                "cache": false,
                "data": parameters

                  },
        "columns":[
          {"render":
              function ( data, type, row ) {
                  return ('<input type="checkbox" name="idcheck" id="' + row['IdCaptura'] + '" value="' + row['IdCaptura'] + '" style="width: 2rem;">');
              }
          },
          {"data":"IdCaptura"},
          {"render":
              function ( data, type, row ) {
                  return (row['PaternoCaptura'] + ' ' + row['MaternoCaptura'] + ' ' + row['NombreCaptura']);
              }
          },
          {"render":
              function ( data, type, row ) {
                  return (row['Calle'] + ' ' + row['Noext'] + ' ' + row['Colonia']);
              }
          },
          {"data": "Colonia"},
/*          {"render":
              function ( data, type, row ) {
                  //console.log("datos", type);
                  //return("<input type='tel' class='input-movilizador form-control' placeholder='Nuevo Movilizador'>");
                  return("<span class='d-none'>1</span><select name='js-movilizador' id='js-movilizador' class='js-movilizador custom-select'></select>");
              }
          },
          {"render":
              function ( data, type, row ) {
                  return ("<button class='btn-update-movilizador mr-2 btn btn-outline-info px-4' data-id='" + row['IdCaptura']  + "' data-movilizador='" + row['Movilizador'] + "'' title='Actualizar Movilizador' style='padding: 0px; margin: 0px; cursor: pointer;'><i class='fa fa-upload'></i></button>");
              }
          },*/
        ],
        "createdRow":
          function( row, data, dataIndex ) {
            /*if ( data[1] == "1188602" ) {
              $(row).addClass( 'important' );
            }else{
              $(row).addClass( 'no-important' );
            }*/
            $('td', row).eq(4).addClass('col-movilizador');
          },
      });

    $("#Ids-Movilizador").on("click", function(event){
      event.preventDefault();
      var selected = [];
      $('#table-filtro input:checked').each(function() {
          selected.push({id: $(this).attr('value')});
      });
      var mov = $('#NuevoMovilizador').val();
      if (mov == null) {
        alert("No has seleccionado al movilizador");
        return;
      }
      var confirmar = confirm("¿Estás seguro(a) de que quieres pasar estas personas con este movilizador?");
      if (confirmar == false) {
        return;
      }
      //AQUI VA EL CONTROLADOR PARA ENVIAR LOS ID'S DE ESTE GRUPO DE CHEQUEOS
      console.log(selected);
      //return false;
        $.ajax({
        url: "../controller/ctrl.mov.movilizador.php",
        type: "POST",
        dataType: "JSON",
        data: {
                mov: mov,
                ids : selected
              },
        success: function(data)
        {
          console.log("ARREGLO", data);
          var $json_data2 = data;
          $.each($json_data2, function(index, value){
            $.ajax({
              url : 'http://sysreg.tech/bridge/funciones/update_vots.php',
              type : 'POST',
              dataType : 'JSON',
              data : {
                u : 'syscam.consulta',
                k : 'cnslt140u4sscm',
                IdCaptura : value.IdCaptura,
                Movilizador : value.Movilizador,
                SincroBridge : value.CheckLocal
              },
              success : function(response){
                console.log(response);
                if (response.estatus == "INSERTADO" || response.estatus == "ACTUALIZADO") {
                  $.ajax({
                    url : '../controller/ctrl.update.bridge.movilizador2.php',
                    type : 'POST',
                    dataType : 'JSON',
                    data : {
                      u : 'syscam.consulta',
                      k : 'cnslt140u4sscm',
                      IdCaptura : value.id
                    },
                    success : function(response){
                      if (response == true) {
                        //alert("SINCRONIZADO");
                      }
                      console.log(response);
                    }
                  });
                }
              }
            });
          });
        }
      })
      .done(function() {
        console.log("success");
      })
      .fail(function() {
        console.log("error");
      })
      .always(function() {
        console.log("complete");
      });
      location.reload(); 
    });

     function agregar()
     {
        $.ajax({
           url: '../controller/ctrl.movilizadores.php',
           type: 'POST',
           dataType: 'JSON',
           success: function(data)
           {
              $('.js-movilizador').html('');
              //$('.js-movilizador').append("<input type='tel' class='input-movilizador form-control' placeholder='Nuevo Movilizador'>");
              var inputSelect = "";
              for(var x in data)
              {
                for(var y in data[x])
                {
                  $('.js-movilizador').append("<option value='" + data[x][y].ClaveMovilizador + "' >" + data[x][y].PaternoMovilizador + " " + data[x][y].MaternoMovilizador + " " + data[x][y].NombreMovilizador);
                  //console.log('Prueba: ' + data[x][y].ClaveMovilizador);
                }
              }
           }
        })
        .done(function() {
           console.log("success");
        })
        .fail(function() {
           console.log("error");
        })
        .always(function() {
           console.log("complete");
        });
     }
     setTimeout(function(){
        agregar();
     }, 100);
   });
   $('body').on('click', '.btn-update-movilizador', function(event) {
       event.preventDefault();
       var movAnterior = $(this).attr('data-movilizador'),
           idCaptura = $(this).attr('data-id')
           movNuevo = $(this).parent('td').siblings('td.col-movilizador').children('.js-movilizador').val();
       //console.log('Id: ' + idCaptura + '\nMovilizador Anterior: ' + movAnterior + '\nMovilizador Nuevo: ' + movNuevo);
       $.ajax({
         url: '../controller/ctrl.update.movilizador2.php',
         type: 'POST',
         dataType: 'JSON',
         data: {
                 id: idCaptura,
                 mov_ant: movAnterior,
                 movilizador: movNuevo
               },
         success: function(data)
         {
            if (data) {
        
              setTimeout(function(){
                location.reload();
              }, 1000);
              //alert("Registros guardados!");
            }
            else {
              alert("Ha ocurrido un error!");
              return;
            }
            idbase64 = "" + data.IdCaptura;
            movbase64 = "" + data.Movilizador;
            estbase64 = "" + data.Estado;
            $.ajax({
              url: "http://sysreg.tech/bridge/funciones/update_vots.php",
              type: "POST",
              dataType: "JSON",
              data: { 
                      u : 'syscam.consulta',
                      k : 'cnslt140u4sscm',
                      IdCaptura : idbase64,
                      Movilizador : movbase64,
                      Estado : estbase64},
              success: function(data)
              {
                if(data.estatus == "INSERTADO" || data.estatus == "ACTUALIZADO")
                {
                  //alert("SINCRONIZADO");
                  $.ajax({
                    url: "../controller/ctrl.update.bridge.movilizador2.php",
                    type: "POST",
                    dataType: "JSON",
                    data: { 
                            IdCaptura : id},
                    success: function(data)
                    {
                    }, 
                    error: function(errno){
                        console.log(errno);
                    }
                  })
                  .done(function() {
                    console.log("success");
                  })
                  .fail(function() {
                    console.log("error");
                  })
                  .always(function() {
                    console.log("complete");
                  });
                }
              }, 
              error: function(errno){
                console.log(errno); 
              }
            })
            .done(function() {
              console.log("success");
            })
            .fail(function(error) {
              console.log(error);
            })
            .always(function() {
              console.log("complete");
            });
         }
       })
       .done(function() {
         console.log("success");
       })
       .fail(function() {
         console.log("error");
       })
       .always(function() {
         console.log("complete");
       });
       
    });
});

function ModalMonsterInc() {
  //Botón para abrir el modal
  $('#monster-modal').addClass('appear');
  //alert("AWAAAANTAAAAAAA");
}

//PARA MODAL DE CORREOS
$(document).ready(function() {
  $(".btn-correo").on("click", function(event){
      //obtenemos el valor
      var correoSeleccionado = $("#select-correo").val();
      var movilizador = $("#menu").val();
      if(correoSeleccionado != null)
      {
        //se selecciono un correo
        //alert(correoSeleccionado);
        $.ajax({
          url: '../controller/ctrl.correos.php',
          type: 'POST',
          dataType: 'JSON',
          data: {
            correo : correoSeleccionado,
            mov : movilizador
          }
        });
        alert("Correo generado");

      }else{
        //no se selecciono un correo
        alert("debes seleccionar un correo");
      }
  });
});

//PARA CONTACTOS
$(document).ready(function() {
  var data = "";
  $(".select-perfil-contacto").on('change', function(event) {
    //Limpia los campos de claves
    $("#txt_clv_op").val("");
    $("#txt_clv_mov").val("");
    //Borra botones dibujados
    $("#btn-print-contactos").remove();
    $("#btn-envia-correo").remove();
    //comienza
    var perfils = $(this).val();
    switch(perfils) {
      case "1":
        $("#txt_clv_mov").addClass('d-none');
        $("#txt_clv_op").addClass('d-none');
        $("#select-orden-lid-op-mov").removeClass('d-none');
        $("#select-orden-rc").addClass('d-none');
        $("#select-orden-visitas").addClass('d-none');
        $("#select-orden-movilidad").addClass('d-none');
        $("#select-orden-completo").addClass('d-none');
      break;
      case "2":
        $("#txt_clv_op").removeClass('d-none');
        $("#txt_clv_mov").addClass('d-none');
        $("#select-orden-lid-op-mov").removeClass('d-none');
        $("#select-orden-rc").addClass('d-none');
        $("#select-orden-visitas").addClass('d-none');
        $("#select-orden-movilidad").addClass('d-none');
        $("#select-orden-completo").addClass('d-none');
      break;
      case "3":
        $("#txt_clv_mov").removeClass('d-none');
        $("#txt_clv_op").addClass('d-none');
        $("#select-orden-lid-op-mov").removeClass('d-none');
        $("#select-orden-rc").addClass('d-none');
        $("#select-orden-visitas").addClass('d-none');
        $("#select-orden-movilidad").addClass('d-none');
        $("#select-orden-completo").addClass('d-none');
      break;
      case "4":
        $("#txt_clv_mov").addClass('d-none');
        $("#txt_clv_op").addClass('d-none');
        $("#select-orden-rc").removeClass('d-none');
        $("#select-orden-lid-op-mov").addClass('d-none');
        $("#select-orden-visitas").addClass('d-none');
        $("#select-orden-movilidad").addClass('d-none');
        $("#select-orden-completo").addClass('d-none');
      break;
      case "5":
        $("#txt_clv_mov").addClass('d-none');
        $("#txt_clv_op").addClass('d-none');
        $("#select-orden-visitas").removeClass('d-none');
        $("#select-orden-lid-op-mov").addClass('d-none');
        $("#select-orden-rc").addClass('d-none');
        $("#select-orden-movilidad").addClass('d-none');
        $("#select-orden-completo").addClass('d-none');
      break;
      case "6":
        $("#txt_clv_mov").addClass('d-none');
        $("#txt_clv_op").addClass('d-none');
        $("#select-orden-movilidad").removeClass('d-none');
        $("#select-orden-lid-op-mov").addClass('d-none');
        $("#select-orden-rc").addClass('d-none');
        $("#select-orden-visitas").addClass('d-none');
        $("#select-orden-completo").addClass('d-none');
      break;
      case "7":
        $("#txt_clv_mov").addClass('d-none');
        $("#txt_clv_op").addClass('d-none');
        $("#select-orden-completo").removeClass('d-none');
        $("#select-orden-lid-op-mov").addClass('d-none');
        $("#select-orden-rc").addClass('d-none');
        $("#select-orden-visitas").addClass('d-none');
        $("#select-orden-movilidad").addClass('d-none');
      break;
    }
  });

  c = 0;
  $("#x").on('click', function(event) {
    //var data = "";
    var perfil = $(".select-perfil-contacto").val();
    if (perfil == null) {
      alert("Escoge un perfil");
      return;
    }
    if (perfil == "1") {
      var selectorden = $("#select-orden-lid-op-mov").val();
      //imprime botones
      $('.imprimir-btn-contactos').html("");
      $('.imprimir-btn-contactos').append('<a href="../controller/ctrl.impresion.contacto.php?perfil='+perfil+'&Orden='+selectorden+'" target="-" id="btn-print-contactos" class="btn btn-success" title="Imprimir">Imprimir</a>');
      data =  { 
        perfil : perfil,
        Orden : selectorden
      };
    } else if (perfil == "2") {
      var selectorden = $("#select-orden-lid-op-mov").val();
      var selectorvalue = $("#txt_clv_op").val();
      if(selectorvalue=="") {
        alert("Debe ingresar una clave");
        return;
      } 
      $('.imprimir-btn-contactos').html("");
      $('.imprimir-btn-contactos').append('<a href="../controller/ctrl.impresion.contacto.php?perfil='+perfil+'&Orden='+selectorden+'&Clave='+selectorvalue+'" target="-" id="btn-print-contactos" class="btn btn-success" title="Imprimir">Imprimir</a>');
      data =  { 
        perfil : perfil,
        Orden : selectorden,
        Clave : selectorvalue
      };                    
    } else if (perfil == "3") {
      var selectorden = $("#select-orden-lid-op-mov").val();
      var selectorvalue = $("#txt_clv_mov").val();
      if(selectorvalue=="") {
        alert("Debe ingresar una clave")
        return;
      }
      //imprime botones
      $('.imprimir-btn-contactos').html("");
      $('.imprimir-btn-contactos').append('<a href="../controller/ctrl.impresion.contacto.php?perfil='+perfil+'&Orden='+selectorden+'&Clave='+selectorvalue+'" target="-" id="btn-print-contactos" class="btn btn-success" title="Imprimir">Imprimir</a>');
      data =  { 
        perfil : perfil,
        Orden : selectorden,
        Clave : selectorvalue
      };
    } else if (perfil == "4") {
      var selectorden = $("#select-orden-rc").val();
      //imprime botones
      $('.imprimir-btn-contactos').html("");
      $('.imprimir-btn-contactos').append('<a href="../controller/ctrl.impresion.contacto.php?perfil='+perfil+'&Orden='+selectorden+'" target="-" id="btn-print-contactos" class="btn btn-success" title="Imprimir">Imprimir</a>');
      data =  { 
        perfil : perfil,
        Orden : selectorden
      };
    } else if (perfil == "5") {
      var selectorden = $("#select-orden-visitas").val();
      //imprime botones
      $('.imprimir-btn-contactos').html("");
      $('.imprimir-btn-contactos').append('<a href="../controller/ctrl.impresion.contacto.php?perfil='+perfil+'&Orden='+selectorden+'" target="-" id="btn-print-contactos" class="btn btn-success" title="Imprimir">Imprimir</a>');
      data =  { 
        perfil : perfil,
        Orden : selectorden
      };
    } else if (perfil == "6") {
      var selectorden = $("#select-orden-movilidad").val();
      //imprime botones
      $('.imprimir-btn-contactos').html("");
      $('.imprimir-btn-contactos').append('<a href="../controller/ctrl.impresion.contacto.php?perfil='+perfil+'&Orden='+selectorden+'" target="-" id="btn-print-contactos" class="btn btn-success" title="Imprimir">Imprimir</a>');
      data =  { 
        perfil : perfil,
        Orden : selectorden
      };
    } else if (perfil == "7") {
      var selectorden = $("#select-orden-completo").val();
      //imprime botones
      $('.imprimir-btn-contactos').html("");
      $('.imprimir-btn-contactos').append('<a href="../controller/ctrl.impresion.contacto.php?perfil='+perfil+'&Orden='+selectorden+'" target="-" id="btn-print-contactos" class="btn btn-success" title="Imprimir">Imprimir</a>');
      data =  { 
        perfil : perfil,
        Orden : selectorden
      };
    }
    $('.imprimir-btn-contactos').append('&nbsp;<button class="btn btn-info" id="btn-envia-correo" data-toggle="modal" data-target="#ModalContactos">Enviar Correo</button>');
    if (c != 0) {
      $('#table-listado').DataTable().destroy();
    } else {
      c++;
    }

    $('#table-listado').removeClass('d-none');
    var tablavotantes = $('#table-listado').DataTable({
      "pageLength": 20,
      "language": {
             "lengthMenu": "Mostrar _MENU_ simpatizantes por página",
             "zeroRecords": "No se ha encontrado ningun elemento - Intente con otra palabra",
             "info": "Página _PAGE_ de _PAGES_",
             "infoFiltered": "(filtrados de _MAX_ simpatizantes)",
             "infoEmpty": "Ningun elemento similar",
             "search": "Buscar:",
             "paginate": {
              "previous": "Atras",
              "next": "Siguiente",
             }
          },
      "ordering": false,
      "ajax": {
              "url": "../controller/ctrl.impresiones.orderby.php",
              "type": "POST",
              "dataType": "JSON",
              "cache": false,
              "data": data
              },
      "columns":[
        {"data":"IdCaptura"},
        {"render":
            function ( data, type, row ) {
                return (row['PaternoCaptura'] + ' ' + row['MaternoCaptura'] + ' ' + row['NombreCaptura']);
            }
        },
        {"render":
            function ( data, type, row ) {
                return (row['Calle'] + ' ' + row['Noext'] + ' ' + row['Colonia']);
            }
        },
        {"data": "Colonia"},
      ]
    });
    //alert("Hola");
  });

  //Eliminar boton al cambiar valor de selects de orden
  $("#select-orden-lid-op-mov").on('change', function(event) {
    $("#btn-print-contactos").remove();
    $("#btn-envia-correo").remove();
  });
  $("#select-orden-rc").on('change', function(event) {
    $("#btn-print-contactos").remove();
    $("#btn-envia-correo").remove();
  });
  $("#select-orden-visitas").on('change', function(event) {
    $("#btn-print-contactos").remove();
    $("#btn-envia-correo").remove();
  });
  $("#select-orden-movilidad").on('change', function(event) {
    $("#btn-print-contactos").remove();
    $("#btn-envia-correo").remove();
  });
  $("#select-orden-completo").on('change', function(event) {
    $("#btn-print-contactos").remove();
    $("#btn-envia-correo").remove();
  });

  //BOTON DE ENVIO DE CORREO
  $("#btn-enviando-correos").on('click', function(event) {
    var correo = $("#select-correo").val();
    //data.push({ "correo" : correo});
    if (data.perfil == "2" || data.perfil == "3") {
      data = { perfil : data.perfil,
               Orden : data.Orden,
               Clave : data.Clave,
               correo : correo };
    } else {
      data = { perfil : data.perfil,
               Orden : data.Orden,
               correo : correo };
    }
    $.ajax({
      url: "../controller/ctrl.control.correos.php",
      type: "POST",
      dataType: "JSON",
      data: data,
      success: function(data)
      {
      }, 
      error: function(errno){
        alert("Correo generado");
        console.log(errno);
      }
    })
    .done(function() {
      console.log("success");
    })
    .fail(function() {
      console.log("error");
    })
    .always(function() {
      console.log("complete");
    });
  });

});


$(document).ready(function() {
  $('#btn-reestablecer').on('click', function(event) {
    var pass = $("#input-pass").val();
    if (pass == "") {
      alert("ingresa la contraseña");
      return;
    }
    $.ajax({
      url: "../controller/ctrl-vidas.php",
      type: "POST",
      dataType: "JSON",
      data: {
              pass : pass
            },
      success: function(data)
      {
        if (data == true) {
          alert("DATOS ACTUALIZADOS");
          location.href = "index.php";
        } else {
          alert("Contraseña incorrecta");
        }
      }, 
      error: function(errno){
        console.log(errno);
      }
    })
    .done(function() {
      console.log("success");
    })
    .fail(function() {
      console.log("error");
    })
    .always(function() {
      console.log("complete");
    });
  });
});